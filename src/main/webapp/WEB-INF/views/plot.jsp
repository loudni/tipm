<html>
<body>
<%@include file="header.jsp" %>

<style>
.double {
  zoom: 1.2;
  transform: scale(1.2);
  -ms-transform: scale(1.2);
  -webkit-transform: scale(1.2);
  -o-transform: scale(1.2);
  -moz-transform: scale(1.2);
  transform-origin: 0 0;
  -ms-transform-origin: 0 0;
  -webkit-transform-origin: 0 0;
  -o-transform-origin: 0 0;
  -moz-transform-origin: 0 0;
  -webkit-transform-origin: 0 0;
}
</style>
<script>
function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;

  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  return result;
}

function show_column_selections(){
  $.ajax({ url: "/rest/metadata/attributes", context: document.body }).done(function(data) {
       var has_windows = false;
       var has_labels = false;
       for (var i = 0; i < data.length; i++ ) {
           var column = Object.keys(data[i])[0];
           if(column.toLowerCase() == "window" ){
           		has_windows = true;
           		continue;
           }
           if(column.toLowerCase() == "label" ){
           		has_labels = true;
           		continue;
           }
           $("#x_column").append($("<option></option>").attr("value",column).text(column));
           $("#y_columns").append($("<option></option>").attr("value",column).text(column));
           $("#y_events").append($("<option></option>").attr("value",column).text(column));
       }
       $("#y_columns").val(Object.keys(data[1])[0]);
       $(".chosen").chosen();
       $(".chosen").next().css('font-size','11px');
       $("#show_windows").prop("checked", has_windows);
       if(!has_windows){
       		$("#show_windows").attr("disabled", true);
       }
       $("#show_labels").prop("checked", has_labels);
       if(!has_labels){
       		$("#show_labels").attr("disabled", true);
       }
  });
}

var pattern_metadata = '';
var pattern_details = ''; //store pattern/support here
function show_patternset_selection(){
	  $.ajax({ url: "/rest/metadata/fileitem", context: document.body }).done(function(data) {
		  pattern_metadata = data["patterns"];
		  pattern_details = [];
		  //add option group and options
		  for(var i=0;i<pattern_metadata.length;i++){
		        var patternset = pattern_metadata[i];
		        if(!patternset['filenameOccurrences']){
		        		pattern_details.push([]);
		        		continue;
		        }
		        var fname = patternset['filename'];
		        var type = patternset['type'];
		        var columns = patternset['columns'];
		        var noPatterns = patternset['noPatterns'];
		        var option_group = $("<optgroup></optgroup>").attr("label", type + " " + columns + "(#" + noPatterns + ")");
		        $("#patterns").append(option_group);
		        $.ajax({ url: "/rest/load-patterns?filename=" + encodeURIComponent(fname), context: document.body, async:false}).done(function(data) {
		  	  		//if only one time series as input: do not show label, e.g. show 'value=7 value=9' as '7 9'
		  	  		var simplify = columns + '=';
	         		if(columns.indexOf(',') != -1){
	         			simplify = '';
	         		}
				  	var rows = data["rows"];
				  	for(var j=1;j<rows.length;j++){
				  		 var pattern = rows[j][0];
				  		 pattern = replaceAll(pattern, simplify, '');
				  		 pattern = pretty_print_pattern(pattern);
				  		 var pattern_txt = j + ': ' + pattern;
				  		 var support = rows[j][1];
				  		 option_group.append($("<option></option>").attr("value",i + "_" + j).text(pattern_txt));
				  	}
				  	pattern_details.push(rows);
			     });
	       }
	       $(".chosen2").chosen({ allow_single_deselect:true }).change(function(){
	       });
	       $(".chosen2").next().css('font-size','10px');
	       load_pattern_occurrences();
  	});
}

var anomaly_metadata = [];
function show_anomaly_score_selection(){
	 $.ajax({ url: "/rest/metadata/fileitem", context: document.body }).done(function(data) {
		  anomaly_metadata = data["scores"];
		  if(anomaly_metadata.length == 0){
		  	 $("#show_anomalies").prop("checked", false);
       		 $("#show_anomalies").attr("disabled", true);
		  	 $('#anomalies').hide();
		  	  return;
		  }
		  //scores":[{"label":"FPOF(maximal itemsets pc1,pc2)","filename":"lunges_and_sidelunges_vs_squats-007_scores_FPOF_maximal_itemsets_pc1_pc2_.csv",
			//		 "filenamesPatternSets":["lunges_and_sidelunges_vs_squats-006.arff_Charm_MFI_pc1_pc2_5.0_patterns.csv"],"algorithm":"FPOF"}]
		  //add option group and options
		  for(var i=0;i<anomaly_metadata.length;i++){
		       var label = anomaly_metadata[i]['label'];
		       var filename = anomaly_metadata[i]['filename'];
			   $('#anomalies').append($("<option></option>").attr("value",filename).text(label));
	       }
	       $(".chosenAnomalies").chosen({ allow_single_deselect:true }).change(function(){
	       });
	       $('#anomalies').val(anomaly_metadata[0]['filename']);
	       $('#anomalies').trigger("chosen:updated");
	       $("#show_anomalies").prop("checked", true);
	       load_anomaly_scores();
  	});
}

var all_anomaly_scores = {};
function load_anomaly_scores(){
	   all_anomaly_scores = {};
	   for(var i=0;i<anomaly_metadata.length;i++){
		   var fname = anomaly_metadata[i]['filename'];
		   var algorithm = anomaly_metadata[i]['algorithm'];
	 	   $.ajax({ url: "/rest/load-anomaly-score?filename=" + fname, context: document.body, async:false}).done(function(data) {
	 	   		 //data is a table with Window, Score
	 	   		 //e.g. ["0","0.176471"]
	 	   		 var rows = data["rows"].slice(1);
	 	   		 var values = [];
	 	   		 var mini =  1000000;
	 	   		 var maxi = -1000000;
	 	   		 for(var j=0; j<rows.length; j++){
	 	   		 	var window = parseInt(rows[j][0]);
	 	   		 	var score =  parseFloat(rows[j][1]);
	 	   		 	values.push([window, score]);
	 	   		 	mini = Math.min(mini,score)
	 	   		 	maxi = Math.max(maxi,score)
	 	   		 }
	 	   		 if(algorithm == 'FPOF'){
	 	   		 	//For display: scale values between min/max and reverse (e.g. 1.0 is most anomalous)
	 	   		 	var valuesTransformedFPOF = [];
	 	   		 	for(var j=0; j<values.length; j++){
	 	   		 		var window = values[j][0];
	 	   		 		var score =  values[j][1];
	 	   		 		score = (score-mini)/(maxi-mini); //min-max normalisation
	 	   		 		score = 1.0 -score
	 	   		 		valuesTransformedFPOF.push([window, score]);
	 	   		 	}
	 	   		 	values = valuesTransformedFPOF;
	 	   		 }
	 	   		 all_anomaly_scores[fname] = values;
 	 		});
 	   }
}

var all_patterns_occurrences = [];
function load_pattern_occurrences(){
	   all_patterns_occurrences = [];
	   for(var i=0;i<pattern_metadata.length;i++){
		   var patternset = pattern_metadata[i];
		   if(!patternset['filenameOccurrences']){
		   	   all_patterns_occurrences.push([]);
		   	   continue;
		   }
		   var fname = patternset['filename'];
	 	   $.ajax({ url: "/rest/load-pattern-occ?filename=" + encodeURIComponent(fname), context: document.body, async:false}).done(function(data) {
	 	   		 //data is a table with Window, PatternId
	 	   		 var rows = data["rows"].slice(1);
	 	   		 for(var j=0; j<rows.length; j++){
	 	   		 	for(var k=0; k<rows[j].length; k++){
	 	   		 		rows[j][k] = parseInt(rows[j][k]);
	 	   		 	}
	 	   		 }
     			 all_patterns_occurrences.push(rows);
 	 		});
 	   }
}

function get_pattern_occurrences(patternsetIdx, patternIdx){
	var occurrences_patternset = all_patterns_occurrences[patternsetIdx]; //table where [0] is Window, and [1] is PatternIdx
	var occurrences_pattern = [];
	for(var i=0;i<occurrences_patternset.length;i++){
		var occ = occurrences_patternset[i]
		if(occ[1] == patternIdx){
			occurrences_pattern.push(occ[0]);
		}
	}
	return occurrences_pattern;
}


var attributes = 0;
var totalSize = 0;

function get_attributes_and_types(){
  $.ajax({ url: "/rest/metadata/attributes", context: document.body }).done(function(data) {
     attributes = data;
  });
}

var tableau20 = [[31, 119, 180], [174, 199, 232], [255, 127, 14], [255, 187, 120],
             [44, 160, 44], [152, 223, 138], [214, 39, 40], [255, 152, 150],
             [148, 103, 189], [197, 176, 213], [140, 86, 75], [196, 156, 148],
             [227, 119, 194], [247, 182, 210], [127, 127, 127], [199, 199, 199],
             [188, 189, 34], [219, 219, 141], [23, 190, 207], [158, 218, 229]];


//return True, if first non-Nane value is parseable as float
/*
function is_continuous(rows,colIdx){
	for (var rowIdx = 0; rowIdx < rows.length; rowIdx++) {
          var value = rows[rowIdx][colIdx];
          if(value != "?"){
              if(isFloat(value)){
              	return true;
              }
              else{
              	return false;
              }
          }
    }
    return false;
}
*/

function get_values_float(rows, colIdx){
	var values = [];
    for (var rowIdx = 0; rowIdx < rows.length; rowIdx++) {
          var value = rows[rowIdx][colIdx];
          if(value != "?"){
              values.push(parseFloat(value));
          }
          else{
          	  values.push(NaN);
          }
    }
    return values;
}

function get_values_other(rows, colIdx){
	var values = [];
    for (var rowIdx = 0; rowIdx < rows.length; rowIdx++) {
          var value = rows[rowIdx][colIdx];
          if(value != "?"){
              values.push(value);
          }
          else{
          	  values.push(NaN);
          }
    }
    return values;
}

//For Window attribute, each row is associated with one or more windows (more in the case of overlapping windows).e.g. 0, or 0;1 or 1;2
//Get_values_window_column return binary a list of valies, where a non-negative integer values denotes start or end of a window
//At timestamp t and 0 otherwise. Note that in the case of increment 1, everything is a window, but that is ok for now
function parse_arr(value){
	 var value_arr = value.split(";");
	 for(i=0;i<value_arr.length;i++){
	 	value_arr[i] = parseFloat(value_arr[i]);
	 }
	 return value_arr;
}

function get_values_window_column(rows, colIdx){
	var values = [];
    for (var rowIdx = 0; rowIdx < rows.length; rowIdx++) {
          var value = parse_arr(rows[rowIdx][colIdx])
          //boundary conditions:
          if(rowIdx==0 || rowIdx==rows.length-1){
          	 values.push(value[0]);
          	 continue
          }
          //check start of window, i.e. first occurrence of windowID
          var startOrEndWindow = false;
          var startOrEndId = -1;
          var value_prev = parse_arr(rows[rowIdx-1][colIdx]);
          for(i=0;i<value.length;i++){
          	 var found = value_prev.includes(value[i]);
          	 if(!found){
          	 	startOrEndWindow = true;
          	 	startOrEndId = value[i];
          	 }
          }
          //check end of window
          var value_next = parse_arr(rows[rowIdx+1][colIdx]);
          for(i=0;i<value.length;i++){
          	 var found = value_next.includes(value[i]);
          	 if(!found){
          	 	startOrEndWindow = true;
          	 	startOrEndId = value[i];
          	 }
          }
          if(startOrEndWindow){
          	 values.push(startOrEndId);
          }
          else{
          	 values.push(-1);
          }
    }
    return values;
}


//custom chart implementation
function render_chart(pagination, X_column, Y_columns, Y_events, init=false){
 $.ajax({ url: "/rest/load-data?pagination=" + pagination}).done(function(data) {
   		 //update total size (global variable)
   		 totalSize = data['totalSize'];
         var columns = data['rows'][0];
         var rows = data['rows'];
         rows = rows.slice(1);
         //by default select first two columns
         if(X_column.length === 0){
            X_column = columns[0];
         }
         if(Y_columns === null || Y_columns.length === 0){
            Y_columns = [];
            if(init){
            		Y_columns = [columns[1]];
            }
         }
         //make X ticks
         var colIdxForLabel = columns.indexOf(X_column);
         var labelValues = []
         var showXLabels = 30;
         for (var rowIdx = 0; rowIdx < rows.length; rowIdx++) {
            var value =rows[rowIdx][colIdxForLabel];
            if(rowIdx % (Math.floor(rows.length/showXLabels)) != 0)
                value = NaN;
            labelValues.push(format(value));
         }
         //get Y values
         if(!Y_columns){
         	Y_columns = [];
         }
         if(!Y_events){
         	Y_events = [];
         }

         var series = [];
         for (var i = 0; i < Y_columns.length; i++) {
            var columnName = Y_columns[i];
            var colIdx = columns.indexOf(columnName);
            var values = get_values_float(rows, colIdx);
            	series.push(values)
         }
         var event_series = [];
         for (var i = 0; i < Y_events.length; i++) {
         	var columnName = Y_events[i];
            var colIdx = columns.indexOf(columnName);
         	values = get_values_other(rows, colIdx);
         	event_series.push(values)
         }

         //get window values
         var window_values = [];
         if(columns.indexOf("Window") != -1){
         	window_values=get_values_window_column(rows, columns.indexOf("Window"));
         }

         //render
         var canvas = $("#myChart")[0];
        	 var ctx = canvas.getContext("2d");
		 var width = canvas.width;
         var height = canvas.height;
         //clear
         ctx. clearRect(0, 0, width, height);
         //anti-aliased
         ctx.imageSmoothingEnabled= true;
         //paint X labels
         var borderx = 25;
         var bordery = 35;
         var x_offset = (width - 2*borderx)/ rows.length;
         var x = borderx;
         var y = height - bordery * 2;
         var width_label = ctx.measureText(labelValues[labelValues.length-1]).width
         ctx.fillStyle='darkgrey';
         for(var i = 0; i < labelValues.length; i++){
         	labelValue = labelValues[i];
         	if(typeof(labelValue) === "number" && isNaN(labelValue)){
         	}
         	else{
         		ctx.save();
 				ctx.translate(x, y - width_label/2);
 				ctx.rotate(-Math.PI/2);
 				ctx.textAlign = "center";
 				ctx.fillText(labelValue, 0,3); //adding two
				ctx.restore();
         	}
         	x += x_offset;
         }
         //paint X axis lines
         var axis_y = height - width_label - bordery -10; //extra margin 10
         var axis_y_paint = axis_y - bordery + 5;
         //paint x axis line
         ctx.beginPath();
		 ctx.moveTo(borderx, axis_y_paint);
		 ctx.lineTo(width - borderx, axis_y_paint);
		 ctx.strokeStyle='lightgrey';
		 ctx.lineWidth=1;
		 ctx.stroke();

		 //paint ticks
		 var x = borderx;
		 for(var i = 0; i < labelValues.length; i++){
         	labelValue = labelValues[i];
         	if(typeof(labelValue) === "number" && isNaN(labelValue)){
         	}
         	else{
         		ctx.save();
         		ctx.moveTo(x, axis_y_paint-2);
		 		ctx.lineTo(x, axis_y_paint+2);
		 		ctx.stroke();
         	}
         	x += x_offset;
		 }

         //deduce Ymin_Ymax
         var y_max = -1000000;
         var y_min =  1000000;
         for(var i = 0; i < series.length; i++){
         	var columnName = Y_columns[i];
         	if(columnName == "Window"){
         		continue;
         	}
         	else{
         		y_max = Math.max(mymax(series[i]),y_max) * 1.25; //keep some space, extend by 25%
         		y_min = Math.min(mymin(series[i]),y_min) * 1.25;
         	}
         }
         //set range in interface, if empty
         var y_range = $("#y_scale").val();
         if(y_range === ""){
         	$("#y_scale").val(format(y_min) + '-' + format(y_max));
         }
         else{
         	var idx = y_range.lastIndexOf("-")
         	y_min = parseFloat(y_range.substring(0,idx));
     		y_max = parseFloat(y_range.substring(idx+1,y_range.length));
         }

         //aux function for rendering Y values
         function logical_y_to_screen(y){
         	var y_scale = (y - y_min) / (y_max - y_min)
         	var y = axis_y - bordery - y_scale * (axis_y -bordery * 2);
         	return y
         }

         //line plot:
         function line_plot(values,color,stroke_width){
         	var x = borderx + x_offset;
         	ctx.strokeStyle=color;
		    ctx.lineWidth=stroke_width;
         	for(var j = 1; j < values.length; j++){
	         	var x_prev = x - x_offset;
	         	var y_prev = logical_y_to_screen(values[j-1]);
	         	var y = logical_y_to_screen(values[j]);
	         	ctx.beginPath();
				ctx.moveTo(x_prev, y_prev);
				ctx.lineTo(x, y);
				ctx.stroke();
				x += x_offset;
			}
         }

         //line plot for sparse timeseries, e.g. lot's of NaN values
         function line_plot_sparse(values,color,stroke_width){
         	var x = borderx + x_offset;
         	ctx.strokeStyle=color;
		    ctx.lineWidth=stroke_width;
         	for(var j = 1; j < values.length; j++){
         		if(Number.isNaN(values[j])){
         			x += x_offset;
         			continue;
         		}
         		//Find x_prev that is not Nan
         		var x_prev = x - x_offset;
	         	var y_prev = logical_y_to_screen(values[j]);
         		for(var l = j-1; l >= 0; l--){
         			if(!Number.isNaN(values[l])){
         				y_prev = logical_y_to_screen(values[l])
         				break;
         			}
         			x_prev = x_prev - x_offset;
         		}
	         	var y = logical_y_to_screen(values[j]);
	         	ctx.beginPath();
				ctx.moveTo(x_prev, y_prev);
				ctx.lineTo(x, y);
				ctx.stroke();
				draw_symbol(x,y,'dot',color,size=3)
				x += x_offset;
			}
         }

         function draw_symbol(x,y,symbol,color,size=6){
     	   if(symbol == "dot"){
         	    ctx.beginPath();
				ctx.arc(x, y, size-1, 0, 2 * Math.PI);
				ctx.fillStyle=color;
				ctx.fill();
			}
			else if(symbol == "cross"){
				var d = size/2;
         	    ctx.beginPath();
         	    ctx.moveTo(x-d,y-d);
         	    ctx.lineTo(x+d,y+d);
         	    ctx.moveTo(x-d,y+d);
         	    ctx.lineTo(x+d,y-d);
         	    ctx.strokeStyle=color;
				ctx.stroke();
			}
			//todo fix bug wit square
			else{
				var d = size/2;
         	    ctx.beginPath();
         	    ctx.moveTo(x-d,y-d);
         	    ctx.lineTo(x+d,y+d);
         	    ctx.moveTo(x-d,y+d);
         	    ctx.lineTo(x+d,y-d);
         	    ctx.strokeStyle=color;
				ctx.stroke();
			}
			//else if(symbol == "square"){
         	//    ctx.fillStyle=color;
         	//   ctx.fillRect(x, y, d+1, d+1);
			//}
         }

         //render events
         function discrete_plot(values, symbol, index){
         	var x = borderx + x_offset;
		    //make set of discrete values
		    var uniqueValues = new Set();
		    for(var j = 0; j < values.length; j++){
         		if(!Number.isNaN(values[j])){
         			uniqueValues.add(values[j]);
         		}
         	}
         	var uniqueValuesArr = Array.from(uniqueValues);
         	uniqueValuesArr.sort();
         	var number_of_items_in_legend_for_space = 15; //if less than 15 types of events -> place whitespace
         	if(uniqueValuesArr.length > number_of_items_in_legend_for_space){ //else -> make room for more events
         		number_of_items_in_legend_for_space = uniqueValuesArr.length
         	}
         	//render legend
         	for(var k=0; k<uniqueValuesArr.length; k++){
         		var value = uniqueValuesArr[k];
         		var relative_index = (k+1) / number_of_items_in_legend_for_space;
         		var y_center = logical_y_to_screen((y_max+y_min) * 0.99 * relative_index);
         		var x_legend = (rows.length * 0.90) * x_offset;
         		if(index > 0){ //move legend to right if multiple discrete event plots
         		 	x_legend += index * 50; //index * 50 * x_offset;
         		}
         		var colorIdx = k  % tableau20.length;
         		if(isFloat(value) && value > 5){ //if value if integer (e.g. event code), use color based on event code for consistency
         			colorIdx = value  % tableau20.length;
         		}
         		var color = 'rgb(' + tableau20[colorIdx][0] + ',' + tableau20[colorIdx][1] + ',' + tableau20[colorIdx][2] + ')';
         		//draw symbol for legend
         		draw_symbol(x_legend - 10,y_center,symbol,color)
         		//draw label for legend
         		ctx.beginPath();
				ctx.fillStyle=color;
				//ctx.textAlign = "center";
         	    ctx.textBaseline = 'middle';
				ctx.fillText(value, x_legend, y_center);
         	}
         	//render points
         	for(var j = 0; j < values.length; j++){
         		if(Number.isNaN(values[j])){
         			x += x_offset;
         			continue;
         		}
         		var value = values[j]
         		//y position determined by index in uniqueValues
         		var relative_index = (uniqueValuesArr.indexOf(value)+1) / number_of_items_in_legend_for_space;
         		var y_center = logical_y_to_screen((y_max+y_min)* 0.9 * relative_index);
         		var colorIdx = uniqueValuesArr.indexOf(value)  % tableau20.length;
         		if(isFloat(value) && value > 5){
         			colorIdx = value  % tableau20.length;
         		}
         		var color = 'rgb(' + tableau20[colorIdx][0] + ',' + tableau20[colorIdx][1] + ',' + tableau20[colorIdx][2] + ')';
         		//draw symbol, e.g. ['dot', 'cross', 'square']
         		draw_symbol(x,y_center,symbol,color)
				x += x_offset;
			}
         }

         //print vertical line if value[i] >= 1
         function find_screen_coordinates_windows(values){
            if(!values){
            		return [];
            	}
         	var windows = [];
         	var x = borderx;
         	for(var j = 0; j < values.length; j++){
	         	if(values[j] >= 0){
					var next_x = x;
					var found = false;
					for(var z = j+1; z < values.length; z++){
						if(values[z] >= 0 && values[z] == values[j]){
							found = true;
							next_x += x_offset;
							break;
						}
						next_x += x_offset;
					}
					//draw rectangle and label for
					if(found){
						windows.push([values[j], x, next_x]);
					}
				}
				x += x_offset;
			}
			return windows;
         }

         //windows is an array of triplets, where first value is window value (>0),
         // and second, thirdh coordinates are the x range in screen coordinates
         function plot_windows(windows,stroke_width){
		    ctx.lineWidth=stroke_width;
         	for(var j = 0; j < windows.length; j++){
         		var x = windows[j][1]
         		var next_x = windows[j][2];
         		var valueJ = windows[j][0];
	         	//choose 5 colors from tableau (for overlapping windows)
	         	var colorIdx = (valueJ % 5) + 10;
	         	var color = 'rgba(' + tableau20[colorIdx][0] + ',' + tableau20[colorIdx][1] + ',' + tableau20[colorIdx][2] + ',0.3)';
	         	ctx.strokeStyle=color;
	         	ctx.fillStyle=color;
	         	//show vertical lines
	         	ctx.beginPath();
				ctx.moveTo(x, logical_y_to_screen(y_min));
				ctx.lineTo(x, logical_y_to_screen(y_max));
				ctx.stroke();
				ctx.moveTo(next_x, logical_y_to_screen(y_min));
				ctx.lineTo(next_x, logical_y_to_screen(y_max));
				ctx.stroke();
				//show horizontal lines
				ctx.moveTo(x, logical_y_to_screen(y_min));
				ctx.lineTo(next_x, logical_y_to_screen(y_min));
				ctx.stroke();
				ctx.moveTo(x, logical_y_to_screen(y_max));
				ctx.lineTo(next_x, logical_y_to_screen(y_max));
				ctx.stroke();
				//show label
				ctx.fillText("W", (x + next_x)/2, logical_y_to_screen(y_max)-8);
				ctx.fillText(valueJ,(x + next_x)/2 + 9, logical_y_to_screen(y_max) -4);
			}
		 }

		 function plot_labels(values){
         	var x = borderx + x_offset;
         	for(var j = 0; j < values.length; j++){
	         	var label = values[j]; //1 if anomaly, -1 is no anomaly, 0 or NAN is non
	         	if(label == 1 || label == -1){
	         		ctx.fillStyle="rgba(0, 255, 0, .05)"; //green
	         		if(label == 1){
	         			ctx.fillStyle="rgba(255, 0, 0, .3)";//red
	         		}
	         		var width_bar = x_offset;
	         		var height_bar = logical_y_to_screen(y_min) - logical_y_to_screen(y_max);
	         		ctx.fillRect(x, logical_y_to_screen(y_max), width_bar, height_bar);
	         	}
				x += x_offset;
			}
		 }

		 //e.g. [[0, 0.1], [1, 0.9], [2, 0.5] ...]
         function plot_anomaly_score(anomaly_scores,color,stroke_width,dash=[]){
         	ctx.strokeStyle=color;
         	ctx.setLineDash(dash);
		    ctx.lineWidth=stroke_width;
         	for(var i=0; i < anomaly_scores.length-1; i++){
         		var window = anomaly_scores[i][0];
         		var score = anomaly_scores[i][1];
         		var windowNext = anomaly_scores[i+1][0];
         		var scoreNext = anomaly_scores[i+1][1];
         		var x_range = window_to_screen(window);
         		var x_range_next = window_to_screen(windowNext);
         		if(x_range && x_range_next){ //can be null if outside x range
         			var x_center_window = (x_range[0] + x_range[1])/2;
         			var x_center_window_next = (x_range_next[0] + x_range_next[1])/2;
         			//like logical_y_to_screen, but no scaling based on y-min/y-max
         			var scale = 0.95
         			var y = axis_y - bordery - score * scale * (axis_y -bordery * 2);
         			var y_next = axis_y - bordery - scoreNext * scale * (axis_y -bordery * 2);
         			//plot line
         			ctx.beginPath();
					ctx.moveTo(x_center_window, y);
					ctx.lineTo(x_center_window_next, y_next);
					ctx.stroke();
         		}
         	}
         	ctx.setLineDash([]);
         }
         //render patterns
         var windows = find_screen_coordinates_windows(window_values)
         function window_to_screen(window){
         	for(var i=0; i<windows.length; i++){
         		if(windows[i][0] == window){
         			return windows[i].slice(1); //return x and next_x in screen coordinates
         		}
         	}
         	return null;
         }

         //render windows
         if($("#show_windows").prop("checked")){
               var windows = find_screen_coordinates_windows(window_values)
         	   plot_windows(windows,color,0.5);
         }

         //render labels
         if($("#show_labels").prop("checked")){
         	   var colIdx = columns.indexOf("label");
               var values = get_values_float(rows, colIdx);
         	   plot_labels(values);
         }

         //render anomaly scores
         if($("#show_anomalies").prop("checked")){
         	var selected_score_fnames = $("#anomalies").val();
         	if(!selected_score_fnames){
         		selected_score_fnames = [];
        		}
         	var colorsGrey = ['rgb(71, 81, 72)', 'rgb(75, 76, 75)', 'rgb(59, 61, 59)', 'rgb(97, 102, 97)'] //some hand-picked dark greys
         	var dashes = [[],[5,7],[7,8]];
         	for(var k=0; k < selected_score_fnames.length; k++){ //show possibly multiple scores
         		//var color = 'rgb(' + tableau20[k+10][0] + ',' + tableau20[k+10][0] + ',' + tableau20[k+10][0] + ')'; //grey
         		var fname = selected_score_fnames[k];
         		var anomaly_scores = all_anomaly_scores[fname]; //e.g. [[0, 0.1], [1, 0.9], [2, 0.5] ...]
	         	ctx.lineWidth=2.0;
	         	var color = colorsGrey[k % 4];
	         	var dash = dashes[k%3];
         		plot_anomaly_score(anomaly_scores,color,2.0, dash);
         	}
         }

         //render Y "lines"
         for(var i = 0; i < series.length; i++){
         	var values = series[i];
         	var color = 'rgb(' + tableau20[i][0] + ',' + tableau20[i][1] + ',' + tableau20[i][2] + ')';
         	var columnName = Y_columns[i]; //see series
            var colIdx = columns.indexOf(columnName);
         	var has_nans = false;
         	//sparse or not sparse
         	for(var j = 0; j < values.length; j++){
         		if(Number.isNaN(values[j])){
         			has_nans = true;
         			break;
         		}
         	}
         	if(!has_nans){
         		line_plot(values,color,1.5);
         	}
         	else{
         		line_plot_sparse(values,color,1.5);
         	}
         }

         //render Y events
         var symbols = ['dot', 'cross', 'square'];
         for(var i = 0; i < event_series.length; i++){
         	var values = event_series[i];
         	var symbol = symbols[i % symbols.length];
         	discrete_plot(values,symbol,i);
         }

         //render patterns
		 var selected_patterns = $('#patterns').val();
		 if(selected_patterns){
		 	 //0. rescale pattern 'item' values between max item and min item, voor visualisation
	         var min_pattern = 1000000;
	         var max_pattern = -1000000;
	         for(var i=0;i<selected_patterns.length;i++){
	         	var selected = selected_patterns[i]; //value consist of tuples of form i_j, where i is index into pattern_metadata, and j is patternID
				var patternsetIdx = parseInt(selected.split('_')[0]);
				var patternIdx = parseInt(selected.split('_')[1]);
				var pattern = pattern_details[patternsetIdx][patternIdx][0].split(' '); //e.g. like value=1 value=10
				for(var patternItemIdx=0; patternItemIdx<pattern.length; patternItemIdx++){
         	    		var patternItem = pattern[patternItemIdx];
         	    		var value = parseFloat(patternItem.split('=')[1]);
         	    		if(value){
         	    			max_pattern = Math.max(max_pattern,value);
         	    			min_pattern = Math.min(min_pattern,value);
         	    		}
		        }
			 }
			 ctx.font = "13px Arial";
			 for(var i=0;i<selected_patterns.length;i++){
				var selected = selected_patterns[i]; //value consist of tuples of form i_j, where i is index into pattern_metadata, and j is patternID
				var patternsetIdx = parseInt(selected.split('_')[0]);
				var patternIdx = parseInt(selected.split('_')[1]);
				//1.filter selected occurrences
				var patternWindows = get_pattern_occurrences(patternsetIdx, patternIdx); //return list of window, e.g. [0,1,4,..]
			 	//2.render
			 	var colorIdx = (i + 8) % 20;
	         	var color = 'rgb(' + tableau20[colorIdx][0] + ',' + tableau20[colorIdx][1] + ',' + tableau20[colorIdx][2] + ')';
	         	ctx.fillStyle=color;
	         	ctx.strokeStyle=color
	         	for(var winIdx=0; winIdx<patternWindows.length; winIdx++){
	         	    var window = patternWindows[winIdx];
	         	    var x_range = window_to_screen(window);
	         	    if(x_range){ //can be null if outside x range
		         	    var x = x_range[0];
		         	    var next_x = x_range[1];
		         	    var x_center = (x + next_x)/2;
		         	    var y_center = logical_y_to_screen((y_max+y_min)/2);
		         	    //compute y coordinates "closest" to sequence, e.g. if time series between 1-10 (in logical coordinates), and
		         	    //pattern like 1-2-3 -> take average value of 2 (in logical coordines)
		         	    //Note: only works for patterns mined in same range as source... -> do scaling
		         	    var pattern = pattern_details[patternsetIdx][patternIdx][0].split(' '); //e.g. like value=1 value=10
		         	    var sum_pattern = 0;
		         	    var count_pattern = 0;
		         	    for(var patternItemIdx=0; patternItemIdx<pattern.length; patternItemIdx++){
		         	    		var patternItem = pattern[patternItemIdx];
		         	    		var value = parseFloat(patternItem.split('=')[1]);
		         	    		if(value){
		         	    			sum_pattern += value;
		         	    			count_pattern += 1;
		         	    		}
		         	    }
		         	    //if(i == 3){
		         	    	//	sum_pattern +=0.5;
		         	    	//}
						if(count_pattern > 0){
							var avg = sum_pattern / count_pattern
							avg = (avg - min_pattern) / (max_pattern - min_pattern + 1) //minmax scaling
							y_center =  logical_y_to_screen((y_max - y_min) * avg);
						}
		         	    //draw label and arc
		         	    ctx.textAlign = "center";
		         	    ctx.textBaseline = 'middle';
		         	    ctx.beginPath();
						ctx.arc(x_center, y_center, 10, 0, 2 * Math.PI);
						ctx.stroke();
						ctx.fillStyle="rgba(255, 255, 255, .7)";
						ctx.fill();
						ctx.fillStyle=color;
						ctx.fillText(patternIdx, x_center, y_center);
					}
				}
	         }
         }

  }); //end ajax
} //end function

function show(){
	var pagination=$("#pagination").val();
    var x_column=$("#x_column").val();
    var y_columns=$("select#y_columns").val();
    var y_events=$("select#y_events").val();
    render_chart(pagination,x_column,y_columns, y_events);
}

$( document ).ready(function() {
   get_attributes_and_types();
   //var columns = data['rows'][0];
   render_chart("0-1000","",[],[], true);
   $("#prev" ).attr("disabled", true);
   show_column_selections();
   show_patternset_selection();
   show_anomaly_score_selection();
   $("#select_columns" ).submit(function( event ) {
      event.preventDefault();
      show();
   });
   $("#next" ).click(function( ) {
      var pagination=$("#pagination").val();
      from = parseInt(pagination.split("-")[0]);
      to = parseInt(pagination.split("-")[1]);
      new_to =  to + (to -from);
      if(new_to > totalSize){
      	new_to =totalSize;
      	$("#next" ).attr("disabled", true);
      }
      $("#prev" ).attr("disabled", false);
      var new_pagination = to.toString() + "-" + new_to.toString()
      $("#pagination").val(new_pagination);
      show();
   });
   $("#prev" ).click(function( ) {
      var pagination=$("#pagination").val();
      from = parseInt(pagination.split("-")[0]);
      to = parseInt(pagination.split("-")[1]);
      new_from =  from - (to -from);
      if(new_from <= 0){
         new_from = 0;
         $("#prev" ).attr("disabled", true);
      }
      $("#next" ).attr("disabled", false);
      var new_pagination = new_from.toString() + "-" + from.toString()
      $("#pagination").val(new_pagination);
      show();
   });
   $("#select_all").on('click', function(){
    		$('#patterns').find('option').prop('selected', true).parent().trigger('chosen:updated');
    });
   $("#y_columns").change(function() {
   		$("#y_scale").val('');
   		show();
    });
    $("#y_events").change(function() {
   		show();
    });

});

$(document).keypress(function(e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    if (keycode == '13') {
        show();
        e.preventDefault();
    }
});

</script>
<div id="container">
   <div id="content">

   <div class="actions" style="max-width: 1190px; margin-left:10px">
   <form id="select_columns" method="POST" enctype="multipart/form-data" action="/" >
       <span class="plotaction">X column</span>
       <select name="x_column" id="x_column" class="chosen" style="width: 250px">
       </select>
       <br/>
       <span class="plotaction">Y columns</span>
       <select name="y_columns" id="y_columns" multiple="true" class="chosen" style="width: 250px"
       		   data-placeholder="Select one more columns">
       </select>
       <span style="width: 100px; display:inline-block;">&nbsp;Y range</span>
       <input class="field" style="width: 150px" type="text" name="y_scale" id="y_scale" placeholder="default"></input>
       <br/>
       <span class="plotaction">Y events</span>
       <select name="y_events" id="y_events" multiple="true" class="chosen" style="width: 250px"
       		   data-placeholder="Select one more columns">
       </select>
       <br/>
       <span class="plotaction">Show</span>
       <!-- see https://stackoverflow.com/questions/4137255/checkboxes-in-web-pages-how-to-make-them-bigger -->
       <div class="double" style="display:inline-block; margin-bottom: 5px;">
       	   <input type="checkbox" id="show_windows" name="show_windows" checked/>
       </div> Windows
       <div class="double" style="display:inline-block; margin-bottom: 5px;">
       	   <input type="checkbox" id="show_labels" name="show_labels"/>
       </div> Labelled anomalies
       <div class="double" style="display:inline-block; margin-bottom: 5px;">
       	   <input type="checkbox" id="show_anomalies" name="show_anomalies"/>
       </div> Anomaly score
       <select name="anomalies" id="anomalies" multiple="true" class="chosenAnomalies" style="width: 300px"
       		   data-placeholder="Select one more scores">
       </select>
       <br/>
       <span class="plotaction">Patterns</span>
       <select name="patterns" id="patterns" multiple="true" class="chosen2" style="width: 900px">
       </select>
       <button class="button" id="select_all">Select all</button>
       <br/>
       <span class="plotaction">X range</span>
       <input class="field" style="width: 100px" type="text" name="pagination" id="pagination" value="0-1000"/>
       <input class="button" type="submit" value="Show"/>
       <input class="button" type="submit" value="&lt; Prev" id="prev"/>
       <input class="button" type="submit" value="&gt; Next" id="next"/>
   </form>
   </div>

   <div style="padding: 10px">
   <canvas id="myChart" width="1200" height="400" style="border: 1px solid lightgrey"></canvas>
   </div>
   </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>
