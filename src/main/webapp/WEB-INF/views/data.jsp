<html>
<body>
<%@include file="header.jsp" %>
<!-- DataTables -->

<script>

var window_modus = false;

function has_window(){
	 $.ajax({ url: 'rest/metadata/attributes', async:false}).done(function(data) { //runn async
	    var found_window = false;
	 	for(var i=0; i<data.length; i++){
	 		if('Window' in data[i]){
	 			found_window = true;
	 		}
	 	}
	 	window_modus = found_window;
	 });
}

//used by two variations of show data
function process_data(data){
	 	 var totalSize = data['totalSize'];
         var rows = data['rows'];
         rows = rows.slice(1);
         //parse floats and by default show 3 decimals
         for(var i=0;i<rows.length;i++){
         	for(var j=0;j<rows[i].length;j++){
         		rows[i][j] = format(rows[i][j])
         		if(rows[i][j].includes(';')){
         			//for values contains ':', i.e. vector values -> parse/format/join
         			var vals = rows[i][j].split(';');
         			for(var k=0; k<vals.length; k++){
         				if(vals[k] == '?'){
         					vals[k] = ' ';
         				}
         				else{
         					vals[k] = format(vals[k]);
         				}
         			}
         			rows[i][j] = vals.join(' ');
         			if(rows[i][j].length > 128){
         				rows[i][j] = rows[i][j].substring(0,128) + '...'
         			}
         		}
         	}
         } 
         $('#footerdata_status').empty();
         $('#footerdata_status').append('Loaded ' + rows.length + '/' + totalSize);
         return rows
}

//show data table first time: init columns and DataTable
function render_data(pagination)
{
   show_start_loading("#data_status");
   var rest = window_modus?"/rest/load-data-windows":"/rest/load-data";
   $.ajax({ url: rest + "?pagination=" + pagination}).done(function(data) {
         show_stop_loading("#data_status");
         //get columns
         var rows = data['rows'];
         var columns = []
         for(var j=0;j<rows[0].length;j++){
            columns.push({title : rows[0][j]});
         }
         rows = process_data(data)
    
        var table = $('#data').DataTable({
            "iDisplayLength": 10,
            "data":rows,
            "columns": columns,
            "responsive": true
         });
         
         //NEW: search for each column, see https://datatables.net/extensions/fixedheader/examples/options/columnFiltering.html
         $('#data thead').append('<tr></tr>');
         for(var j=0;j<columns.length;j++){
         	$('#data thead tr:eq(1)').append('<th style="padding: 2px"></th>');
         }
    		 $('#data thead tr:eq(1) th').each( function (i) {
        		var title = $(this).text();
        		$(this).html( '<input type="text" placeholder="search ' +title+'" style="width:75px; border: 0px"/>' );
 			$( 'input', this ).on( 'keyup change', function () {
            		if ( table.column(i).search() !== this.value ) {
                		table
                    .column(i)
                    .search( this.value )
                    .draw();
            		}
        		});
    		}); 
       
		}).fail(function(data){
        show_error("#data_status");
    });
}

//show data clicking "load more"
function refresh_table(pagination){
  show_start_loading("#data_status");
  var rest = window_modus?"/rest/load-data-windows":"/rest/load-data";
  $.ajax({ url: rest + "?pagination=" + pagination }).done(function(data) {
         show_stop_loading("#data_status");
         rows = process_data(data)
         var table = $('#data').dataTable();
         table.fnClearTable();
         table.fnAddData(rows);
         table.fnDraw();   
   }).fail(function(data){
         show_error("#data_status");
   }); 
}

$( document ).ready(function() {
   console.log( "document loaded" );
   has_window();
   $('#window_checkbox').prop( "checked", window_modus );
   render_data("");
   $("#load_more" ).submit(function( event ) {
      event.preventDefault();
      var pagination=$("#pagination").val();
      refresh_table(pagination);
   });
   $('#window_checkbox').change(function() {
        if($(this).is(":checked")) {
            window_modus = true;
        }
        else{
        		window_modus = false;
        }     
        var pagination=$("#pagination").val();
      	refresh_table(pagination); 
    });
});
</script>
<style>
th.sorting {
		word-break: break-all; 
}
</style>
<div id="container">
 <div id="content">

   <div style="padding: 10px; max-width: 1200px">
  	 <div id="headerdata"></div>
  	 <input type="checkbox" id="window_checkbox" name="window" value="window"/>Show Windows <br/>
  	 <br/>
  	 <table id="data" class="display condensed" cellspacing="0" style="font-size: 0.9em; width: 100%; word-wrap:break-word;table-layout: fixed;">
  	 	<thead></thead>
  	 </table>
  	 <div id="data_status"></div>
  	 
  	 <div>
   		<form style="margin: 0; padding: 0;" id="load_more" enctype="multipart/form-data" method="POST" action="/" >
       		<span id="footerdata_status" style="display: block-inline"></span>
       		<span style="padding-left:15px">Load more:     
       		<input class="field" style="width:150px" type="text" name="pagination" id="pagination" value="0-10000"/>
       		<input class="button" type="submit" value="Load"/> 
       </form>
  	 </div>
   </div>
  </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>