<html>
<body>
<%@include file="header.jsp" %>
<script>

function refresh(data){
  window.location.href = "/transform";
}

//Page-specific stuff
function convert_to_arff(){
  url = '/rest/transform/convert-to-arff';
  status_element_id = "#convert_to_arff_status";
  do_basic_post_with_reporting_call(url,status_element_id,{},refresh);
}

function drop_null_columns(){
  url = '/rest/transform/drop-null-columns';
  status_element_id = "#drop_null_columns_status";
  do_basic_post_with_reporting_call(url,status_element_id,{},refresh);
}

function run_date_transform(dateformat_from, dateformat_to){
  url = '/rest/transform/run-date-transform';
  status_element_id = "#transform_date_status";
  do_basic_post_with_reporting_call(url,status_element_id,{dateformat_from: dateformat_from, dateformat_to: dateformat_to},refresh);
}


function translate_labels(labels_txt){
  url = '/rest/transform/translate-labels';
  status_element_id = "#translate_labels_status";
  do_basic_post_with_reporting_call(url,status_element_id,{labels_txt: labels_txt},refresh);
  //also load_schema?
}

function transform_columns(columns,transforms){
  url = '/rest/transform/transform-columns';
  status_element_id = "#transform_columns_status";
  do_basic_post_with_reporting_call(url,status_element_id, {columns: columns.join(), transforms: transforms.join()},refresh);
   //also load_schema?
}

 function replace_outlier(column,smaller,smaller_replace,larger,larger_replace){
   url = '/rest/transform/replace-outlier';
   status_element_id = "#replace_outlier_status";
   do_basic_post_with_reporting_call(url,status_element_id, {column: column, smaller:smaller, smaller_replace:smaller_replace, larger:larger, larger_replace:larger_replace},refresh);
 }

function transform_discretize(columns,density,bins){
   url = '/rest/transform/discretize';
   status_element_id = "#transform_discretize_status";
   do_basic_post_with_reporting_call(url,status_element_id, {columns: columns, density:density, bins:bins},refresh);
 }

function transform_split(nullpatterns){
   url = '/rest/transform/split';
   status_element_id = "#transform_split_status";
   do_basic_post_with_reporting_call(url,status_element_id, {nullpatterns: nullpatterns.join(';')},refresh);
}

function transform_join(files){
   url = '/rest/transform/join';
   status_element_id = "#transform_join_status";
   do_basic_post_with_reporting_call(url,status_element_id, {file: files},refresh);
}

function transform_paa(columns, window){
   url = '/rest/transform/paa';
   status_element_id = "#transform_paa_status";
   do_basic_post_with_reporting_call(url,status_element_id, {columns: columns, window: window},refresh);
}

function transform_make_windows(window, increment){
   url = '/rest/transform/make-windows';
   status_element_id = "#transform_make_windows_status";
   do_basic_post_with_reporting_call(url,status_element_id, {window: window, increment: increment},refresh);
}

function transform_filter_windows(parameters){
   var myJSON = JSON.stringify(parameters);
   url = '/rest/transform/filter-windows';
   status_element_id = "#filter_windows_status";
   do_basic_post_with_reporting_call(url,status_element_id, {'parameters': myJSON},refresh);
}

function transform_group_by_window(parameters){
   var myJSON = JSON.stringify(parameters);
   url = '/rest/transform/groupby-windows';
   status_element_id = "#groupby_windows_status";
   do_basic_post_with_reporting_call(url,status_element_id, {parameters: myJSON},refresh);
}

function run_corr(remove){
  var url = '/rest/mining/run-corr';
  var status_element_id = "#run_corr_status";
  var jsondata = {remove:remove};
  do_basic_post_with_reporting_call(url,status_element_id,jsondata,refresh);
}

function prettyPrintNumberIfApplicable(x){
                    if(Number(x) !== x){
                      return x;
                    }
                    if(x>10000000 || x< -10000000)
                        return round(x/1000000) + "m";
                    if(x>100000 || x< -100000)
                        return round(x/1000) + "k";
                    else
                      return round(x);
}

function prettyPrint(x){
	if(isFloat(x)){
		return prettyPrintNumberIfApplicable(parseFloat(x));
	}
	else{
		if(x.length > 8){
			return x.substring(0,6) + '..';
		}
		else{
			return x;
		}
	}
}

function reduce_histogram(histo, histo_labels){
	var new_labels = []
	var new_values = []
	var increment = Math.floor(histo.length / 11)
	for(var i=0; i<histo.length; i+=increment){
		var label = histo_labels[i] + '-' + histo_labels[Math.min(i+increment-1, histo_labels.length-1)];
		var count = 0;
		for(var j=i; j<i+increment && j<histo.length; j++){
			count += histo[j]
		}
		new_labels.push(label)
		new_values.push(count)
	}
	return {'histo': new_values, 'histo_labels': new_labels}
}


function load_schema(){
   url = '/rest/metadata/attribute-and-statistics';
   status_element_id="#schema_status";
   show_start_loading(status_element_id);
   $.get( url, {}).done(function(data ) {
       show_stop_loading(status_element_id);
       var tablebody = $("#schema").find("tbody");
       tablebody.empty();
       jQuery.each(data, function(index, obj) {
          var name = obj.attribute;
          var type = obj.type
          var long_type = type;
          if(type.length > 50){
            type = type.substring(0,47) + "..."
          }
          else{
            long_type = ""
          }
          var properties_txt = ""
          var histo_txt = "<td></td>";
          if("mean" in obj){
            properties_txt = "<span style=\"width: 60px; display:inline-block\">Mean:</span>" + parseFloat(obj.mean).toFixed(2) + "<br/>" +
            				     "<span style=\"width: 60px; display:inline-block\">Std:</span>" + parseFloat(obj.std).toFixed(2) + "<br/>"
            /*
histogram":"8,6,9,7,8,10,65,1405166,478723,30",
"std":"25781.185599122327","min":"0.0","max":"1381700.0","mean":"1100233.769737579","noDistinct":"225",
"support-first-value":"1146800:17427","attribute":"MP509","noNotNull":"1884033","type":"INTEGER","noValues":"1884861",
"histogram-labels":"0.0,138170.0,276340.0,414510.0,552680.0,690850.0,829020.0,967190.0,1105360.0,1243530.0,1381700.0"}
            */
          }
          if("histogram" in obj){
            var histo = obj["histogram"].split(",").map(parseFloat);
            var max_of_array = Math.max.apply(Math, histo)
            var histo_labels = obj["histogram-labels"].split(",").map(prettyPrint)
            if(histo.length > 11){
            		d = reduce_histogram(histo, histo_labels)
            		histo = d['histo']
            		histo_labels = d['histo_labels']
            }
            var title=''
            var histo_txt = '<td title="' + title + '"><canvas id="histo_'+ name + '" width="300" height="120"></canvas></td>'
          }
          if("distribution" in obj){
          	var distribution = obj.distribution;
          	if(distribution.length > 50){
            		distribution = distribution.substring(0,47) + "..."
          	}
          	properties_txt = "<span style=\"width: 60px; display:inline-block\">Counts: </span>" + distribution + "<br/>";
          }
          if(index != 0){
	          tablebody.append("<tr><td>" + name +  "</td>" +
	              "<td title='" + long_type + "'>" + type + "</td>" +
	              "<td>" + "<span style=\"width: 60px; display:inline-block\">#Unique: </span>" +  obj.noDistinct + "/" + obj.noValues + "<br/>" +
	              		   "<span style=\"width: 60px; display:inline-block\">#Null: </span>" + (obj.noValues - obj.noNotNull) + "/" + obj.noNotNull + "<br/>" +
	              		    properties_txt
	              + "</td>" +
	              histo_txt +
	              +"</tr>");
	           show_histogram('#histo_'+ name, histo, histo_labels);
           }
           else{
           	 tablebody.append("<tr><td>" + name +  "</td>" +
	              "<td title='" + long_type + "'>" + type + "</td>" +
	              "<td><span style=\"width: 60px; display:inline-block\">First:</span>" + format(obj.min) + "<br/>" +
            			  "<span style=\"width: 60px; display:inline-block\">Last:</span>" + format(obj.max) + "<br/>" +
	              "<td></td></tr>");
           }
       });
    }).fail(function(data){
       show_error(status_element_id);
    });
}


function show_column_selections(id,clazz){
  $.ajax({ url: "/rest/metadata/attributes", async: false, context: document.body }).done(function(data) {
       for (var i = 0; i < data.length; i++ ) {
           var column = Object.keys(data[i])[0];
           $(id).append($("<option></option>").attr("value",column).text(column));
       }
       $("." + clazz).chosen();
       $("." + clazz).css('width','150px');
       $("." + clazz).next().css('font-size','11px');
  });
}

function show_file_selections(){
	$.ajax({ url: "/rest/projects", context: document.body }).done(function(projects) {
       for(var i=0;i<projects.length;i++){
          var project = projects[i];
          var project_name = project['name'];
          for(var j=0;j<project['fileItems'].length;j++){
             var id = project['fileItems'][j]['id'];
              var logical_name = project['fileItems'][j]['logicalName'];
              var version = project['fileItems'][j]['version'];
              var noRows =  project['fileItems'][j]['noRows'];
              var noColumns = project['fileItems'][j]['noColumns'];
              var txt = project_name + '/' + logical_name + '-' + version;
   		      $('#selected_files').append($("<option></option>").attr("value",txt).text(txt + '(#rows=' + noRows + ')'));
           }
       }
       $(".chosenfs").chosen();
       $(".chosenfs").css({"width": "150px"});
       $(".chosenfs").next().css('font-size','11px');
   });
}

function get_metadata_attribute(attribute){
  var result = {};
 $.ajax({ url: "/rest/metadata/attribute-and-statistics", context: document.body,  async: false}).done(function(data) {
       for (var i = 0; i < data.length; i++ ) {
           var column = data[i]["attribute"]
           if(column == attribute){
           	   result = data[i];
           }
       }
  });
  return result;
}

<!-- for SQL --~
function run_sql_query(query){
  url = '/rest/transform/run-sql-query';
  status_element_id = "#run_sql_query_status";
  do_basic_post_with_reporting_call(url,status_element_id,{query:query},refresh);
}

function init_query(){
  $.ajax({ url: "/rest/metadata/attributes", async: false, context: document.body }).done(function(data) {
       var init_query = 'SELECT ';
       for (var i = 0; i < data.length; i++ ) {
           var column = Object.keys(data[i])[0];
           init_query += column.toLowerCase();
           if(i!=data.length -1){
                init_query += ', ';
           }
           else{
           	    init_query += ' ';
           }
       }
       if(data.length >0){
       	   init_query += '\nFROM file\nORDER BY ' +  Object.keys(data[0])[0].toLowerCase() + '\n'
       	   $('#sql').val(init_query)
       }

  });
}


$( document ).ready(function() {
  $("#run_sql_query" ).submit(function( event ) {
      event.preventDefault();
      var query=$("#sql").val();
      run_sql_query(query);
  });
  init_query();
});

function load_null_patterns(){
  $.ajax({ url: "/rest/metadata/nullpatterns",context: document.body }).done(function(data) {
		   for (var i = 0; i < data.length; i++ ) {
		   	  var pattern = data[i][0];
    		      var support = data[i][1];
			  $('#null_patterns').append('<input type="checkbox" id="null_pattern1" name="null_pattern1" value="' + pattern + '"/>' + pattern + ' (' + support + ')<br/>');
           }
   });
}

function show_histogram(id,data_row_values, data_row_legend){
  console.log(data_row_values)
  if( !$(id).length )
    return;
  var ctx = $(id).get(0).getContext("2d");
  var options = {barShowStroke: false};
  var data = {labels: data_row_legend,
              datasets: [{data: data_row_values}]
              };
  var myLineChart = new Chart(ctx).Bar(data, options);
}

//check if CSV file, and if so, disable everything
function check_and_show_file_type(){
	 $.ajax({ url: "/rest/metadata/fileitem",context: document.body }).done(function(data) {
	 	   var is_csv = data["csv"] == true;
		   if(is_csv){
		   		$('.arff').hide();
		   }
		   else{
		   		$('.csv').hide();
		   }
   });
}

$( document ).ready(function() {
  $("#convert_to_arff" ).submit(function( event ) {
  		event.preventDefault();
 		convert_to_arff();
  });
  $("#drop_null_columns" ).submit(function( event ) {
      event.preventDefault();
      drop_null_columns();
  });
  $("#transform_date" ).submit(function( event ) {
      event.preventDefault();
      var dateformat_from=$("#dateformat_from").val();
      var dateformat_to=$("#dateformat_to").val();
      run_date_transform(dateformat_from,dateformat_to);
  });
  $("#translate_labels" ).submit(function( event ) {
      event.preventDefault();
      var labels_txt=$("#translate_labels_text").val();
      translate_labels(labels_txt);
  });
  $("#transform_columns" ).submit(function( event ) {
      event.preventDefault();
      var columns=$("#selected_columns").val();
      var transforms=$("#selected_transforms").val();
      transform_columns(columns, transforms);
  });
   $("#replace_outlier" ).submit(function( event ) {
      event.preventDefault();
      var column=$("#selected_columns2").val().join();
      var smaller = $("#smaller").val();
      var smaller_replace = $("#smaller_replace").val();
      var larger = $("#larger").val();
      var larger_replace = $("#larger_replace").val();
      replace_outlier(column,smaller,smaller_replace,larger,larger_replace);
  });
  $("#transform_paa" ).submit(function( event ) {
      event.preventDefault();
      var columns=$("#selected_columns4").val().join();
      var window = $("#window").val();
      transform_paa(columns, window);
  });
  $("#transform_discretize" ).submit(function( event ) {
      event.preventDefault();
      var columns=$("#selected_columns3").val().join();
      var density = $("#density").prop('checked');
      var width = $("#width").prop('checked');
      if(!width){
      	density=true;
      }
      var bins = $("#no_bins").val();
      transform_discretize(columns, density, bins);
  });
  //make window
  $("#transform_make_windows" ).submit(function( event ) {
      event.preventDefault();
      var window=$("#mwindow").val();
      var increment = $("#mincrement").val();
      transform_make_windows(window, increment);
  });
  $("#filter_windows").submit(function(){
    event.preventDefault();
  	var parameters = [];
  	$('.window_filter').each(function(index){
  		var col = $(this).find("select[name='columns_filter']").val();
  		var predicate = $(this).find("select[name='predicate_filter']").val(); //e.g. equals
  		var val = $(this).find("input[name='value_filter']").val(); //e.g. "203"
  		var combination = $(this).find("select[name='combi_filter']").val(); //e.g. "and" or "or"
  		parameters.push({col:col, predicate:predicate, val:val, combination:combination});
  	});
  	console.log(parameters);
  	transform_filter_windows(parameters);
  });
  $("#runWindowGroupBy").click(function(){
  	var parameters = [];
  	$('.window_group').each(function(index){
  		var col = $(this).find("select[name='column_group']").val();
  		var transform = $(this).find("select[name='transform_group']").val();
  		var parameter = $(this).find("input[name='parameter_group_by']").val();
  		parameters.push({col:col, transform: transform, parameter:parameter});
  	});
  	console.log(parameters);
  	transform_group_by_window(parameters);
  });




  //join stuff
  $("#transform_split" ).submit(function( event ) {
      event.preventDefault();
      var nullpatterns = [];
      $('#null_patterns :checked').each(function() {
       		nullpatterns.push($(this).val());
    	  });
      transform_split(nullpatterns);
  });
   $("#transform_join" ).submit(function( event ) {
      event.preventDefault();
      var files=$("#selected_files").val().join();
      transform_join(files);
  });

  $("#run_corr" ).submit(function( event ) {
      event.preventDefault();
      var remove = $('#remove').prop('checked');
      run_corr(remove);
  });
  load_schema();
  load_null_patterns();
  show_column_selections("#selected_columns","chosen");
  show_column_selections("#selected_columns2","chosen2");
  show_column_selections("#selected_columns3","chosen3");
  show_column_selections("#selected_columns4","chosen4");
  show_column_selections("#columns_filter","columns_filter");
  show_column_selections("#column_group","column_group");
  show_file_selections();
  //interaction: set default for capping
  $(".chosen2").change(function() {
    var attribute = $(this).val();
    var metadata = get_metadata_attribute(attribute);
    $("#smaller").val(round(metadata["IQ_01"]));
    $("#smaller_replace").val(round(metadata["IQ_01"]));
    $("#smaller").prop('title','quantiles IQ000-IQ010:' + metadata["quantiles-IQ000-IQ010"]);
    $("#larger").val(round(metadata["IQ_99"]));
    $("#larger_replace").val(round(metadata["IQ_99"]));
    $("#larger").prop('title', 'quantiles IQ090-IQ100: :' + metadata["quantiles-IQ099-IQ100"]);
  });
  check_and_show_file_type();
  //for window filters
  $('#addWindowFilter').click(function() {
   		var clone = $('#windows_filter1').clone();
    		 $("#window_filter").append(clone);
    });
  $('#addWindowGroupBy').click(function() {
   		var clone = $('#window_group1').clone();
    		$("#window_group").append(clone);
    });
});


</script>
   <div id="content">
   <div id="container"
  		<!-- table for layout -->
   		<table>
   			<tr>
   			<!-- left: histograms -->
   			<td valign="top">

   			<table class="table arff" id="schema" style="padding: 5px; width: 100%">
               <thead>
                  <tr><th>Column</th><th>Type</th><th>Properties</th><th>Histogram</th><!-- <th>Histogram - Equal Density</th> --></tr>
               </thead>
               <tbody>
               </tbody>
            	</table>

            <span id="schema_status" style="margin-top:15px; display:inline-block;"/></td>
            </td>

            <!-- right: options -->
            <td valign="top">


      <%-- Advanced --%>
      <table class="arff actions" style="width:510px; table-layout: fixed">
      <tr><td valign="top" style="width:125px;">PAA</td>
            <td valign="top" style="width:375px;">
               <form id="transform_paa" enctype="multipart/form-data" method="POST" action="/" >
            	   <select name="selected_columns4" id="selected_columns4" multiple="false" class="chosen4"
            	           style="width: 150px; padding: 5px"
            	   		   data-placeholder="Select one or more columns">
               </select>
               <input class="field" type="text" id="window" name="window" placeholder="Window" style="width:100px"/>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="transform_paa_status"/></td>
      </tr>
      <tr><td valign="top">Outliers</td>
            <td valign="top">
               <form id="replace_outlier" enctype="multipart/form-data" method="POST" action="/" >
            	   <select name="selected_columns2" id="selected_columns2" multiple="false" class="chosen2"
            	   		   style="width: 150px; padding: 3px"
            	   		   data-placeholder="Select one or more columns">
               </select>
               <br/>
               If value &lt; <input class="field"  type="text" id="smaller" name="smaller" placeholder="min" style="width:60px"/>
               then <input class="field"  type="text" id="smaller_replace" name="smaller_replace" placeholder="value" style="width:60px"/>
               <br/>
               If value &gt; <input class="field"  type="text" id="larger" name="larger" placeholder="max" style="width:60px"/>
               then <input class="field"  type="text" id="larger_replace" name="larger_replace" placeholder="value" style="width:60px"/>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="replace_outlier_status"/></td>
      <tr><td valign="top">Discretise</td>
            <td valign="top"><form id="transform_discretize" enctype="multipart/form-data" method="POST" action="/" >
            	   <select name="selected_columns3" id="selected_columns3" multiple="true" class="chosen3"
            	           style="width: 150px; padding: 5px"
            	           data-placeholder="Select one or more columns">
               </select>
               <br/>
               <input style="position:relative; top:5px" class="field" type="checkbox" id="width" name="width" value="width" checked/>Equal Width
               <input style="position:relative; top:5px" class="field" type="checkbox" id="density" name="density" value="density"/> Equal Density
               <br/>
               #bins: <input class="field" type="text" id="no_bins" name="no_bins" value="10" style="width:50px"></input>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="transform_discretize_status"/></td>
      </tr>
      <tr> <td valign="top">More transforms</td>
           <td valign="top"><form id="transform_columns" enctype="multipart/form-data" method="POST" action="/" >
               <select name="selected_columns" id="selected_columns" multiple="true" class="chosen"
                       style="width: 150px; margin: 5px;"
               		   data-placeholder="Select one or more columns">
               </select>
               <select name="selected_transforms" id="selected_transforms" multiple="true" class="chosen"
                       style="width: 100px; margin: 5px;"
               		   data-placeholder="Transform">
                          <option value="normalize">normalize</option>
                          <option value="remove_outliers">remove outliers (>5 sigma)</option>
                          <option value="remove">drop column</option>
                          <!-- <option value="sec2datetime">floating-point seconds to datetime</option> -->
                          <option value="datetime2sec">change from datetime to millis</option>
                          <option value="2categorical">change type to categorical</option>
               </select>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="transform_columns_status"/>
           </td>
      </tr>
      </table>

     <%-- Make windows --%>
     <table class="arff actions" style="width:500px; table-layout:fixed;">
          <tr><td valign="top" style="width:125px">Make segments</td>
              <td valign="top" style="width:375px">
            		<form id="transform_make_windows" enctype="multipart/form-data" method="POST" action="/" >
			    <input class="field" id="mwindow" type="text" name="window" style="width:100px" placeholder="Window"></input>
            	    <input class="field" id="mincrement" type="text" name="increment" style="width:100px" placeholder="Increment"></input>
                <input class="button" type="submit" value="Run"/>
                </form>
            </td>
          </tr>
          <tr>
          	<td valign="top">Filter
          	</td>
          	<td>
          		<form id="filter_windows" enctype="multipart/form-data" method="POST" action="/" >
          		<div id="window_filter">
	          		<!-- one -->
	          		<div id="windows_filter1" class="window_filter">
		          		<select class="combo" name="columns_filter" id="columns_filter"
		            	           style="width: 100px; padding: 5px;"
		            	           data-placeholder="Select column">
		            	           	  <option value=""></option>
		            	    </select>
		            	    <select class="combo" name="predicate_filter" id="predicate_filter"
		                       style="width: 85px; padding: 5px;" data-placeholder="Select option">
		                          <option value="equals">Equals</option>
		                          <option value="notEquals">Not equals</option>
		                          <option value="smaller">Smaller</option>
		                          <option value="larger">Larger</option>
		                          <!--
		                          <option value="contains">Contains</option>
		                          <option value="notContains">Not contains</option>
		                          -->
		              	</select>
		              	<input id="value_filter" class="field" type="text" name="value_filter" style="width:50px" placeholder=""></input>
		              	<select class="combo" name="combi_filter" id="combi_filter" class="combi_filter"
		                       style="width: 60px; padding: 5px;" data-placeholder="Select option">
		                       	  <option value=""></option>
		                          <option value="and">And</option>
		                          <option value="or">Or</option>
		              	</select>
	              	</div>
              	</div>
              	<input id="addWindowFilter" class="button" name="add" type="button" value="+" style="margin: 5px; height:24px"/>
              	<input id="runWindowsFilter" class="button" name="run" type="submit" value="Run" style="margin:10px; float: right; margin-right:40px"/>
              	<span id="filter_windows_status"/>
              	</form>
          	</td>
          </tr>
          <tr>
          	<td valign="top">Aggregate
          	</td>
          	<td>
          		<div id="window_group">
	          		<!-- one -->
	          		<div id="window_group1" class="window_group">
		          		<select class="combo" name="column_group" id="column_group"
		            	           style="width: 100px; padding: 5px;"
		            	           data-placeholder="Select column">
		            	           	  <option value=""></option>
		            	    </select>
		            	    <select class="combo" name="transform_group" id="transform_group"
		                       style="width: 85px; padding: 5px;" data-placeholder="Select transform">
		                          <option value="min">Min</option>
		                          <option value="max">Max</option>
		                          <option value="mean">Mean</option>
		                          <option value="count">Count</option>
		                          <option value="contains">Contains (parameter: x). If found 1, else 0.</option>
		                          <option value="unique">Unique (output is set of values per window)</option>
		                          <option value="paa">Piecewise Aggregate (parameter: window) (output is set of values per window)</option>

		              	</select>
		              	<input id="parameter_group_by" class="field" type="text" name="parameter_group_by" style="width:75px" placeholder="param"></input>
	              	</div>
              	</div>
              	<input id="addWindowGroupBy" class="button" name="add" type="submit" value="+" style="margin: 5px; height:24px"/>
              	<input id="runWindowGroupBy" class="button" name="run" type="submit" value="Run" style="margin:10px; float: right; margin-right:40px"/>
              	<span id="groupby_windows_status"/>
          	</td>
          </tr>
     </table>



	  <%-- Joins --%>
	  <table class="arff actions" style="width:500px; table-layout:fixed;">
	   <tr><td valign="top" style="width:125px">Partition dataset</td>
           <td valign="top" style="width:375px">
               <form id="transform_split" enctype="multipart/form-data" method="POST" action="/" >
               <div id="null_patterns" style="font-size:10px">
               </div>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="transform_split_status"/>
            </td>
      </tr>
       <tr><td valign="top">Merge dataset</td>
            <td valign="top"><form id="transform_join" enctype="multipart/form-data" method="POST" action="/" >
               <select name="selected_files" id="selected_files" multiple="false" class="chosenfs"
               		   style="width: 200px; padding: 5px"
               		   data-placeholder="Select file">
                        </select>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="transform_join_status"/>
              </td>
      </tr>
      </table>

        <%-- Queries --%>
	<table class="arff actions" style="width:500px; table-layout:fixed;">
     <tr>
     	<td valign="top" style="width:125px">SQL</td>
        <td valign="top" style="width:375px">
        		<form id="run_sql_query" enctype="multipart/form-data" method="POST" action="/" >
               <textarea id="sql" name="sql" cols="35" rows="3" style="font-family: Consolas, monaco, monospace; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px;">
SELECT *
FROM file
               </textarea>
               <input class="button" type="submit" value="Run" style="margin-left:7px"/> <span id="run_sql_query_status"/>
             </form>
        </td>
       </tr>
    	</table>

          	<!-- basic options -->
	   <table class="csv actions" style="width:500px; table-layout:fixed;">
	   <%-- Basic --%>
	   <tr valign="top"><td>Convert to arff</td>
				    <td valign="top">
				    		<form id="convert_to_arff" enctype="multipart/form-data" method="POST" action="/" >
				        	   <input type="submit" value="Convert"/>
				       	</form>
				    <span id="convert_to_arff_status"/>
				    </td>
			</tr>
	  </table>

	  <table class="arff actions" style="width:500px; table-layout:fixed;">
	        <tr><td valign="top"  style="width:150px">

	        <span title="Computes correlation coefficient between all normalized time series. If two time series have a correlation coefficient that is higher than 0.99, one of those series is removed.">Run analysis</span></td>
            <td valign="top" ><form id="run_corr" enctype="multipart/form-data" method="POST" action="/" >
               <input style="position:relative; top:5px" type="checkbox" name="remove" id="remove" value="True">
               	<span style="position:relative; top:5px" title="if pearson coefficient &gt;0.99 or &lt;-0.99">Remove correlated series</span>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="run_corr_status"/>
            </td>
      </tr>

      <tr>
      	<!--
      <td valign="top" style="width:150px">Drop null columns</td>
          <td valign="top" style="width:350px">
          		<form id="drop_null_columns" enctype="multipart/form-data" method="POST" action="/" >
                <input type="submit" value="Convert"/>
                 </form>
                 <span id="drop_null_columns_status"/>
          </td>
      </tr>
      -->
      <tr><td valign="top">Rename columns</td>
           <td valign="top">
            <form id="translate_labels" enctype="multipart/form-data" method="POST" action="/" >
              <textarea class="field" id="translate_labels_text" name="translate_labels_text" cols="40" rows="1" style="font-size: 12px; heigh:30px" placeholder="Oldname Newname" style="height:40px; width:250px;"></textarea>
             <input class="button" type="submit" value="Run"/>
            </form>
            <span id="translate_labels_status"/></td>
            </td>
      </tr>
      <tr><td valign="top">Change date format</td>
            <td valign="top"><form id="transform_date" enctype="multipart/form-data" method="POST" action="/" >
               <span style="margin-left: 5px; width:40px; display: inline-block;">From:</span>
               <input type="text" class="field" id="dateformat_from" name="dateformat_from" placeholder="dd-MMM-yyyy HH:mm:ss" style="width: 175px"/>
               <br/>
               <span style="margin-left: 5px; width:40px; display: inline-block;">To:</span>
               <input class="field" type="text" id="dateformat_to" name="dateformat_to" value="yyyy-MM-dd HH:mm:ss" style="width: 175px"/>
               <input class="button" type="submit" value="Run"/>
              </form>
              <span id="transform_date_status"/></td>
      </tr>
      </table>
                  </td>
          	</tr>
		</table>

   </div>
   </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>