<script src="/static/jquery-1.10.2.js"></script>
<script src="/static/jquery-ui.min.js"></script>
<script src="/static/chosen.jquery.js"></script>
<script src="/static/jquery.dataTables.min.js"></script>
<script src="/static/Chart.js"></script>
<script src="/static/webmime.js"></script>
<link rel="stylesheet" href="/static/chosen.css" type="text/css">
<link rel="stylesheet" href="/static/jquery-ui.min.css" type="text/css">
<link rel="stylesheet" href="/static/jquery.dataTables.css" type="text/css">
<link rel="stylesheet" href="/static/webmime.css" type="text/css">
<style>
.mlink {
	font-size: 14px; 
	position: relative; 
	top:-10px;
	padding: 11px; 
	text-decoration: none; 
	border: 1px solid #fefefe; 
	margin: 0px;
}

.active{
	background-color: #eeefef;
}
</style>
<div id="header">
	<div style="display: inline-block; padding-right: 100px; padding-left: 5px">
		<div style="font-size:15px; float: top; text-align: left">TiPM</div>
		<div style="font-size:10px; float: bottom">Time series Pattern mining &amp; <br/> Anomaly detection</div>
	</div>
	<a href="list" id="m_list" class="mlink">Dataset</a><a href="data" id='m_data' class="mlink">Table View</a><a href="transform" id='m_transform' class="mlink">Transform</a><a href="mine" id='m_mine' class="mlink">Data Mining</a><a href="plot" id='m_plot' class="mlink">Plot</a>
</div>
<script>
function highlight_menu(){
	var path = window.location.pathname;
	var page = path.split("/").pop();
	console.log( page );
	$('#m_' + page).addClass('active');
}
highlight_menu();

</script>


<div id="session">
   	<ul class="breadcrumb">
        <li><a href="#" id="breadcrumbProject">${sessionScope.mySession.currentProject}</a><span class="divider">/</span></li>
        <li class="active">${sessionScope.mySession.currentItem}</li>
	</ul>
</div>

