
//Shows 'loading' icon + Finished/Error took xxxms, when done
var associativeArray = {};
function show_start_loading(status_element_id){
      var startTime = new Date();
      associativeArray[status_element_id] = startTime;
      $(status_element_id).empty().prepend('<img id="loading-image" src="images/ajax-loader.gif" alt="Loading..." />');
}

function show_stop_loading(status_element_id){
      var endTime = new Date();
      var startTime = associativeArray[status_element_id];
      var timeDiff = (endTime - startTime)/1000;
      $(status_element_id).empty().append('<span style="color: #CCCCCC;font-family: Consolas, monaco, monospace; font-size:0.9em; padding-left:5px">Finished. Took ' + timeDiff + 's<span>');
}

function show_error(status_element_id){
      var endTime = new Date();
      var startTime = associativeArray[status_element_id];
      var timeDiff = (endTime - startTime)/1000;
      $(status_element_id).empty().append('<span style="color: #C85A17;font-family: Consolas, monaco, monospace; font-size:0.9em; padding-left:5px;">Error. Took ' + timeDiff + 's<span>');
}

function show_error_with_message(status_element_id, message){
      var endTime = new Date();
      var startTime = associativeArray[status_element_id];
      var timeDiff = (endTime - startTime)/1000;
      $(status_element_id).empty().append('<span style="color: #C85A17;font-family: Consolas, monaco, monospace; font-size:0.9em; padding-left:5px;">Error: ' + message + ' Took ' + timeDiff + 's<span>');
}


//Do show loading icon stuff + rest post
function do_basic_post_with_reporting(put_url, status_element_id){
  do_basic_post_with_reporting_payload(put_url,{},status_element_id);
}

function do_basic_post_with_reporting_call(put_url, status_element_id, json_data, callback){
  show_start_loading(status_element_id);
  $.post(put_url, json_data).done(function(data ) {
       show_stop_loading(status_element_id);
       callback(data);
    }).fail(function(data){
    		if(data['responseJSON'] && data['responseJSON']['message']){
    			show_error_with_message(status_element_id, data['responseJSON']['message']);
    		}else{
    			show_error(status_element_id); 
    		}
    });
}



function do_basic_post_with_reporting_payload(put_url, json_data, status_element_id){
  show_start_loading(status_element_id);
  $.post(put_url, json_data).done(function(data ) {
       show_stop_loading(status_element_id);
    }).fail(function(data){
       show_error(status_element_id);  
    });
}

function round(x){
  return Math.round(x * 1000)/1000;
}

function isFloat(val) {
    var floatRegex = /^-?\d+(?:[.,]\d*?)?$/;
    if (!floatRegex.test(val))
        return false;

    val = parseFloat(val);
    if (isNaN(val))
        return false;
    return true;
}

function format(value){
	if(isFloat(value)){
		f = parseFloat(value)
		//value is integer, or decimal part is 0.00000
		if(f % 1 === 0){
			return f.toFixed(0)
		}
		else{
			return f.toFixed(3)
		}
	}
	return value
}

function pretty_print_pattern(pattern){
     var patternArr = pattern.split(' ') //for continous patterns, round, e.g. -0.3986845514419437 -0.6192965985168282
     for(var j=0;j<patternArr.length;j++){
    		if(isFloat(patternArr[j])){
    			patternArr[j] = round(patternArr[j]);
    		}
    		else if(patternArr[j].includes('=')){
    			patternArr[j] = patternArr[j].split('=')[0] + '=' + parseFloat(patternArr[j].split('=')[1]).toFixed(2);
    		}
     }
     pattern= patternArr.join(' ');
	 return pattern
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function mymax(values){
	var maxi = -1000000;
	for(var i = 0; i < values.length; i++){
         var val = values[i];
         if(typeof(val) === "number" && isNaN(val)){
         	
         }
         else{
         	maxi = Math.max(maxi,val);
         }
    }
    return maxi;
}

function mymin(values){
	var mini = 1000000;
	for(var i = 0; i < values.length; i++){
         var val = values[i];
         if(typeof(val) === "number" && isNaN(val)){
         	
         }
         else{
         	mini = Math.min(mini,val);
         }
    }
    return mini;
}

function set_current_project(project, filename){
  $.ajax({ url: "/rest/load-data?pagination=&project=" + project + "&name=" + filename}).done(function(data) {
    });
}