#!/bin/python3
"""
Compute evaluation scores in python

authors: Len Feremans
created: July 2019

Used for integration with TIPM: A tool for Interactive time series
pattern mining and anomaly detection. (https://bitbucket.org/len_feremans/tipm_pub).

Compute evaluation scores in Python, since Java libraries for computing those are harder to find/use?
"""
import sys
import os
import pandas as pd
import numpy as np
from sklearn.metrics import roc_auc_score, average_precision_score


def has_multiple_values(data_block) -> bool:
    prev_val = data_block[0]
    for val in data_block:
        if prev_val != val:
            return True
    return False


if __name__ == '__main__':
    # get file argument
    arguments = sys.argv
    print('Argument List:' + str(arguments))
    score_and_labels_file = arguments[1]
    fpof = arguments[2]
    # validate input
    if not os.path.exists(score_and_labels_file):
        print("Error: can not read file:{}".format(os.path.abspath(score_and_labels_file)))
        sys.exit(-1)
    df = pd.read_csv(score_and_labels_file)
    if list(df.columns.values) != ["Window", "Score", "Label"]:
        print("Error: expecting [Window,Score,Label] as columns")
        print(df.columns.values)
        sys.exit(-1)
    # compute evaluation score
    windows = df['Window'].values
    scores = df['Score'].values
    # for FPOF, score needs to be reversed, e.g. 0.0 is anomalous and 1.0 is normal, since we compute just the fraction of #patterns-matching-windows/#all-patterns
    if fpof == 'FPOF':
        scores = 1.0 - scores
    window_labels = df['Label'].values
    ixl = np.where(window_labels != 0)[0]

    # the roc score is a methode of evaluating reliability but it doesn't work if we didn't properly fill in the labels
    if has_multiple_values(window_labels):
        auc = roc_auc_score(y_true=window_labels[ixl], y_score=scores[ixl])
        ap = average_precision_score(y_true=window_labels[ixl], y_score=scores[ixl])
        print("AUC: {:.3f}".format(auc))
        print("AP: {:.3f}".format(ap))
    sys.stdout.flush()
