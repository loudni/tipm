package be.uantwerpen.exception;

public class SPMFException extends RuntimeException{

    public SPMFException(String string) {
        super(string);
    }

}
