package be.uantwerpen.datamining.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.IOUtils.Streamer2;
import be.uantwerpen.mime_webapp.model.FileItem;

public class Operations {

	public static void unionCSVFile(List<File> csvFiles, File output, String wordInHeader) throws IOException {
		if(output.exists()) {
			output.delete();
		}
		final boolean[] first = new boolean[] {true};
		for(File file: csvFiles) {
			IOUtils.stream2(file, output, new Streamer2() {
				//only copy header of first csv file
				@Override
				public String doLine(String line) {
					if(line.contains(wordInHeader)){
						if(first[0]) {
							first[0] = false;
							return line;
						}
						else return null;
					}
					else {
						return line;
					}
				}
			}, true);
		}
	}

	/*
	public static void unionCSVFileDifferentSchema(List<File> csvFiles, File output) throws IOException {
		Set<String> masterHeader = new LinkedHashSet<>();
		for(File file: csvFiles) {
			String[] header = IOUtils.readFirstLine(file).split(";");
			for(String head: header) {
				masterHeader.add(head);
			}
		}
		List<String> masterHeaderLst = new ArrayList<String>(masterHeader);

		BufferedWriter writer = new BufferedWriter(new FileWriter(output));
		writer.write(CollectionUtils.join(masterHeader, ";"));
		writer.write("\n");
		for(File file: csvFiles) {
			List<String> lines = IOUtils.readFile(file);
			String[] header = lines.get(0).split(";");
			List<String> data = new ArrayList<String>();
			for(int i=0; i < masterHeader.size(); i++) {
				data.add("?");
			}
			for(int i=0; i<header.length; i++) {
				int idx = masterHeaderLst.indexOf(header[i]);
				data.set(idx,"" + i);
			}
			for(int i=1; i<lines.size(); i++) {
				String[] row =  lines.get(i).split(";");
				List<String> dataCurrent = new ArrayList<String>(data);
				for(int c=0; c < dataCurrent.size(); c++) {
					if(!dataCurrent.get(c).equals("?")) {
						dataCurrent.set(c, row[Integer.valueOf(dataCurrent.get(c))]);
					}
				}
				writer.write(CollectionUtils.join(dataCurrent, ";"));
				writer.write("\n");
			}
		}
		writer.close();
		File sorted = Utils.sortCSVFile(output, 0);
		output.delete();
		sorted.renameTo(output);
	}
	*/

	public static void timeJoin(FileItem currentInputArff, FileItem otherItemArff, File outputArff) throws IOException {
		List<String> names1 = ArffUtils.getAttributeNames(currentInputArff.getFile());
		names1 = names1.stream().map(String::toUpperCase).collect(Collectors.toList());
		List<String> names2 = ArffUtils.getAttributeNames(otherItemArff.getFile());
		names2 = names2.stream().map(String::toUpperCase).collect(Collectors.toList());
		List<String> types1 = ArffUtils.getAttributeValues(currentInputArff.getFile());
		List<String> types2 = ArffUtils.getAttributeValues(otherItemArff.getFile());
		List<List<String>> data1 = ArffUtils.loadDataMatrix(currentInputArff.getFile());
		List<List<String>> data2 = ArffUtils.loadDataMatrix(otherItemArff.getFile());
		//new schema:
		List<String> newNames= new ArrayList<String>(names1);
		List<String> newTypes= new ArrayList<String>(types1);
		for(String name: names2) {
			if(!names1.contains(name)) {
				newNames.add(name);
				newTypes.add(types2.get(names2.indexOf(name)));
			}
		}
		//modify data:
		List<List<String>> newData = new ArrayList<List<String>>();
		for(List<String> row: data1) {
			List<String> newRow = new ArrayList<String>(row);
			for(int i=0; i<newNames.size() - names1.size(); i++) {
				newRow.add("?");
			}
			newData.add(newRow);
		}
		for(List<String> row: data2) {
			List<String> newRow = new ArrayList<String>();
			for(String name: newNames) {
				if(names2.contains(name)) {
					newRow.add(row.get(names2.indexOf(name)));
				}
				else {
					newRow.add("?");
				}
			}
			newData.add(newRow);
		}
		Collections.sort(newData, new Comparator<List<String>>() {

			@Override
			public int compare(List<String> o1, List<String> o2) {
				try {
					Double d1 = Double.valueOf(o1.get(0));
					Double d2 = Double.valueOf(o2.get(0));
					return d1.compareTo(d2);
				}catch(Exception e) {
					return o1.get(0).compareTo(o2.get(0));
				}
			}

		});
		//save arff
		//save schema
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputArff));
		writer.write(String.format("@relation %s%n", currentInputArff.getLogicalName()));
		for(int idx=0; idx<newNames.size(); idx++){
			String name = newNames.get(idx);
			String value = newTypes.get(idx);
			writer.write(String.format("@attribute %s %s%n", name, value));
		}
		writer.write("\n@data\n");
		for(List<String> row: newData) {
			writer.write(CSVUtils.serializeLineArff(row));
			writer.write("\n");
		}
		writer.close();
	}
}
