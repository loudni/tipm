package be.uantwerpen.datamining.images;


import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import be.uantwerpen.ldataminining.utils.CollectionUtils;

import java.awt.image.BufferedImage;

import mil.nga.tiff.FieldTagType;
import mil.nga.tiff.FieldType;
import mil.nga.tiff.FileDirectory;
import mil.nga.tiff.Rasters;
import mil.nga.tiff.TIFFImage;

public class ImageUtils {
	private ImageUtils() {}

	public static int getImageDepth(TIFFImage input) {
		List<FileDirectory> directories = input.getFileDirectories();
		Rasters rasters = directories.get(0).readRasters();
		final FieldType[] types = rasters.getFieldTypes();
		int depth = Integer.MIN_VALUE;
		for(FieldType type : types) {
			if(depth == Integer.MIN_VALUE) depth = type.getBits();

			if(depth != type.getBits()) {
				throw new IllegalArgumentException("Tiff image has multiple depths prev:"+depth+" cur:"+type.getBits());
			}
		}
		return depth;
	}

	/**
	 * @param image
	 * //Canal order is RGB[unkown canals] if the sample count is >4
	 * @return [y][x][canal]
	 */
	public static RawImage convertImageToRaw(TIFFImage image) {
		if(image.getFileDirectories().size() != 1) {
			throw new IllegalArgumentException("Tiff image has multiple directories");
		}

		FileDirectory directory = image.getFileDirectories().get(0);
		Rasters rasters = directory.readRasters();

		System.out.println("image has " + rasters.getFieldTypes().length + " canals");
		System.out.println("image type is " + rasters.getFieldTypes()[0]);

		System.out.println(CollectionUtils.join(rasters.getFieldTypes(), ","));

		int photometricInterpretation = (int)directory.getFieldTagTypeMapping().get(FieldTagType.PhotometricInterpretation).getValues();
		System.out.println(PhotoMetric.photometricInterpretationToString(photometricInterpretation));

		Number[][][] rawImage = new Number[rasters.getHeight()][rasters.getWidth()][rasters.getFieldTypes().length];

		for(int y = 0; y < rasters.getHeight(); y++) {
			for(int x = 0; x < rasters.getWidth(); x++) {
				Number[] pixel = rasters.getPixel(x, y);
				for(int i = 0; i < pixel.length; i++) {
					//weird quirks with the tiff java library we get the pixels with the wrong type
					if(pixel[i].getClass().equals(Short.class) && rasters.getFieldTypes()[0].equals(FieldType.BYTE)) {
						rawImage[y][x][i] = Byte.valueOf(pixel[i].byteValue());
					} else {
						rawImage[y][x][i] = pixel[i];
					}
				}

			}
		}

		return new RawImage(rawImage, photometricInterpretation, getImageDepth(image));
	}

	/**
	 * @param rawImage [y][x][canal]
	 * @param output
	 * @throws IOException
	 */
	public static void convertRawImageToJpg(RawImage rawImage, File output) throws IOException {
		byte[] image = new byte[rawImage.getHeight() * rawImage.getWidth() * 3];

		double min = 0;
		double max = 0;

		//calculating min and max is only useful for floating point conversion
		if(rawImage.isFloating()) {
			System.out.println("image is float");
			for(int y = 0; y < rawImage.getHeight(); y++) {
				for(int x = 0; x < rawImage.getWidth(); x++) {
					for(int i = 0; i < rawImage.getCanalCount(); i++) {
						if(rawImage.imageData[y][x][i].doubleValue() < min) min = rawImage.imageData[y][x][i].doubleValue();
						if(rawImage.imageData[y][x][i].doubleValue() > max) max = rawImage.imageData[y][x][i].doubleValue();
					}
				}
			}
		}


		// the tif format qualify the alpha channel as an extra channel and all extra channels are after the main channels
		// the number of main channels are defined by the photometric interpretation
		for(int y = 0; y < rawImage.getHeight(); y++) {
			for(int x = 0; x < rawImage.getWidth(); x++) {
				byte grayscaleValue = downSampleToByte(rawImage.imageData[y][x][0], min, max);
				switch(rawImage.photoMetric) {
				case PhotoMetric.PHOTOMETRIC_MINISBLACK:
					image[(y * 3 * rawImage.getWidth()) + x * 3] = grayscaleValue;
					image[(y * 3 * rawImage.getWidth()) + x * 3 + 1] = grayscaleValue;
					image[(y * 3 * rawImage.getWidth()) + x * 3 + 2] = grayscaleValue;
					break;

				case PhotoMetric.PHOTOMETRIC_MINISWHITE:
					//need to flip byte, byte in java are from -128 to 127
					// unsiged conversion => 0 to 255 => removing 255 gives us the inverted grayscaleValue
					image[(y * 3 * rawImage.getWidth()) + x * 3] = (byte) (255 - Byte.toUnsignedInt(grayscaleValue));
					image[(y * 3 * rawImage.getWidth()) + x * 3 + 1] = (byte) (255 - Byte.toUnsignedInt(grayscaleValue));
					image[(y * 3 * rawImage.getWidth()) + x * 3 + 2] = (byte) (255 - Byte.toUnsignedInt(grayscaleValue));
					break;

				case PhotoMetric.PHOTOMETRIC_RGB:
					for(int i = 0; i < 3; i++) {
						image[(y * 3 * rawImage.getWidth()) + (x * 3) + i] = downSampleToByte(rawImage.imageData[y][x][i], min, max);
					}
					break;

				default:
					throw new IllegalArgumentException("Unknown or unsupported photometric interpretation");
				}

			}
		}

		int[] pixels = new int[image.length / 3];
		// merging the three channels into one int
		for(int i = 0; i < image.length; i += 3) {
			pixels[i / 3] = (image[i] << 16 & 0xFF0000) | (image[i + 1] << 8 & 0x00FF00) | (image[i + 2] & 0x0000FF);
		}

		//using rgb for all image types simplify the code but was *3 the amount of memory for grey scale images.
		BufferedImage imageBuffer = new BufferedImage(rawImage.getWidth(), rawImage.getHeight(), BufferedImage.TYPE_INT_RGB);
		imageBuffer.setRGB(0, 0, rawImage.getWidth(), rawImage.getHeight(), pixels, 0, rawImage.getWidth());

		System.out.println(output.getAbsolutePath());
		output.mkdirs();
		ImageIO.write(imageBuffer, "jpg", output);
	}

	private static byte downSampleToByte(Number number, double min, double max) {
		switch(number.getClass().getName()) {
			case "java.lang.Byte":
				return number.byteValue();
			case "java.lang.Short":
				return (byte)(number.longValue() / Short.MAX_VALUE * 255);
			case "java.lang.Integer":
				return (byte)(number.longValue() / Integer.MAX_VALUE * 255);
			case "java.lang.Long":
				return (byte) (number.longValue() / Long.MAX_VALUE * 255);

			//since floats have a value range of -10^37 to 10^37, we can't just scale them by their max value since most data will not be in that range.
			case "java.lang.Float":
			case "java.lang.Double":
				return (byte) ((number.doubleValue() - min) / (max - min) * 255);
			default:
				throw new IllegalArgumentException("Unknown number type: "+number.getClass().getName());
		}
	}
}
