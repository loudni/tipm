package be.uantwerpen.datamining.images;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageClustering {
	private ImageClustering() {}

	/**
	 * @param canal [pixel]
	 * @param k
	 * @return
	 */
	private static double[] randomCentroids(double[] canal, int k) {
		double[] centroids = new double[k];


		for(int i = 0; i < k; i++){
			centroids[i] = canal[(int)Math.round(canal.length * Math.random())];
		}

		return centroids;
	}

	private static double[] kppCentroids(double[] canal, int k) {
		if(k > 1) {
			double[] prevCentroids = kppCentroids(canal, k - 1);

			int minIndex = 0;
			double maxDist = 0;

			//initialize max dist
			for(double centroid: prevCentroids) {
				maxDist += (int)Math.abs(canal[0] - centroid);
			}

			//we want to maximize the distance between the centroid and the new point
			for(int i = 1; i < canal.length; i++) {
				double dist = Math.abs(canal[i] - prevCentroids[0]);
				for(double centroid: prevCentroids) {
					dist = Math.min(
						Math.abs(canal[i] - centroid),
						dist
					);
				}
				if(dist > maxDist) {
					maxDist = dist;
					minIndex = i;
				}
			}

			double[] centroids = new double[k];
			System.arraycopy(prevCentroids, 0, centroids, 0, k - 1);
			centroids[k - 1] = canal[minIndex];
			return centroids;
		} else {
			return randomCentroids(canal, 1);
		}
	}

	private static double euclideanDistance(double pointa, double pointb){
		return Math.sqrt(Math.pow(pointa - pointb, 2));
	}

	/**
	 * @param pixelsCluster [pixel][imageId]
	 * @param pixels
	 * @param k
	 * @return
	 */
	private static double[] recomputeCentroids(int[] pixelsCluster, double[] pixels , int k) {
		// Map Cluster => Pixel indicies
		Map<Integer, List<Integer>> centroidMap = new HashMap<>();
		for(int pixel = 0; pixel < pixels.length; pixel++) {
			centroidMap.putIfAbsent(pixelsCluster[pixel], new ArrayList<>());
			centroidMap.get(pixelsCluster[pixel]).add(pixel);
		}

		/** [centroidId][pixels] */
		double[] newCentroids = new double[k];
		for(int key : centroidMap.keySet()) {
			double centroid = Double.MIN_VALUE;

			for(int point : centroidMap.get(key)) {
				double pixelValue = pixels[point];

				if(centroid == Double.MIN_VALUE) {
					centroid = pixelValue;
				} else {
					centroid += pixelValue;
				}
			}

			final int ELEMENT_COUNT = centroidMap.get(key).size();
			centroid /= ELEMENT_COUNT;
			newCentroids[key] = centroid;
		}

		for(int i = 0; i < k; i++) {
			List<Integer> values = centroidMap.getOrDefault(i, new ArrayList<>());
			if(values.isEmpty()) {
				newCentroids[i] = findFurthestValues(pixels, newCentroids);
			}
		}


		return newCentroids;
	}

	private static double findFurthestValues(double[] pixels, double[] newCentroids) {
		double value = 0;
		double maxDist = 0;

		for(double pixel : pixels) {
			double dist = Double.MAX_VALUE;
			for(double centroid : newCentroids ) {
				dist = Math.min(dist, euclideanDistance(centroid, pixel));
			}

			if(dist > maxDist) {
				maxDist = dist;
				value = pixel;
			}
		}
		return value;
	}

	/**
	 * @param centroidCoordinates
	 * @param images [imageId][(x + y * width) * canals]
	 * @return all a table of all points by image
	 */
	public static PointBuff[] coordinateToPoints(Vector[] centroidCoordinates, PointBuff[] image) {
		/**
		 * @type { PointBuff[][] | undefined }
		 */

		PointBuff[] result = new PointBuff[centroidCoordinates.length];

		// for each coordinates
		for(int coordinateId = 0; coordinateId < centroidCoordinates.length; coordinateId++) {
			Vector coordinate = centroidCoordinates[coordinateId];
			PointBuff imagePoint = null;
			//we get the matching points
			for(PointBuff point : image) {
				if(point.x == coordinate.x && point.y == coordinate.y) {
					imagePoint = point;
					break;
				}
			}

			result[coordinateId] = imagePoint;
		}
		return result;
	}

	// standard normalisation method
	//https://en.wikipedia.org/wiki/Normalization_(image_processing)
	private static long normalizePixel(double pixel, double min, double max, double newmax, double newmin) {
		return (long) ((pixel - min) / ( (newmax - newmin) / (max - min) ) + newmin);
	}

	/**
	 * //format inherited from the dtw algorithm not changed to simplify switching between algorithms
	 * @param images [canal][pixels][imageIndex]
	 * @param k
	 * @param maxIterations
	 * @return [canal][clusters (interlaces merged images)]
	 */
	public static int[][] kmeans(double[][][] images, int k, int maxIterations, boolean normalize, boolean randomCentroids){

		if(normalize) {
			for(int imageId = 0; imageId < images[0][0].length; imageId++) {
				double canal0min = Double.MAX_VALUE;
				double canal0max = Double.MIN_VALUE;
				for(int pixel = 0; pixel < images[0].length; pixel++) {
					if(images[0][pixel][imageId] < canal0min) {
						canal0min = images[0][pixel][imageId];
					}
					if(images[0][pixel][imageId] > canal0max) {
						canal0max = images[0][pixel][imageId];
					}
				}
				for(int canal = 1; canal < images.length; canal++) {
					double min = Double.MAX_VALUE;
					double max = Double.MIN_VALUE;
					for(int pixel = 0; pixel < images[canal].length; pixel++) {
						if(images[canal][pixel][imageId] < min) {
							min = images[canal][pixel][imageId];
						}
						if(images[canal][pixel][imageId] > max) {
							max = images[canal][pixel][imageId];
						}
					}
					for(int pixel = 0; pixel < images[canal].length; pixel++) {
						images[canal][pixel][imageId] = normalizePixel(images[canal][pixel][imageId], min, max, canal0max, canal0min);
					}
				}
			}
		}

		//merging canals
		// [canal][pixels]
		double[][] pixels = new double[images.length][images[0].length * images[0][0].length];

		for(int imageId = 0; imageId < images[0][0].length; imageId++) {
			for(int pixelId = 0; pixelId < images[0].length; pixelId++) {
				for(int canalId = 0; canalId < images.length; canalId++) {
					//interlaced placement
					// example with a 2 image dataset: [pixel1 of image1][pixel1 of image2][pixel2 of image1]...
					int index = pixelId * images[0][0].length + imageId;
					pixels[canalId][index] = images[canalId][pixelId][imageId];
				}
			}
		}

		int [][] discretizedPixels = kmeans(pixels, k, maxIterations, randomCentroids);
		return discretizedPixels;
	}

	/**
	 * @param images [canal][pixels]
	 * @param k
	 * @param maxIterations
	 * @param randomCentroids if false will use K-means++ algorithm to generate random centroids
	 * @return [canal][pixel]
	 */
	public static int[][] kmeans(double[][] images, int k, int maxIterations, boolean randomCentroids){
		System.out.println("K-means  " + images.length + " canals");
		// Select K initial centroids

		final int[][] clusterizedPixels = new int[images.length][images[0].length];

		//thread pool
		// threads are an effective way to divide by 4 the time required to compute the k-means algorithm.
		// but also costs memory.
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		for(int canal=0; canal < images.length; canal++) {
			//need to be constant so the thread can access it's canal id
			final int canalId = canal;
			executor.submit(() -> {
				double[] centroids;

				if(randomCentroids) {
					centroids = randomCentroids(images[0], k);
				} else {
					centroids = kppCentroids(images[0], k);
				}

				int previousHashcode = 0;
				for(int i = 0; i < maxIterations; i++) {
					System.out.println("Kmeans Iteration: " + i + "/" + maxIterations);
					for(int pixelId = 0; pixelId < images[canalId].length; pixelId++) {
						double pixel = images[canalId][pixelId];
						double minDist = Double.MAX_VALUE;
						// Find the centroid at a minimum distance from it and add the record to its cluster
						for(int id = 0; id < centroids.length; id++ ){
							double centroid = centroids[id];

							//if the centroid is the min value it means that it didn't get any pixels
							if(centroid != Double.MIN_VALUE) {
								double dist = euclideanDistance(centroid, pixel);
								if(dist < minDist){
									minDist = dist;
									clusterizedPixels[canalId][pixelId] = id;
								}
							}
						}
					}

					int hashcode = Arrays.hashCode(clusterizedPixels[canalId]);
					//no changes in the clusters so stop the algorithm
					if(previousHashcode == hashcode) {
						break;
					}
					previousHashcode = hashcode;

					// Recompute centroids according to new cluster assignments
					centroids = recomputeCentroids(clusterizedPixels[canalId], images[canalId], k);
				}
			});
		}

		executor.shutdown();
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}

		return clusterizedPixels;
	}
}
