package be.uantwerpen.datamining.images;

import java.io.Serializable;

public class RawImage implements Serializable{
	public final Number[][][] imageData;
	public final int photoMetric;
	public final int depth;

	public RawImage(Number[][][] imageData, int photoMetric, int depth) {
		this.imageData = imageData;
		this.photoMetric = photoMetric;
		this.depth = depth;
	}

	public int getWidth() {
		return imageData[0].length;
	}

	public int getHeight() {
		return imageData.length;
	}

	public boolean isFloating() {
		return imageData[0][0][0] instanceof Double ||
			imageData[0][0][0] instanceof Float;
	}

    public int getCanalCount() {
        return imageData[0][0].length;
    }
}
