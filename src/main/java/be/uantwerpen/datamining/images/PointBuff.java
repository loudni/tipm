package be.uantwerpen.datamining.images;

public class PointBuff extends Vector {
    public PointBuff(int x, int y, double[] values) {
        super(x, y);
        this.values = values;
    }

    public PointBuff(int x, int y, Number[] values) {
        super(x, y);
        this.values = new double[values.length];
        for(int i = 0; i < values.length; i++) {
            this.values[i] = values[i].doubleValue();
        }
    }
    public double[] values;
}
