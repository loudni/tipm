package be.uantwerpen.datamining.images;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//TODO: Add KmeanPP

public class ImageClusteringDTW {
	private ImageClusteringDTW() {}

	/**
	 * @param images [pixel][imageId]
	 * @param k
	 * @return
	 */
	private static double[][] randomCentroids(double[][] images, int k) {
		double[][] centroids = new double[k][images[0].length];


		for(int i = 0; i < k; i++){
			centroids[i] = images[(int)Math.round(images.length * Math.random())];
		}

		return centroids;
	}

	private static double euclideanDistance(double pointa, double pointb){
		return Math.sqrt(Math.pow(pointa - pointb, 2));
	}

	private static double dtwDistance(double[] seriesA, double[] seriesB){
		double[][] dtw = new double[seriesA.length][seriesB.length];

		for(int i = 0; i < seriesA.length; i++)
			for(int j = 0; j < seriesB.length; j++)
				dtw[i][j] = Double.MAX_VALUE;

		dtw[0][0] = 0;

		for(int i = 1; i < seriesA.length; i++){
			for(int j = 1; j < seriesB.length; j++){
				double cost = euclideanDistance(seriesA[i], seriesB[j]);
				dtw[i][j] = cost + Math.min(dtw[i-1][j], Math.min(dtw[i][j-1], dtw[i-1][j-1]));
			}
		}

		return dtw[seriesA.length - 1][seriesB.length - 1];
	}

	/**
	 * @param pixelsCluster [pixel][imageId]
	 * @param pixels
	 * @param k
	 * @return [centroidId][pixels]
	 */
	private static double[][] recomputeCentroids(int[] pixelsCluster, double[][] pixels , int k) {
		// Map Cluster => Pixel indicies
		Map<Integer, List<Integer>> centroidMap = new HashMap<>();
		for(int pixel = 0; pixel < pixels.length; pixel++) {
			centroidMap.putIfAbsent(pixelsCluster[pixel], new ArrayList<>());
			centroidMap.get(pixelsCluster[pixel]).add(pixel);
		}

		/** [centroidId][pixels] */
		double[][] newCentroids = new double[k][0];
		for(int key : centroidMap.keySet()) {
			double[] centroid = new double[0];

			for(int point : centroidMap.get(key)) {
				double[] pixelValue = pixels[point];

				if(centroid.length == 0) {
					centroid = pixelValue;
				} else {
					for(int pixel = 0; pixel < pixelValue.length; pixel++) {
						centroid[pixel] += pixelValue[pixel];
					}
				}
			}

			final int ELEMENT_COUNT = centroidMap.get(key).size();
			for(int pixel = 0; pixel < centroid.length; pixel++) {
				centroid[pixel] /= ELEMENT_COUNT;
			}
			newCentroids[key] = centroid;
		}

		for(int i = 0; i < k; i++) {
			List<Integer> values = centroidMap.getOrDefault(i, new ArrayList<>());
			if(values.isEmpty()) {
				newCentroids[i] = findFurthestValues(pixels, newCentroids);
			}
		}

		return newCentroids;
	}

	private static double[] findFurthestValues(double[][] pixels, double[][] newCentroids) {
		double[] value = {};
		double maxDist = 0;

		for(double pixel[] : pixels) {
			double dist = Double.MAX_VALUE;
			for(double[] centroid : newCentroids ) {
				dist = Math.min(dist, dtwDistance(centroid, pixel));
			}

			if(dist > maxDist) {
				maxDist = dist;
				value = pixel;
			}
		}
		return value;
	}

	/**
	 * @param centroidCoordinates
	 * @param images [imageId][(x + y * width) * canals]
	 * @return all a table of all points by image
	 */
	public static PointBuff[] coordinateToPoints(Vector[] centroidCoordinates, PointBuff[] image) {
		/**
		 * @type { PointBuff[][] | undefined }
		 */

		PointBuff[] result = new PointBuff[centroidCoordinates.length];

		// for each coordinates
		for(int coordinateId = 0; coordinateId < centroidCoordinates.length; coordinateId++) {
			Vector coordinate = centroidCoordinates[coordinateId];
			PointBuff imagePoint = null;
			//we get the matching points
			for(PointBuff point : image) {
				if(point.x == coordinate.x && point.y == coordinate.y) {
					imagePoint = point;
					break;
				}
			}

			result[coordinateId] = imagePoint;
		}
		return result;
	}

	/**
	 * @param images [canal][pixels][imageId]
	 * @param k
	 * @param maxIterations
	 * @return [canal][clusters]
	 */
	public static int[][] kmeans(double[][][] images, int k, int maxIterations){
		// Select K initial centroids

		int[][] clusterizedPoints = new int[images.length][images[0].length];

		//thread pool
		// threads are an effective way to divide by 4 the time required to compute the k-means algorithm.
		// but also costs memory.
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		for(int canal = 0; canal < images.length; canal++) {

			final int canalId = canal;
			executor.submit(() -> {
				double[][] centroids = randomCentroids(images[canalId], k);
				int previousHashcode = 0;

				for(int i = 0; i < maxIterations; i++) {
					System.out.println("Kmeans Iteration: " + i + "/" + maxIterations);
					// Find the centroid at a minimum distance from it and add the record to its cluster
					for(int pixel = 0; pixel < images[0].length; pixel++) {
						double minDist = Double.MAX_VALUE;
						for(int id = 0; id < centroids.length; id++ ){
							double[] centroid = centroids[id];

							if(centroid.length > 0) {
								double dist = dtwDistance(centroid, images[canalId][pixel]);
								if(dist < minDist){
									minDist = dist;
									clusterizedPoints[canalId][pixel] = id;
								}
							}
						}
					}

					int hashcode = Arrays.hashCode(clusterizedPoints[canalId]);
					//no changes in the clusters so stop the algorithm
					if(previousHashcode == hashcode) {
						break;
					}
					previousHashcode = hashcode;

					// Recompute centroids according to new cluster assignments
					centroids = recomputeCentroids(clusterizedPoints[canalId], images[canalId] ,k);
				}
			});
		}

		executor.shutdown();
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}

		return clusterizedPoints;
	}
}
