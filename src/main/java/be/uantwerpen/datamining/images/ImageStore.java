package be.uantwerpen.datamining.images;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.web.multipart.MultipartFile;

import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.mime_webapp.Settings;
import mil.nga.tiff.TIFFImage;
import mil.nga.tiff.TiffReader;

/**
 * @author Marc barbier
 *
 * this class store images in the tmp directory of the system.
 * since we don't care about keeping the files everything is stored there.
 */
public final class ImageStore {
	//thread safe Int
	private static AtomicInteger imageCount = null;
	//the file extension doesn't matter but i put .bin here and .dat in the images to avoid collisions.
	private static final File imageMap = new File(Settings.IMAGE_FOLDER + "imageMap.bin");
	private static HashMap<Integer, String> nameMap = null;

	private ImageStore() {}


	//made to be used with multiple threads
	public static int store(MultipartFile file) throws IOException {
		if(file.getOriginalFilename() == null) throw new InvalidParameterException();

		if(file.getOriginalFilename().endsWith(".tiff") ||
				file.getOriginalFilename().endsWith(".tif")) {

			final TIFFImage image = TiffReader.readTiff(file.getInputStream());
			return store(image, file.getOriginalFilename());
		}
		throw new InvalidParameterException("unrecognized format for" + file.getOriginalFilename());
	}


	/**
	 * @param image
	 * @return imageId
	 * made to be used with multiple threads
	 */
	public static int store(TIFFImage tiffImage, String label) throws IOException {
		loadDatabase();
		int imageId = imageCount.getAndIncrement();
		RawImage rawImage = ImageUtils.convertImageToRaw(tiffImage);
		File rawOutput = new File(Settings.IMAGE_FOLDER + imageId + ".dat");
		File jpgOutput = new File(Settings.IMAGE_FOLDER + imageId + ".jpg");

		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(rawOutput));
		oos.writeObject(rawImage);
		oos.close();

		ImageUtils.convertRawImageToJpg(rawImage, jpgOutput);
		nameMap.put(imageId, label);

		System.out.println("stored image " +  jpgOutput.getAbsolutePath());

		saveDatabase();
		return imageId;
	}

	public static Pair<File, String> retrieveOverview(int imageId) throws IOException {
		return new Pair<>(new File(Settings.IMAGE_FOLDER + imageId + ".jpg"), nameMap.get(imageId));
	}

	/**
	 * @param imageId
	 * @return Pair<[y][x][canal], name>
	 * @throws IOException
	 */
	public static Pair<RawImage, String> retrieveRaw(int imageId) throws IOException {
		loadDatabase();
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(Settings.IMAGE_FOLDER + imageId + ".dat")));
		try {
			return new Pair<>((RawImage)ois.readObject(), nameMap.get(imageId));
		} catch(ClassNotFoundException e) {
			//will never happen
			throw new IOException(e);
		} finally {
			ois.close();
		}
	}

	public static void erase(int imageId) throws IOException {
		loadDatabase();

		new File(Settings.IMAGE_FOLDER + imageId).delete();
		if(imageId == imageCount.get() - 1) imageCount.decrementAndGet();

		saveDatabase();
	}

	/**
	 * Clears the image database and remove all images from the filesystem.
	 * @throws IOException
	 */
	public static void clear() throws IOException {
		loadDatabase();
		for(int i = 0; i < imageCount.get(); i++) {
			new File(Settings.IMAGE_FOLDER + i + ".dat").delete();
			new File(Settings.IMAGE_FOLDER + i + ".jpg").delete();
		}
		imageCount.set(0);
		nameMap.clear();
		saveDatabase();
	}

	private static void loadDatabase() throws IOException {
		if(imageCount != null)return;
		try {
			ObjectInputStream oid = new ObjectInputStream(new FileInputStream(imageMap));
			imageCount = new AtomicInteger((int) oid.readObject());
			nameMap = (HashMap<Integer, String>) oid.readObject();
			oid.close();
		} catch(FileNotFoundException | ClassNotFoundException e) {
			imageCount = new AtomicInteger(0);
			nameMap = new HashMap<>();
			saveDatabase();
		}
	}


	private static void saveDatabase() throws IOException {
		new File(Settings.IMAGE_FOLDER).mkdirs();
		ObjectOutputStream oid = new ObjectOutputStream(new FileOutputStream(imageMap));
		oid.writeObject(imageCount.get());
		oid.writeObject(nameMap);
		oid.close();
	}
}
