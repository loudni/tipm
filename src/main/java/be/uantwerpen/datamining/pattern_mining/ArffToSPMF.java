package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.ldataminining.model.CountMap;
import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.NaturalOrderComparator;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Triple;
public class ArffToSPMF {
	private ArffToSPMF(){}

	public static Map<String,Integer> oneHotEncodingItems(List<String> names, List<List<String>> rows, List<String> columnsFiltered, File dictOutput) throws IOException {
		CountMap<String> items = new CountMap<>();
		for(List<String> row: rows) {
			for(String name: columnsFiltered) {
				int colIdx = names.indexOf(name);
				if(CSVUtils.isEmptyValueInCSV(row.get(colIdx))) { //if empty skip
					continue;
				}
				items.add(String.format("%s=%s", names.get(colIdx), row.get(colIdx)));
			}
		}
		System.out.format("Found %d items: %s%n", items.getMap().size(), items.getKeysSortedDescending());
		List<String> itemsSorted = new ArrayList<>(items.getMap().keySet());
		Collections.sort(itemsSorted);
		IOUtils.saveFile(itemsSorted,dictOutput);
		Map<String,Integer> itemsSortedMap = new HashMap<>();
		for(int i=0; i< itemsSorted.size(); i++) {
			itemsSortedMap.put(itemsSorted.get(i),i+1); //Items are 1-indexed!
		}
		return itemsSortedMap;
	}

	//Note: //copy-pasted from TableLoadController.groupByWindow
	public static Pair<TreeSet<Integer>, ListMap<Integer,List<String>>> groupByWindow(List<String> names, List<List<String>> rows) {
		int windowIndex = names.indexOf("Window");
		if(windowIndex == -1)
			throw new RuntimeException("Window not found!");
		TreeSet<Integer> windowIdsSorted = new TreeSet<>();
		for(List<String> row: rows) {
			String windowStr = row.get(windowIndex);
			String[] windowValues = windowStr.split(";"); //in case of overlapping windows, can be multiple, e.g. "0;1"
			for(String windowValue: windowValues) {
				windowIdsSorted.add(Integer.valueOf(windowValue));
			}
		}
		ListMap<Integer,List<String>> groupByWindow = new ListMap<>();
		for(List<String> row: rows) {
			String[] overlappingWindows = row.get(windowIndex).split(";");
			for(String windowStr: overlappingWindows) {
				Integer window = Integer.valueOf(windowStr);
				groupByWindow.put(window,row);
			}
		}
		return new Pair<>(windowIdsSorted, groupByWindow);
	}

	/**
	 *
	 * @param arff
	 * @param columnsFiltered
	 * @return Triple<DictFile, TransactionFile, dict>
	 * @throws IOException
	 */
	static Triple<File,File, Map<String,Integer>> arffToSPMF(boolean isSequence, File arff, List<String> columnsFiltered) throws IOException {
		List<String> names = ArffUtils.getAttributeNames(arff);
		List<List<String>> rows = ArffUtils.loadDataMatrix(arff);

		//1. make item representation
		File dict = new File("./temp/", IOUtils.getFileWithDifferentExtension(IOUtils.getFileWithDifferentSuffix(arff, "-dict"),"txt").getName());
		// generate dictionary by counting
		Map<String,Integer> itemsSortedMap = oneHotEncodingItems(names,rows,columnsFiltered,dict);
		//2. Group by window
		Pair<TreeSet<Integer>, ListMap<Integer,List<String>>> pair = groupByWindow(names, rows);
		TreeSet<Integer> windowIdsSorted = pair.getFirst();
		ListMap<Integer,List<String>> groupByWindow = pair.getSecond();
		//3. create table/grouped representation:
		System.out.println("Creating sequence database. Nr of windows: " + windowIdsSorted.size());
		List<String> transactions = new ArrayList<>();

		for(Integer window: windowIdsSorted) {
			List<List<String>> rowGroup = groupByWindow.get(window);
			List<Set<Integer>> transaction = new ArrayList<>(); //note: if multivariate/mixed-type timeseries: add items of different dimensions
			for(List<String> row: rowGroup) {
				Set<Integer> itemsetAtTimestampK = new TreeSet<>();
				for(String column: columnsFiltered) {
					int colIdx = names.indexOf(column);
					String value = row.get(colIdx);
					if(!CSVUtils.isEmptyValueInCSV(value)) {
						int itemId = itemsSortedMap.get(String.format("%s=%s", column, value));
						itemsetAtTimestampK.add(itemId);
					}
				}
				transaction.add(itemsetAtTimestampK);
			}

			if(isSequence) {
				// SPMF format:
				// 1 -1 1 2 3 -1 1 3 -1 4 -1 3 6 -1 -2
				// The first line represents a sequence where the itemset {1} is followed by the itemset {1, 2, 3},
				// followed by the itemset {1, 3}, followed by the itemset {4}, followed by the itemset {3, 6}
				StringBuilder sb = new StringBuilder();
				for(Set<Integer> set: transaction) {
					if(set.isEmpty())
						continue;
					sb.append(CollectionUtils.join(set, " "));
					sb.append(" -1 ");
				}
				sb.append("-2");
				transactions.add(sb.toString());
			} else {
				Set<Integer> transactionAsList = new TreeSet<>();
				for(Set<Integer> set: transaction) {
					transactionAsList.addAll(set);
				}
				transactions.add(CollectionUtils.join(transactionAsList, " "));
			}

		}
		File transactiondb = new File("./temp", IOUtils.getFileWithDifferentExtension(IOUtils.getFileWithDifferentSuffix(arff,
			isSequence ?
				"-sequencedb":
				"-transactions"
			),"txt").getName()
		);

		IOUtils.saveFile(transactions, transactiondb);

		return new Triple<>(dict, transactiondb, itemsSortedMap);
	}

	private static void saveOutputReadable(List<Triple<String, Integer, String[]>> readablePatterns, File outputCSVReadable, File occurrencesOutput) throws IOException {
		readablePatterns.sort(new PatternComparator());
		System.out.println("Found " + readablePatterns.size() + " patterns.");
		//save
		List<String> linesOutput = new ArrayList<>();
		List<String> occurrenceList = new ArrayList<>();
		linesOutput.add("pattern,support");
		occurrenceList.add("Window,PatternId");
		ListIterator<Triple<String, Integer, String[]>> it = readablePatterns.listIterator();
		while(it.hasNext()) {
			int id = it.nextIndex() + 1;
			Triple<String, Integer, String[]> pattern = it.next();

			linesOutput.add(String.format("%s,%s", pattern.getFirst(), pattern.getSecond()));

			//add all of the windows,id combinations to the occurances file
			Arrays.asList(pattern.getThird()).stream().map(window -> {
				return String.format("%s,%s", window, id);
			}).forEach(occurrenceList::add);
		}
		IOUtils.saveFile(linesOutput, outputCSVReadable);
		IOUtils.saveFile(occurrenceList, occurrencesOutput);
	}

	public static void saveOutputReadableSequential(File dictFile, File resultSPMF, File outputCSVReadable, File occurrencesOutput) throws IOException {
		// Sequencial pattern use this format:
		// 1 -1 1 2 3 -1 1 3 -1 4 -1 3 6 -1 #SUP: support #SID: 1 2 3 4 5 6 7 8 9
		// note that itemsets are separated by -1 and two values can happen at the same time ex: 1 3 -1 -2

		Map<String, String> itemToLabel = loadTranslation(dictFile);

		// pcX=Y support windows
		List<Triple<String, Integer, String[]>> readablePatterns = new ArrayList<>();
		List<String> lines = IOUtils.readFile(resultSPMF);

		for(String line: lines) {
			String[] sidSplit = line.split(" #SID: ");
			String[] supSplit = sidSplit[0].split("#SUP: ");
			String[] items = supSplit[0].split(" -1 ");
			int support = Integer.parseInt(supSplit[1]);

			StringBuilder sb = new StringBuilder();
			for(String item: items) {
				String[] values = item.split(" ");
				List<String> translatedItem = new ArrayList<>(values.length);
				for(String value : values) {
					translatedItem.add(itemToLabel.get(value));
				}
				//end of line
				sb.append(CollectionUtils.join(translatedItem, ";"));
				sb.append(" ");
			}
			readablePatterns.add(new Triple<>(sb.toString().trim(), support, sidSplit[1].split(" ")));
		}

		saveOutputReadable(readablePatterns, outputCSVReadable, occurrencesOutput);
	}

	public static void saveOutputReadableItemset(File dictFile, File outputSPMF, File outputCSVReadable, File occurrencesOutput)
			throws IOException {
		//For itemsets output like:
		//0 #SUP: 36
		//25 76 87 117 119  #SUP: 134

		Map<String, String> itemToLabel = loadTranslation(dictFile);
		// pcX=Y support windows
		List<Triple<String, Integer, String[]>> readablePatterns = new ArrayList<>();
		List<String> lines = IOUtils.readFile(outputSPMF);


		for(String line: lines) {
			String[] sidSplit = line.split(" #TID: ");
			String[] supSplit = sidSplit[0].split("#SUP: ");
			int support = Integer.parseInt(supSplit[1]);

			StringBuilder sb = new StringBuilder();
			String[] items = supSplit[0].split(" ");
			for(String item : items) {
				sb.append(itemToLabel.get(item));
				sb.append(" ");
			}

			readablePatterns.add(new Triple<>(sb.toString().trim(), support, sidSplit[1].split(" ")));
		}

		saveOutputReadable(readablePatterns, outputCSVReadable, occurrencesOutput);
	}

	public static class PatternComparator implements Comparator<Triple<String, Integer, String[]>>{

		NaturalOrderComparator comp = new NaturalOrderComparator();

		@Override
		public int compare(Triple<String, Integer, String[]> a, Triple<String, Integer, String[]> b) {
			if(b.getSecond().compareTo(a.getSecond()) == 0) { //if support is equal, compare on pattern itself
				return comp.compare(a.getFirst(), b.getFirst());
			}
			else {
				return b.getSecond().compareTo(a.getSecond());
			}
		}
	}

	private static Map<String, String> loadTranslation(File dictFile) throws IOException {
		List<String> labels = IOUtils.readFile(dictFile);
		Map<String,String> itemToLabel = new HashMap<>();
		for(int i=0; i< labels.size(); i++) {
			itemToLabel.put(String.valueOf(i + 1), labels.get(i));
		}
		return itemToLabel;
	}
}
