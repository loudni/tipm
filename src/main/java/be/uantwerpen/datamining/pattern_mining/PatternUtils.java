package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import be.uantwerpen.exception.UnknownPatternTypeException;
import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.PatternSet;
import be.uantwerpen.mime_webapp.model.Project;

public class PatternUtils {
	private PatternUtils(){}

	/**
	 * @param timeseriesTable
	 * @return Map of window to a Map of column to a values, id pair
	 */
	public static Map<Integer, Map<String, List<Pair<Float, Integer>>>>  groupByWindows(Table timeseriesTable) {
		Map<Integer, Map<String, List<Pair<Float, Integer>>>> windowGroups = new HashMap<>();
		List<List<String>> rows = timeseriesTable.getRows();
		List<String> columnNames = rows.remove(0);
		final int windowsColumnId = columnNames.indexOf("Window");

		ListIterator<List<String>> iterator = rows.listIterator();
		while(iterator.hasNext()) {
			int rowIndex = iterator.nextIndex();
			List<String> row = iterator.next();
			String[] windows = row.get(windowsColumnId).split(";");

			for(String window : windows) {
				windowGroups.putIfAbsent(Integer.valueOf(window), new HashMap<>());
			}

			ListIterator<String> it = row.listIterator();
			while(it.hasNext()) {
				int columnIndex = it.nextIndex();
				try{
					Float columnValue = Float.valueOf(it.next());
					String columnName = columnNames.get(columnIndex);
					for(String window : windows) {
						windowGroups.get(Integer.valueOf(window)).putIfAbsent(columnName, new ArrayList<>());
						windowGroups.get(Integer.valueOf(window)).get(columnName).add(new Pair<>(columnValue, rowIndex));
					}
				} catch(NumberFormatException e) {
					//non numeric columns are ignored
					continue;
				}
			}
		}

		return windowGroups;
	}

	public static ResponseEntity<String> filterPattern(
		FileItem currentItem,
		String patternFilename,
		IRowPredicte predicate
	) {
		final Optional<PatternSet> optPatternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(patternFilename)).findFirst();

		if(!optPatternSet.isPresent())
			return new ResponseEntity<>(
				"patternSet not found for: " + patternFilename, new HttpHeaders(), HttpStatus.NOT_FOUND);

		PatternSet patternSet = optPatternSet.get();

		if(patternSet.getFilenameOccurrences() == null || patternSet.getFilenameOccurrences().equals("")) {
			return new ResponseEntity<>(
				"patternSetOccurrences not found for: " + patternFilename, new HttpHeaders(), HttpStatus.NOT_FOUND);
		}

		try {
			File patternFile = new File(Settings.FILE_FOLDER + patternSet.getFilename());
			Table patternTable = CSVUtils.loadCSV(patternFile);

			// we list the patterns to remove and remove them
			List<List<String>> rows = patternTable.getRowsStartingFrom1();
			List<Integer> toRemove = new ArrayList<>();
			Iterator<List<String>> it = rows.iterator();
			//do not use list iterator since we use it.remove
			int index = 0;
			while(it.hasNext()) {
				List<String> row = it.next();

				if( !predicate.predicate(row, index) ) {
					toRemove.add(index);
					it.remove();
				}
				index++;
			}


			// new id map IT DOESN'T COVER ALL OF THE IDS ON UP TO THE POINT WHERE THE PATTERNS ARE REMOVED
			Map<Integer, Integer> newIdMap = new HashMap<>();
			Collections.sort(toRemove);
			int min = 1;
			int offset = 0;
			for(Integer idToRemove : toRemove) {
				if(idToRemove > min) {
					for(int i = min; i < idToRemove; i++) {
						if(!toRemove.contains(i)) {
							newIdMap.put(i, i + offset);
						}
						min = i + 1;
					}
				}
				offset--;
			}


			//we need to remove the occurences WHILE making sure the pattern id are right
			// if we remove pattern 1 all the following patterns will need to be decremented by 1
			File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.getFilenameOccurrences());
			Table patterOcc = CSVUtils.loadCSV(patternOccFile);


			//linked lists have way better removing performance than arrays
			final List<List<String>> occRows = new LinkedList<>(patterOcc.getRowsStartingFrom1());
			Iterator<List<String>> occurrencesIterator = occRows.iterator();
			while(occurrencesIterator.hasNext()) {
				List<String> row = occurrencesIterator.next();
				final int patternId = Integer.parseInt(row.get(1));

				if(toRemove.contains(patternId)) {
					occurrencesIterator.remove();
				} else {
					if(newIdMap.containsKey(patternId)) {
						row.set(1, newIdMap.get(patternId).toString());
					}
					else {
						row.set(1, Integer.toString(patternId + offset));
					}
				}
			}
			patterOcc.setRows(occRows);

			CSVUtils.saveTable(patternTable, patternFile);
			CSVUtils.saveTable(patterOcc, patternOccFile);
			storePatternSetOccurrences(currentItem, patternFilename, patternOccFile);
			savePatternsWithMetadata(patternFile, patternOccFile, currentItem.getFile());

			patternSet.setNoPatterns((long) (patternTable.getRows().size() -1));
			ProjectRepository.save();

		} catch(IOException io) {
			return new ResponseEntity<>(
				"Unreadable patterns", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (UnknownPatternTypeException e) {
			//we can dissmis this exception
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	public static void storePatternSetOccurrences(FileItem currentInput, String patternFilename, File occurrencesFile) throws UnknownPatternTypeException {
		Project project = ProjectRepository.findProjectByItem(currentInput);
		final FileItem newInput = ProjectRepository.getLatestItem(project, currentInput.getLogicalName());
		storePatternSetOccurrences(newInput, project, patternFilename, occurrencesFile);
	}


	public static void storePatternSetOccurrences(FileItem currentInput, Project project, String patternFilename, File occurrencesFile) throws UnknownPatternTypeException
	{
		//get pattern set
		PatternSet set = null;
		for(PatternSet patternSet: currentInput.getPatterns()) {
			if(patternSet.getFilename().equals(patternFilename)) {
				set = patternSet;
			}
		}
		if(set == null) {
			throw new RuntimeException("Pattern set with name " + patternFilename + " not found!");
		}

		set.setFilenameOccurrences(occurrencesFile.getName());
		ProjectRepository.saveProject(project);
	}

	public static void savePatternsWithMetadata(File patternsFile, File occurrencesFile, File itemFile) throws IOException {
		//TODO: use RDBMS or something like pandas for Java
		//e.g. PatternsFile:
		//pattern,support
		//pc1=1 pc1=2 pc1=4 pc2=1;pc3=4 pc3=5,56
		//pc1=2 pc1=3 pc2=0 pc2=1 pc3=4 pc3=5 pc3=6,56
		Table patterns = CSVUtils.loadCSV(patternsFile);
		List<List<String>> patternsList = patterns.getRowsStartingFrom1();
		//OccurrencesFile:
		//Window,PatternId (starting from 1)
		//1,978
		//2,20
		//2,94
		Table pattern2window  = CSVUtils.loadCSV(occurrencesFile);
		System.out.println(occurrencesFile.getAbsolutePath());
		ListMap<Integer,Integer> patternId2windowListMap = new ListMap<>();
		for(List<String> row: pattern2window.getRowsStartingFrom1()) {
			patternId2windowListMap.put(Integer.valueOf(row.get(1)) -1, Integer.valueOf(row.get(0))); //patternId (-1 to get zero-based idx)->window1,window2...
		}
		//DataFile:
		Map<Integer, Integer> window2LabelMap = getWindowLabels(itemFile);
		//output: patterns with confidence, e.g. computing support normal, conf (normal/support) en 1-conf
		patterns.getRows().get(0).addAll(Arrays.asList("conf","windows","anomalies"));

		ListIterator<List<String>> it = patternsList.listIterator();
		while(it.hasNext()) {
			int patternId = it.nextIndex();
			List<String> patternRow = it.next();

			String pattern = patternRow.get(0);
			int support = Integer.parseInt(patternRow.get(1));

			if(support == 0) {
				patternRow.clear();
				patternRow.addAll(Arrays.asList(pattern, "0", "1", "", ""));
			} else {
				List<Integer> windows = patternId2windowListMap.get(patternId);
				List<Integer> anomalies = new ArrayList<>();
				for(Integer window: windows) {
					int label = window2LabelMap.get(window);
					if(label == 1) {
						anomalies.add(window);
					}
				}
				//check
				if(windows.size() != support) {
					System.err.println("Warning pattern support != #occurrences " + pattern + " support:" + support + ", #windows:" + windows.size());
				}
				double conf = (windows.size() - anomalies.size()) / (double)windows.size(); //conf, that if X->label=normal
				patternRow.clear();
				patternRow.addAll(Arrays.asList(pattern, String.valueOf(windows.size()), String.valueOf(conf), CollectionUtils.join(windows, " "), CollectionUtils.join(anomalies, " ")));
			}

		}
		CSVUtils.saveTable(patterns, new File(patternsFile.getAbsolutePath() + "_metadata.csv"));
	}

	public static Map<Integer, Integer> getWindowLabels(File itemFile) throws IOException {
		Table window2label = MakePatternOccurrences.groupByWindow(itemFile,0,Integer.MAX_VALUE);
		Map<Integer,Integer> window2LabelMap = new HashMap<>();
		// timestamp,value,label,Window
		List<String> columns = window2label.getRows().get(0);
		int windowIdx = columns.indexOf("Window");
		int labelIdx = columns.indexOf("label");
		for(List<String> row: window2label.getRowsStartingFrom1()) {
			if (labelIdx == -1) {
				window2LabelMap.put(Integer.valueOf(row.get(windowIdx)), -1);
			} else {
				List<String> labels = Arrays.asList(row.get(labelIdx).split(";"));
				int label = -1;
				if(labels.contains("1.0")) {
					label = 1;
				}
				else if(labels.contains("-1.0")) {
					label = -1;
				}
				else {
					label = 0; //ok?
				}
				window2LabelMap.put(Integer.valueOf(row.get(windowIdx)), label); //window->label
			}
		}
		return window2LabelMap;
	}
}
