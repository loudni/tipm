package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
public final class MakePatternOccurrences {
	private MakePatternOccurrences() {}

	public static ListMap<Integer,List<String>> groupByWindowGetListMap(File arff, int from, int to) throws IOException {
		Table table = ArffDenseUtils.loadArff(arff, from, to);
		//we get the Window attribute through a random column in the table
		int windowIndex = table.getRows().get(0).indexOf("Window");
		if(windowIndex == -1)
			throw new RuntimeException("No windows avaliable");

		//parse windows
		List<String> column = table.getColumnValues(windowIndex);
		column.remove(0);
		TreeSet<Integer> windowSet = new TreeSet<>();
		for(String value: column) {
			String[] windowValues = value.split(";"); //in case of overlapping windows, can be multiple, e.g. "0;1"
			for(String windowValue: windowValues) {
				if(windowValue.equals("?")) { //should not happen
					System.err.println("Window value is ? (last window is " + windowSet.last() + ")");
					continue;
				}
				windowSet.add(Integer.valueOf(windowValue));
			}

		}
		//group by window
		ListMap<Integer,List<String>> groupByWindow = new ListMap<>(true);
		for(List<String> row: table.getRows().subList(1, table.getRows().size())) {
			String[] overlappingWindows = row.get(windowIndex).split(";"); //in case of overlapping windows, can be multiple, e.g. "0;1"
			for(String windowStr: overlappingWindows) {
				if(windowStr.equals("?"))
					continue;
				Integer window = Integer.valueOf(windowStr);
				groupByWindow.put(window,row);
			}
		}
		System.out.println("Group by. #" + groupByWindow.values().size());
		return groupByWindow;
	}

	//e.g. Make output like
	//	   ["timestamp","value","label","Window"]
	//	   ["2013-07-04 03:00:00 - 2013-07-07 15:00:00",
	//		"8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;6;4;5;4;4;3;3",
	//		"0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;-1;0;0;0",
	//		"0"],
	public static Table groupByWindow(File arff, int from, int to) throws IOException {
		List<String> columns = ArffUtils.getAttributeNames(arff);
		List<String> types = ArffUtils.getAttributeValues(arff);
		ListMap<Integer,List<String>> groupByWindow = groupByWindowGetListMap(arff, from, to);
		Set<Integer> window_values = groupByWindow.keySet();
		//create table/grouped representation:
		//1. for timestamps: return first/last values within group
		//2. for reals/integers/discrete values: return list of values
		//3. for window just return window
		Table tableNew = new Table();
		tableNew.addRow(columns);
		for(Integer window: window_values) {
			List<List<String>> row_group = groupByWindow.get(window);
			List<String> new_row = new ArrayList<>();
			for(int i=0; i<types.size(); i++) {
				String name = columns.get(i);
				String type = types.get(i);
				if(type.startsWith("DATE")) { //if date, or first column, take first and last column
					String new_value = row_group.get(0).get(i) + " - " + row_group.get(row_group.size()-1).get(i);
					new_row.add(new_value);
				}
				else if(name.equalsIgnoreCase("Window")) {
					String new_value = String.valueOf(window);
					new_row.add(new_value);
				}
				else { //make list
					List<String> values = new ArrayList<>();
					for(List<String> row: row_group) {
						String value = row.get(i);
						values.add(value);
					}
					String new_value = CollectionUtils.join(values,";");
					new_row.add(new_value);
				}
			}
			tableNew.addRow(new_row);
		}
		return tableNew;
	}
}
