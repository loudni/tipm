package be.uantwerpen.datamining.pattern_mining;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.time.DateUtils;

import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;

public class MakeWindows {
	private MakeWindows() {}

	/**
	 * Assuming integer intervals, not dates
	 *
	 * @param arff
	 * @param window
	 * @param increment
	 * @throws IOException
	 */
	public static int makeWindows(File arff, File output, int window, int increment) throws IOException {
		if(increment == 0) {
			throw new InvalidParameterException();
		}

		String relation = ArffUtils.getRelationName(arff);
		List<String> names = ArffUtils.getAttributeNames(arff);
		List<String> types = ArffUtils.getAttributeValues(arff);
		List<List<String>> rows = ArffUtils.loadDataMatrix(arff);
		//make windows
		List<Pair<Integer,Integer>> windows = new ArrayList<>(rows.size() / increment);
		for(int pos = 0; pos < rows.size(); pos += increment) {
			windows.add(new Pair<>(pos, Math.max(pos+window, windows.size()))); //exclusive i+window, e.g. window=3, 0-3 and items on index 0,1,2
		}
		//map position to window
		ListMap<Integer, Integer> position2window = new ListMap<>();
		for(int windowId=0; windowId < windows.size(); windowId++) {
			Pair<Integer,Integer> win = windows.get(windowId);
			//pour toute l'intervale
			for(int pos=win.getFirst(); pos<win.getSecond(); pos++) {
				position2window.put(pos, windowId);
			}
		}
		//add columns to data table, with set of windows
		int prevWindowIdx = names.indexOf("Window"); //remove previous window column
		if(prevWindowIdx !=-1) {
			names.remove(prevWindowIdx);
			types.remove(prevWindowIdx);
		}
		names.add("Window");
		types.add("string");

		for(int pos=0; pos<rows.size(); pos++) {
			List<String> row = rows.get(pos);
			if(prevWindowIdx !=-1) {
				row.remove(prevWindowIdx);
			}
			row.add("\"" + CollectionUtils.join(position2window.get(pos),";") + "\"");
		}
		/*
		 * save ARFF, e.g.
		@relation ambient_temp

		@attribute timestamp DATE "yyyy-MM-dd HH:mm:ss"
		@attribute value REAL
		@attribute label {0, -1, 1}

		@data
		"2013-07-04 00:00:00",0.43,0
		*/
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), "UTF-8"));
		writer.write(String.format("@relation %s%n", relation));
		writer.newLine();
		for(int i=0; i<names.size(); i++) {
			writer.write(String.format("@attributes %s %s%n", names.get(i), types.get(i)));
		}
		writer.newLine();
		writer.write("@data\n");
		for(List<String> row: rows)
		{
			writer.append(CSVUtils.serializeLineArff(row));
			if(row != rows.get(rows.size()-1))
				writer.newLine();
		}
		writer.close();
		return windows.size();
	}

	/**
	 * Assuming date intervals
	 *
	 * @param arff
	 * @param window
	 * @param increment
	 * @throws IOException
	 */
	public static int makeWindowsTime(File arff, File output, int windowSeconds, int incrementSeconds) throws IOException {
		String relation = ArffUtils.getRelationName(arff);
		List<String> names = ArffUtils.getAttributeNames(arff);
		List<String> types = ArffUtils.getAttributeValues(arff);
		List<List<String>> rows = ArffUtils.loadDataMatrix(arff);
		//make windows
		List<Pair<Date,Date>> windows = new ArrayList<>();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);
		Date first_date;
		Date last_date;
		try {
			first_date = dateformat.parse(rows.get(0).get(0));
			first_date.setSeconds(0);
			last_date = dateformat.parse(rows.get(rows.size()-1).get(0));
		} catch (ParseException e) {
			throw new RuntimeException("Unable to parse date (assuming yyyy-MM-dd HH:mm:ss format)");
		}
		for(Date date = first_date; date.before(last_date); date = DateUtils.addSeconds(date, incrementSeconds)) {;
			windows.add(new Pair<>(date, DateUtils.addSeconds(date, windowSeconds)));
		}
		//map position to window
		ListMap<Integer, Integer> position2window = new ListMap<>();
		for(int windowId=0; windowId < windows.size(); windowId++) {
			Pair<Date,Date> win = windows.get(windowId);
			//TODOL super un-efficient
			for(int i=0; i<rows.size(); i++) {
				try {
					Date date = dateformat.parse(rows.get(i).get(0));
					if((date.after(win.getFirst()) || date.equals(win.getFirst())) && date.before(win.getSecond())) {
						position2window.put(i, windowId);
					}
					if(date.after(win.getSecond())) {
						break;
					}
				} catch (ParseException e) {
					throw new RuntimeException("Unable to parse date (assuming yyyy-MM-dd HH:mm:ss format)");
				}
			}
		}
		//add columns to data table, with set of windows
		int prevWindowIdx = names.indexOf("Window"); //remove previous window column
		if(prevWindowIdx !=-1) {
			names.remove(prevWindowIdx);
			types.remove(prevWindowIdx);
		}
		names.add("Window");
		types.add("string");
		for(int pos=0; pos<rows.size(); pos++) {
			List<String> row = rows.get(pos);
			if(prevWindowIdx !=-1) {
				row.remove(prevWindowIdx);
			}
			row.add("\"" + CollectionUtils.join(position2window.get(pos),";") + "\"");
		}
		/*
		 * save ARFF, e.g.
		@relation ambient_temp

		@attribute timestamp DATE "yyyy-MM-dd HH:mm:ss"
		@attribute value REAL
		@attribute label {0, -1, 1}

		@data
		"2013-07-04 00:00:00",0.43,0
		*/
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), "UTF-8"));
		writer.write(String.format("@relation %s%n", relation));
		writer.newLine();
		for(int i=0; i<names.size(); i++) {
			writer.write(String.format("@attributes %s %s%n", names.get(i), types.get(i)));
		}
		writer.newLine();
		writer.write("@data\n");
		for(List<String> row: rows)
		{
			writer.append(CSVUtils.serializeLineArff(row));
			if(row != rows.get(rows.size()-1))
				writer.newLine();
		}
		writer.close();
		return windows.size();
	}
}
