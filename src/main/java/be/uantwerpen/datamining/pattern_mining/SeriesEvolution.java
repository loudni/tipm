package be.uantwerpen.datamining.pattern_mining;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;

public class SeriesEvolution {
	private SeriesEvolution() {};

	public static void transform(File arff, File output, String[] columnsToEvolve) throws IOException {
		List<String> types = ArffUtils.getAttributeValues(arff);
		String relation = ArffUtils.getRelationName(arff);
		List<String> names = ArffUtils.getAttributeNames(arff);
		List<List<String>> rows = ArffUtils.loadDataMatrix(arff);
		List<List<String>> newRows = new ArrayList<>(rows.size());

		System.out.println(relation);

		List<Integer> indicies = new ArrayList<>(columnsToEvolve.length);
		for(int i = 0; i < columnsToEvolve.length; i++) {
			int index = names.indexOf(columnsToEvolve[i]);

			if(index == -1) {
				throw new InvalidParameterException("One or more columns are not valid");
			}

			if(!types.get(index).equalsIgnoreCase("REAL"))
				throw new InvalidParameterException("one of the columns is not a valid type");
			indicies.add(index);
		}

		List<String> newTypes = new ArrayList<>(types.size() + indicies.size());
		List<String> newNames = new ArrayList<>(types.size() + indicies.size());

		//TODO: add support for numerical categorical
		for(int i=0; i < types.size(); i++) {
			newTypes.add(types.get(i));
			newNames.add(names.get(i));

			if(indicies.contains(i)) {
				newTypes.add("{ ?, -, /, \\ }");
				newNames.add(names.get(i) + "_EVO");
			}
		}



		for(int rowId = 0; rowId < rows.size(); rowId++) {
			//O(1) since it's an arrayList
			List<String> row = rows.get(rowId);
			List<String> newRow = new ArrayList<>(row.size());
			for(int columnId = 0; columnId < row.size(); columnId++) {
				newRow.add(row.get(columnId));
				if(indicies.contains(columnId)) {
					if(rowId == 0) {
						newRow.add("?");
					} else {
						double previousValue = Double.parseDouble(rows.get(rowId - 1).get(columnId));
						double currentValue = Double.parseDouble(row.get(columnId));
						if(previousValue == currentValue) newRow.add("-");
						else if(previousValue < currentValue) newRow.add("/");
						else newRow.add("\\");
					}
				}
			}
			newRows.add(newRow);
		}

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), "UTF-8"));
		writer.write(String.format("@relation %s%n", relation));
		writer.newLine();
		for(int i=0; i<newNames.size(); i++) {
			writer.write(String.format("@attributes %s %s%n", newNames.get(i), newTypes.get(i)));
		}
		writer.newLine();
		writer.write("@data\n");
		for(List<String> row: newRows) {
			writer.append(CSVUtils.serializeLineArff(row));
			if(row != rows.get(rows.size()-1))
				writer.newLine();
		}
		writer.close();

	}

}
