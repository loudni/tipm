package be.uantwerpen.mime_webapp;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

//see https://stackoverflow.com/questions/17326976/spring-rest-using-jackson-400-bad-request-logging
@ControllerAdvice
public class ControllerConfig {

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handle(HttpMessageNotReadableException e) {
		e.printStackTrace();
		System.err.println("Returning HTTP 400 Bad Request");
		throw e;
	}
}