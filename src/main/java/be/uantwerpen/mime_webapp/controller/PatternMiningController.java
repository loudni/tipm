package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.HashMultimap;

import be.uantwerpen.datamining.images.ImageUtils;
import be.uantwerpen.datamining.images.PhotoMetric;
import be.uantwerpen.datamining.images.RawImage;
import be.uantwerpen.datamining.pattern_mining.MineUsingSPMF;
import be.uantwerpen.datamining.pattern_mining.PatternUtils;
import be.uantwerpen.datamining.pattern_mining.RunPBADEmbeddingOnly;
import be.uantwerpen.ldataminining.model.CountMap;
import be.uantwerpen.ldataminining.model.MultiKeyMap2;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.model.SetMap;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.AlgorithmParameters;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.CommandLineUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Utils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.AnomalyScores;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.ImageFileItem;
import be.uantwerpen.mime_webapp.model.MySession;
import be.uantwerpen.mime_webapp.model.PatternSet;
import be.uantwerpen.mime_webapp.model.Project;

@RestController
public class PatternMiningController extends AbstractController{
	RunPBADEmbeddingOnly runPBADEmbeddingOnly = new RunPBADEmbeddingOnly();

	private String runMining(String columns, HttpServletRequest request, Optional<String> id, String algorithm, boolean isItemset, Map<String, String> parameterMap) {
		try {
			String[] columnsArr = columns.split(",\\s*");
			FileItem currentInput = getCurrentItem(request, id);

			if(!currentInput.isArff())
				throw new RuntimeException("Only Arff supported");

			File data  = currentInput.getFile();

			Pair<File, File> output = MineUsingSPMF.runMining(!isItemset, data, Arrays.asList(columnsArr), algorithm, parameterMap);

			Project newProject = savePatterns(request, id, output.getFirst(), isItemset ? "itemsets" : "sequential patterns", columns, algorithm, parameterMap.get(AlgorithmParameters.MINSUP).replace("%", ""));
			final FileItem newInput = ProjectRepository.getLatestItem(newProject, currentInput.getLogicalName());
			PatternUtils.storePatternSetOccurrences(newInput, newProject, output.getFirst().getName(), output.getSecond());
			PatternUtils.savePatternsWithMetadata(output.getFirst(), output.getSecond(), data);
			return String.format("Found %d patterns.", IOUtils.countLines(output.getFirst()) - 1);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@PostMapping(value="/rest/mining/run-itemsets")
	public @ResponseBody String runItemsetMining(
			@RequestParam("id") Optional<String> id,
			@RequestParam("columns") String columns,
			@NotNull @RequestParam("algorithm") String algorithm,
			@NotNull @RequestParam("support") String support,
			HttpServletRequest request)
	{
		return runMining(columns, request, id, algorithm, true, new HashMap<>());
	}

	@PostMapping(value="/rest/mining/run-sp")
	public @ResponseBody String runSequentialPatternMining(
			@RequestParam("id") Optional<String> id,
			@RequestParam("columns") String columns,
			@NotNull @RequestParam("algorithm") String algorithm,
			HttpServletRequest request,

			//parameters
			@NotNull @RequestParam("support") String support,
			//optional parameters for CM-SPAM
			@RequestParam("MinPatternLength") Optional<String> minPatternLength,
			@RequestParam("MaxPatternLength") Optional<String> maxPatternLength,
			@RequestParam("Required Items") Optional<String> requiredItems,
			@RequestParam("Max gap") Optional<String> maxGap
	) {
		Map<String, String> parameterMap = formatParameters(minPatternLength, maxPatternLength, requiredItems, maxGap, support);
		return runMining(columns, request, id, algorithm, false, parameterMap);
	}

	//new unified route.
	@PostMapping(value="/rest/mining/mine")
	public @ResponseBody String runItemsetMining(
			@RequestParam("id") Optional<String> id,
			@RequestParam("columns") String columns,
			@NotNull @RequestParam("algorithm") String algorithm,
			@NotNull @RequestParam("isItemset") Boolean isItemset,
			HttpServletRequest request,

			//parameters
			@NotNull @RequestParam("support") String support,
			//optional parameters for CM-SPAM
			@RequestParam("MinPatternLength") Optional<String> minPatternLength,
			@RequestParam("MaxPatternLength") Optional<String> maxPatternLength,
			@RequestParam("Required Items") Optional<String> requiredItems,
			@RequestParam("Max gap") Optional<String> maxGap
	) {
		Map<String, String> parameterMap = formatParameters(minPatternLength, maxPatternLength, requiredItems, maxGap, support);
		return runMining(columns, request, id, algorithm, isItemset, parameterMap);
	}

	private Map<String, String> formatParameters(
		Optional<String> minPatternLength,
		Optional<String> maxPatternLength,
		Optional<String> requiredItems,
		Optional<String> maxGap,
		String minsup
	) {
		Map<String, String> parameterMap = new HashMap<>();

		//the names here need to match the names in the the "DescriptionOfParameter" of spmf you can look them up in ca.pfv.spmf.algrithmmanager.descriptions."NAME OF THE ALGORITHM".getParametersDescription()

		parameterMap.put(AlgorithmParameters.MINSUP, minsup + "%");

		if(minPatternLength.isPresent())
			parameterMap.put(AlgorithmParameters.MIN_PATTERN_LENGTH, minPatternLength.get());

		if(maxPatternLength.isPresent())
			parameterMap.put(AlgorithmParameters.MAX_PATTERN_LENGTH, maxPatternLength.get());

		if(requiredItems.isPresent())
			parameterMap.put(AlgorithmParameters.REQUIRED_ITEMS, requiredItems.get());

		if(maxGap.isPresent())
			parameterMap.put(AlgorithmParameters.MAX_GAP, maxGap.get());

		return parameterMap;

	}

	@PostMapping(value="/rest/mining/remove-patternset")
	public @ResponseBody String removePatternset(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		try {
			FileItem currentItem = getCurrentItem(request, id);
			for(PatternSet set: currentItem.getPatterns()) {
				if(set.getFilename().equals(filename)) {
					currentItem.getPatterns().remove(set);
					//remove files:
					File patternsFile = new File(Settings.FILE_FOLDER + set.getFilename());
					if(patternsFile.exists()) {
						patternsFile.delete();
						System.out.println("Deleting " + patternsFile.getName());
					}
					if(set.getFilenameOccurrences() != null && !set.getFilenameOccurrences().equals("")) {
						File occurrencesFile = new File(Settings.FILE_FOLDER + set.getFilenameOccurrences());
						if(occurrencesFile.exists()) {
							occurrencesFile.delete();
							System.out.println("Deleting " + occurrencesFile.getName());
						}
					}
					//update repository
					ProjectRepository.save();
					break;
				}
			}
			return "Removed patterns.";
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@PostMapping(value="/rest/mining/filter-length")
	public ResponseEntity<String> filterPatternsOnLength(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			@RequestParam("minlen") String minLen,
			@RequestParam("maxlen") String maxLen,
			HttpServletRequest request) throws IOException
	{
		//1. get input data
		FileItem currentItem = getCurrentItem(request, id);
		Optional<PatternSet> patternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(filename)).findFirst();

		if(!patternSet.isPresent())
			throw new RuntimeException("patternSet not found");

		int minLenInt = 1;
		int maxLenInt = Integer.MAX_VALUE;

		if(!minLen.isEmpty())
			minLenInt = Integer.parseInt(minLen);

		if(!maxLen.isEmpty())
			maxLenInt = Integer.parseInt(maxLen);


		final int minLength = minLenInt;
		final int maxLength = maxLenInt;
		return PatternUtils.filterPattern(currentItem, filename, (row, index) -> {
			String[] parts = row.get(0).split(" ");
			return parts.length >= minLength && parts.length <= maxLength;
		});

	}

	@PostMapping(value="/rest/mining/filter-support")
	public ResponseEntity<String> filterPatternsOnSupport(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			@RequestParam("topk") String topk,
			HttpServletRequest request) throws IOException
	{
		//1. get input data
		FileItem currentItem = getCurrentItem(request, id);
		Optional<PatternSet> patternSet = currentItem.getPatterns().stream().filter((x) -> x.getFilename().equals(filename)).findFirst();

		if(!patternSet.isPresent())
			return new ResponseEntity<>("patternSet not found", HttpStatus.NOT_FOUND);

		int topkInt = Integer.parseInt(topk);
		//2. transform
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilename());
		//i don't trust that the pattern file is sorted by support
		Table patternsTable = CSVUtils.loadCSV(patternFile);
		//best performing lowest memory intensive way of searching the topk in an unordered list
		PriorityQueue<Pair<Integer, String>> queue = new PriorityQueue<>(topkInt + 1 , (a, b) -> Integer.valueOf(a.getSecond()).compareTo(Integer.valueOf(b.getSecond())));

		ListIterator<List<String>> it = patternsTable.getRowsStartingFrom1().listIterator();
		while(it.hasNext()) {
			queue.add(new Pair<>(it.nextIndex() + 1, it.next().get(1)));
			if (queue.size() > topkInt) {
				queue.poll();
			}
		}

		final List<Integer> topkIds = queue.stream().map(Pair::getFirst).collect(Collectors.toList());

		ResponseEntity<String> response = PatternUtils.filterPattern(currentItem, filename, (row, index) -> topkIds.contains(index));

		currentItem.getStackOperations().add(String.format("Filter support(set=%s,topK=%s)",
			patternSet.get().getLabel(),topk));
		ProjectRepository.save();
		return response;
	}

	//todo: Now only WITHIN 1 patternset, not between patternsets!
	@PostMapping(value="/rest/mining/filter-jaccard")
	public ResponseEntity<String> filterPatternsOnJaccardSimmilarity(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			@RequestParam("threshold") String threshold,
			HttpServletRequest request) throws IOException
	{
		//1. get input data
		FileItem currentItem = getCurrentItem(request, id);
		Optional<PatternSet> patternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(filename)).findFirst();

		if(!patternSet.isPresent())
			return new ResponseEntity<>("patternSet not found", HttpStatus.NOT_FOUND);
		if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().equals("")) {
			return new ResponseEntity<>("patternSet occurrences not found", HttpStatus.NOT_FOUND);
		}

		double thresholdDouble = Double.parseDouble(threshold);
		if(thresholdDouble > 1.0 || thresholdDouble < 0.0) {
			throw new IllegalArgumentException("Threshold must be in (0.0,1.0]");
		}
		//2. transform
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilename());
		File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences());
		Table patternsTable = CSVUtils.loadCSV(patternFile);
		Table patternOccurrencesTable = CSVUtils.loadCSV(patternOccFile);
		//like:
		//Window, PatternId (1-indexed)
		//0, 1
		//0, 5
		//1, 1
		//1, 6
		SetMap<Integer,Integer> pattern2window = new SetMap<>();
		for(int i=1; i<patternOccurrencesTable.getRows().size();i++) {
			Integer window = Integer.valueOf(patternOccurrencesTable.getRows().get(i).get(0));
			Integer patternId = Integer.valueOf(patternOccurrencesTable.getRows().get(i).get(1));
			pattern2window.put(patternId, window);
		}
		Set<Integer> redundantPatterns = new HashSet<>();
		for(int i=1; i<patternsTable.getRows().size(); i++) {
			Set<Integer> windows1 =  pattern2window.get(i);
			for(int j=i+1; j<patternsTable.getRows().size(); j++) {
				Set<Integer> windows2 =  pattern2window.get(j);
				Set<Integer> intersection = new HashSet<>(windows1);
				intersection.retainAll(windows2);
				double jaccardIndex = intersection.size() / (double)(windows1.size() + windows2.size() - intersection.size());
				if(jaccardIndex > thresholdDouble) {
					redundantPatterns.add(j);
				}
				if(i==1 && j==2) {
					System.out.format("Info: Removing redundant patterns using Jaccard.\n" +
								"Pattern 1 windows: %s\n" +
								"Pattern 2 windows: %s\n" +
								"JaccardIndex: %d / (%d + %d -%d)\n" +
								"Threshold: %.3f>%.3f\n",
								windows1, windows2, intersection.size(),
								windows1.size(), windows2.size(), intersection.size(),
								jaccardIndex, thresholdDouble
							);
				}
			}
		}
		Table newTable = new Table();
		newTable.addRow(patternsTable.getRows().get(0));
		for(int i=1; i<patternsTable.getRows().size();i++) {
			if(!redundantPatterns.contains(i))
				newTable.addRow(patternsTable.getRows().get(i));
		}

		return PatternUtils.filterPattern(currentItem, filename, (row, index) -> {
			return !redundantPatterns.contains(index);
		});
	}

	@PostMapping(value="/rest/mining/min-max-filter-support")
	public @ResponseBody ResponseEntity<String> minMaxFilterSupport(
			@RequestParam("id") Optional<String> id,
			@RequestParam("patternFilename") String patternFilename,
			@RequestParam("maxfreq") Optional<Integer> maxFreq,
			@RequestParam("minfreq") Optional<Integer> minFreq,
			HttpServletRequest request) throws IOException
	{
		if(!maxFreq.isPresent() && !minFreq.isPresent()) {
			return new ResponseEntity<>(
					"neither the minFreq nor the maxFreq was specified", new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}


		FileItem currentItem = getCurrentItem(request, id);
		ResponseEntity<String> responseEntity = PatternUtils.filterPattern(currentItem, patternFilename, (row, index) -> {
			int support = Integer.parseInt(row.get(1));

			return !(
				(maxFreq.isPresent() && support > maxFreq.get()) ||
				(minFreq.isPresent() && support < minFreq.get())
			);
		});

		if(minFreq.isPresent() && maxFreq.isPresent())
			currentItem.getStackOperations().add("Removed patterns with support < " + minFreq.get() + " or > " + maxFreq.get());
		else if(minFreq.isPresent())
			currentItem.getStackOperations().add("Removed patterns with support < " + minFreq.get());
		else
			currentItem.getStackOperations().add("Removed patterns with support > " + maxFreq.get());

		return responseEntity;
	}


	//non evolving pattern contain only one item repeated one or multiple times ex: << A, A, A, A >>
	@PostMapping(value="/rest/mining/remove-non-evolving-patterns")
	public ResponseEntity<String> removeNonEvolutionaryPatterns(
		@RequestParam("id") Optional<String> id,
		@RequestParam("patternFilename") String patternFilename,
		@RequestParam("length") int length,
		HttpServletRequest request
	) {
		FileItem currentItem = getCurrentItem(request, id);
		ResponseEntity<String> responseEntity = PatternUtils.filterPattern(currentItem, patternFilename, (row, index) -> {
			String[] pattern = row.get(0).split(" ");
			if(pattern.length == 0) return false;
			//if it's the only value we don't consider it repeating.
			if(pattern.length == 1) return true;
			Set<String> patternSet = new HashSet<>();
			Collections.addAll(patternSet, pattern);

			for(String value: patternSet) {
				int itemLength=0;
				for(String value2: pattern) {
					if(value.equals(value2)) {
						itemLength++;
						if(itemLength >= length) return false;
					} else {
						itemLength = 0;
					}
				}
			}
			return true;
		});
		currentItem.getStackOperations().add("Removed non evolving patterns");
		return responseEntity;
	}


	@PostMapping(value="/rest/mining/filter-time-constraint")
	public ResponseEntity<String> filterPatternsOccOnTimeContraints(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			@RequestParam("maxgap") Optional<String> maxGap,
			@RequestParam("maxspan") Optional<String> maxSpan,
			HttpServletRequest request) throws IOException
	{
		//1. get input data
		FileItem currentItem = getCurrentItem(request, id);
		Optional<PatternSet> patternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(filename)).findFirst();

		if(!patternSet.isPresent())
			return new ResponseEntity<>("Pattern set not found", HttpStatus.NOT_FOUND);
		if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().equals(""))
			return new ResponseEntity<>("Pattern occurrences set not found", HttpStatus.NOT_FOUND);


		int maxGapInt = -1;
		int maxSpanInt = -1;

		if( maxGap.isPresent() && !maxGap.get().trim().isEmpty()) {
			maxGapInt = Integer.parseInt(maxGap.get());
		}

		if( maxSpan.isPresent() && !maxSpan.get().trim().isEmpty()) {
			maxSpanInt= Integer.parseInt(maxSpan.get());
		}

		//2. transform
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilename());
		File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences());
		boolean isItemset = patternSet.get().getType().equals("itemsets");

		MultiKeyMap2<Integer,Integer,List<Integer>> occurrencesWithSpan = searchPatternSpans(patternFile, patternOccFile, currentItem.getFile(), !isItemset);

		// list patternid window
		HashSet<Pair<Integer, Integer>> patternsToRemove = new HashSet<>();
		Collection<Pair<Integer, Integer>> values = occurrencesWithSpan.getKeyPairs();

		for(Pair<Integer, Integer> pair: values) {
			List<Integer> indicies = occurrencesWithSpan.get(pair.getFirst(), pair.getSecond());

			boolean isRemoved = false;
			if(maxSpanInt >= 0) {
				int span = Collections.max(indicies) - Collections.min(indicies) + 1;
				if(span > maxSpanInt) {
					isRemoved = true;
					patternsToRemove.add(pair);
				}
			}

			//no use to check if someting is removable when it is alredy going to be removed
			if(maxGapInt >= 0 && !isRemoved) {
				int currentIndex = indicies.remove(0);
				for(int index : indicies) {
					int gap = index - currentIndex;
					if(gap > maxGapInt) {
						patternsToRemove.add(pair);
						break;
					}
					currentIndex = index;
				}
			}

		}

		// Update occurences
		//patternId support
		Map<Integer, Integer> newSupport = new HashMap<>();
		Table patternOcc = CSVUtils.loadCSV(patternOccFile);

		Iterator<List<String>> it = patternOcc.getRowsStartingFrom1().iterator();
		while(it.hasNext()) {
			List<String> row = it.next();
			Integer windows = Integer.valueOf(row.get(0));
			Integer patternId = Integer.valueOf(row.get(1));
			newSupport.putIfAbsent(patternId, 0);
			if(patternsToRemove.contains(new Pair<>(windows, patternId))) {
				it.remove();
			} else {
				newSupport.put(patternId, newSupport.get(patternId) + 1);
			}
		}

		CSVUtils.saveTable(patternOcc, patternOccFile);

		//Update pattensSupport
		Table pattern = CSVUtils.loadCSV(patternFile);
		int patternId = 1;
		for(List<String> row : pattern.getRowsStartingFrom1()) {
			row.set(1, newSupport.get(patternId).toString());
			patternId++;
		}

		CSVUtils.saveTable(pattern, patternFile);
		ProjectRepository.save();

		//remove patterns without support
		PatternUtils.filterPattern(currentItem, filename, (row, index) -> {
			return Integer.parseInt(row.get(1)) > 0;
		});

		currentItem.getStackOperations().add(String.format("Filter occurrences on time constraints(set=%s,gap=%s,span=%s)",
				patternSet.get().getLabel(), maxGap, maxSpan));
		ProjectRepository.save();
		return new ResponseEntity<>(HttpStatus.OK);
	}


	@GetMapping(value="/rest/mining/dictionary")
	public @ResponseBody ResponseEntity<Map<String, Set<String>>> getDictionary(
			@RequestParam("id") Optional<String> id,
			HttpServletRequest request) throws IOException
	{
		FileItem currentItem = getCurrentItem(request, id);
		File data = currentItem.getFile();

		Table items = null;
		if(currentItem.isArff()) {
			items = ArffDenseUtils.loadArff(data);
		} else {
			items = CSVUtils.loadCSV(data);
		}

		String[] labels = items.getRows().get(0).toArray(new String[0]);
		int nbCols = labels.length;
		Map<String, Set<String>> dictionary = new HashMap<>();

		//create the dictionary
		for(int i = 0; i < nbCols; i++) {
			dictionary.put(labels[i], new HashSet<>());
		}

		for(List<String> row : items.getRowsStartingFrom1()) {
			for(int i = 0; i < nbCols; i++) {
				dictionary.get(labels[i]).add(row.get(i));
			}
		}

		return new ResponseEntity<>(dictionary, new HttpHeaders(), HttpStatus.OK);

	}

	@PostMapping(value="/rest/mining/anomaly-detection-fpof")
	public @ResponseBody String anomalyDetectionFPOF(
			@RequestParam("id") Optional<String> id,
			@RequestParam("patternFilenames") String patternFilenames,
			HttpServletRequest request) throws IOException, InterruptedException
	{
		//1. get input
		FileItem currentItem = getCurrentItem(request, id);
		String[] patternFilenamesArr = patternFilenames.split(",\\s*");
		//2. compute FPOF for each window
		Timer timer = new Timer("FPOF");
		int totalPatterns = 0;
		CountMap<Integer> countsPerWindow = new CountMap<>(true);
		List<String> labelsPatternSets = new ArrayList<>();
		for(String patternFilename: patternFilenamesArr) {
			Optional<PatternSet> patternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(patternFilename)).findFirst();

			if(!patternSet.isPresent())
				throw new RuntimeException("patternSet not found " + patternFilename);
			if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().equals("")) {
				throw new RuntimeException("patternSet occurrences not found " + patternSet.get().getFilenameOccurrences());
			}

			labelsPatternSets.add(patternSet.get().getLabel());
			//compute FPOF for each window
			long noPatterns = patternSet.get().getNoPatterns();
			totalPatterns += noPatterns;
			//Note: Original FPOP work on itemsets, not on mix of different pattern types
			File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences());
			Table occ = CSVUtils.loadCSV(patternOccFile);
			for(int i=1; i<occ.getRows().size(); i++) {
				int windowId = Integer.parseInt(occ.getRows().get(i).get(0));
				countsPerWindow.add(windowId);
				if(i == 1) {
					System.out.format("info: FPOF window 0, after set %s, is %d/%d%n", patternSet.get().getLabel(),
							countsPerWindow.get(windowId), totalPatterns);
				}
			}
		}
		List<Pair<Integer,Float>> fpofFactors = new ArrayList<>();
		int lastWindow = Collections.max(countsPerWindow.getMap().keySet());//technically not correct...
		for(int windowId =0; windowId <lastWindow ; windowId++) {
			if(windowId == 0) {
				System.out.format("info: FPOF window 0 is %d/%d%n", countsPerWindow.get(windowId), totalPatterns);
			}
			float fpof = countsPerWindow.get(windowId) / (float)totalPatterns; //countsPerWindow can be empty (return 0), e.g. if no patterns match!
			fpofFactors.add(new Pair<>(windowId,fpof));
		}
		//3. save results
		//3.1. save scores
		String label = "FPOF(" + CollectionUtils.join(labelsPatternSets, ";") + ")"; //e.g. FPOF(maximal itemsets pc1,pc2; sequential patterns pc1)
		String suffix = "_scores_" + label.replaceAll("[\\(\\);\\,\\s+]", "_") + ".csv";
		File scoreFile = new File(Settings.FILE_FOLDER + currentItem.getId() + suffix);
		Table tableScores = new Table();
		tableScores.addRow(Arrays.asList("Window","Score"));
		for(Pair<Integer,Float> score: fpofFactors) {
			tableScores.addRow(Arrays.asList(score.getFirst().toString(), String.format(Locale.ENGLISH, "%.6f", score.getSecond())));
		}
		CSVUtils.saveTable(tableScores, scoreFile);
		long elapsed = timer.elapsed();
		//3.2 get evaluation metrics
		Map<String,Double> results = computeAnomalyEvaluationMetrics(scoreFile, currentItem.getFile(), "FPOF");

		if(results.containsKey("AUC") && results.containsKey("AP"))
			label = label.substring(0,label.length()-1) + String.format(Locale.ENGLISH, "; AUC=%.3f, AP=%.3f", results.get("AUC"), results.get("AP"));
		else label = label.substring(0,label.length()-1) + "; Unkown precision missing labels";

		//3.3  save project
		AnomalyScores score = new AnomalyScores();
		score.setAlgorithm("FPOF");
		score.setLabel(label);
		score.setFilenamesPatternSets(Arrays.asList(patternFilenamesArr));
		score.setFilename(scoreFile.getName());
		score.setEvaluationResults(results);
		currentItem.getScores().add(score);
		currentItem.getStackOperations().add("Anomaly detection " + label);
		ProjectRepository.save();
		if(results.containsKey("AUC") && results.containsKey("AP"))
			return String.format(Locale.ENGLISH, "Finished FPOF. Took %s. AUC=%.3f, AP=%.3f", Utils.milisToStringReadable(elapsed), results.get("AUC"), results.get("AP"));
		else return String.format(Locale.ENGLISH, "Finished FPOF. Took %s. Unkown precision missing labels", Utils.milisToStringReadable(elapsed));
	}

	@PostMapping(value="/rest/mining/anomaly-detection-pbad")
	public @ResponseBody String anomalyDetectionPBAD(
			@RequestParam("id") Optional<String> id,
			@RequestParam("patternFilenames") String patternFilenames,
			HttpServletRequest request) throws IOException, InterruptedException
	{
		//1. get input
		FileItem currentItem = getCurrentItem(request, id);
		String[] patternFilenamesArr = patternFilenames.split(",\\s*");
		List<String> labelsPatternSets = new ArrayList<>();
		for(String patternFilename: patternFilenamesArr) {
			Optional<PatternSet> patternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(patternFilename)).findFirst();

			if(!patternSet.isPresent())
				throw new RuntimeException("patternSet not found " + patternFilename);

			labelsPatternSets.add(patternSet.get().getLabel());
		}
		//2. compute PBAD for each window
		String label = "PBAD(" + CollectionUtils.join(labelsPatternSets, ";") + ")";
		String suffix = "_scores_" + label.replaceAll("[\\(\\);\\,\\s+]", "_") + ".csv";
		File scoreFile = new File(Settings.FILE_FOLDER + currentItem.getId() + suffix);
		Timer timer = new Timer("PBAD");
		runPBADEmbeddingOnly.computePBADScore(currentItem, scoreFile);
		long elapsed = timer.end();
		//2.2 get evaluation metrics
		Map<String,Double> results = computeAnomalyEvaluationMetrics(scoreFile, currentItem.getFile(),"PBAD");
		if(results.containsKey("AUC") && results.containsKey("AP"))
			label = label.substring(0,label.length()-1) + String.format(Locale.ENGLISH, "; AUC=%.3f, AP=%.3f", results.get("AUC"), results.get("AP"));
		else label = label.substring(0,label.length()-1) + "; Unkown precision missing labels";
		//3. save results
		AnomalyScores score = new AnomalyScores();
		score.setAlgorithm("PBAD");
		score.setLabel(label);
		score.setFilenamesPatternSets(Arrays.asList(patternFilenamesArr));
		score.setFilename(scoreFile.getName());
		score.setEvaluationResults(results);
		currentItem.getScores().add(score);
		currentItem.getStackOperations().add("Anomaly detection " + label);
		ProjectRepository.save();
		if(results.containsKey("AUC") && results.containsKey("AP")) {
			return String.format("Finished PBAD. Took %s. AUC=%.3f, AP=%.3f", Utils.milisToStringReadable(elapsed), results.get("AUC"), results.get("AP"));
		} else {
			return String.format("Finished PBAD. Took %s. Unevluated missing labels", Utils.milisToStringReadable(elapsed));
		}
	}

	private Map<String,Double> computeAnomalyEvaluationMetrics(File scoreFile, File itemFile, String method) throws IOException, InterruptedException {
		Map<Integer, Integer> window2LabelMap = PatternUtils.getWindowLabels(itemFile);
		//Window,Score
		//0,0.246154
		//1,0.093706
		//2,0.030769
		Table scores = CSVUtils.loadCSV(scoreFile);
		scores.getRows().get(0).add("Label");
		for(List<String> row: scores.getRowsStartingFrom1()) {
			int window = (int)Float.parseFloat(row.get(0));
			Integer label = window2LabelMap.get(window);
			row.add(String.valueOf(label));
		}
		File output = new File(scoreFile.getAbsolutePath() + "_with_label.csv");
		CSVUtils.saveTable(scores, output);
		//run python, since sklearn.metrics has everything
		File log = new File("./temp/eval_log.txt");
		//check evaluation script and python are installed
		File evalPython = new File("./src/main/python/compute_anomaly_evaluation.py").getAbsoluteFile();
		if(!new File(Settings.PYTHON3).exists()) {
			String msg = String.format("Evaluation depends on Python3. Please download and install Python3.\n" +
					"Location of Python3 is configured in be.uantwerpen.mime_webapp.Settings.\n" +
					"Expected location of Python3 is: %s", new File(Settings.PYTHON3).getAbsolutePath());
			System.err.println(msg);
		}
		if(!evalPython.exists()) {
			String msg = String.format("Program to compute evaluation not found.\n" +
						"Expected location PBAD is: %s", evalPython);
			System.err.println(msg);
			throw new RuntimeException(msg);
		}
		//run
		String[] command = new String[]{Settings.PYTHON3, evalPython.getAbsolutePath(), output.getAbsolutePath(), method};
		int status = CommandLineUtils.runCommandInUserDir(command, log, 10);
		if(status != 0) {
			String logStr = IOUtils.readFileFlat(log);
			System.err.println("Error running evaluation. Log:");
			System.err.println(logStr);
			throw new RuntimeException("Error running evaluation: " + logStr);
		}
		else {
			List<String> logStr = IOUtils.readFile(log);
			System.out.println(CollectionUtils.join(logStr));
			//last two lines contain AUX and AP, parse
			//>Running /usr/local/bin/python3 compute_anomaly_evaluation.py blabla_scores_FPOF_maximal_itemsets_pc1_.csv_with_label.csv
			//Argument List:['compute_anomaly_evaluation.py', 'blabla_scores_FPOF_maximal_itemsets_pc1_.csv_with_label.csv']
			//AUC: 0.442
			//AP: 0.259
			//<Finished took  1.04 seconds
			if(StringUtils.join(logStr).contains("AUC") && StringUtils.join(logStr).contains("AP")) {
				Map<String,Double> evalMap = new HashMap<>(2);
				//last line
				Double auc = Double.valueOf(logStr.get(logStr.size()-3).split(":")[1]);
				Double ap = Double.valueOf(logStr.get(logStr.size()-2).split(":")[1]);
				evalMap.put("AUC", auc);
				evalMap.put("AP",ap);
				return evalMap;
			}
			return new HashMap<>(0);
		}
	}

	private Project savePatterns(HttpServletRequest request, Optional<String> id, File patternFile, String type, String columns, String algorithm, String support) throws IOException{
		FileItem currentItem = getCurrentItem(request, id);
		Project project = ProjectRepository.findProjectByItem(currentItem);
		//make pattern set object
		PatternSet patternSet = new PatternSet();
		patternSet.setColumns(columns);
		patternSet.setAlgorithm(algorithm);
		patternSet.setSupport(support);
		patternSet.setNoPatterns(IOUtils.countLines(patternFile) - 1);
		patternSet.setFilename(patternFile.getName());
		patternSet.setType(type);
		//save item
		FileItem item;
		if(currentItem instanceof ImageFileItem)
			item = new ImageFileItem((ImageFileItem)currentItem);
		else
			item = new FileItem(currentItem);

		item.setVersion(item.getVersion()+1);
		item.add(patternSet);
		String transform = "Mine " + (type.equals("itemsets")?"itemsets":"sequential patterns") + "(" + columns + ", " + algorithm  + ", " + support + "%)";
		item.getStackOperations().add(transform);
		project.add(item);
		ProjectRepository.saveProject(project);
		//set current session
		if(request.getSession() != null) {
			request.getSession().setAttribute("mySession", new MySession(project.getName(), item.getId()));
		}
		return project;
	}

	/**
	 * //this might look like it has a higher complexity than the original code but this is 100x more efficient
	 * since we divide the complexity by the number of windows
	 * @return truple of window, patternId, elementIndicies
	 * @throws IOException
	 */
	private MultiKeyMap2<Integer, Integer, List<Integer>> searchPatternSpans(File patternFile, File occurrencesFile, File timeseries, boolean sequential) throws IOException {
		MultiKeyMap2<Integer, Integer, List<Integer>> occurrencesIndices = new MultiKeyMap2<>();
		Table occurrencesTable = CSVUtils.loadCSV(occurrencesFile);

		// Windows <Pattern ids>
		HashMultimap<Integer, Integer> occurrences = HashMultimap.create();
		for(List<String> row : occurrencesTable.getRowsStartingFrom1()) {
			int window = Integer.parseInt(row.get(0));
			int patternId = Integer.parseInt(row.get(1));
			occurrences.put(window, patternId);
		}

		// Map of patternId to pattern
		Table patternTable = CSVUtils.loadCSV(patternFile);
		Map<Integer, String> patterns = new HashMap<>();
		int index = 1;
		for(List<String> row : patternTable.getRowsStartingFrom1()) {
			String pattern = row.get(0);
			patterns.put(index++, pattern);
		}

		// Extract indices
		Table timeseriesTable = ArffDenseUtils.loadArff(timeseries);
		Map<Integer, Map<String, List<Pair<Float, Integer>>>> windowGroups = PatternUtils.groupByWindows(timeseriesTable);

		for(Integer window : windowGroups.keySet()) {
			Collection<Integer> patternIds = occurrences.get(window);
			for(Integer patternId : patternIds) {
				List<Integer> indices = new ArrayList<>();
				String pattern = patterns.get(patternId);
				String[] items = pattern.split(" ");

				Map<String, List<Pair<Float, Integer>>> values = windowGroups.get(window);
				if(sequential) {
					int itemIndex = 0;
					for(String item : items) {
						List<Integer> possibleIndicies = null;
						//we list all indicies that match the current item and selectively add the to possible indicies
						for(String requiredItem : item.split(";")) {
							List<Integer> currentIndicies = new ArrayList<>();
							String column = requiredItem.split("=")[0];
							float ItemValue = Float.parseFloat(requiredItem.split("=")[1]);
							List<Pair<Float, Integer>> columnValues = values.get(column);
							for(Pair<Float, Integer> val : columnValues) {
								if(val.getFirst().equals(ItemValue)) {
									currentIndicies.add(val.getSecond());
								}
							}

							if(possibleIndicies == null) {
								possibleIndicies = currentIndicies;
							} else {
								possibleIndicies.retainAll(currentIndicies);
							}
						}
						itemIndex = Collections.min(possibleIndicies);
						indices.add(itemIndex);
					}
				} else {
					//for item sets we don't really care about order so this solution est the simplest
					// and we don't have to deal with two column constraints at the same time
					for(String item : items) {
						String column = item.split("=")[0];
						float value = Float.parseFloat(item.split("=")[1]);

						for(Pair<Float, Integer> val : values.get(column)) {
							if(value == val.getFirst())
								indices.add(val.getSecond());
						}
					}
				}
				occurrencesIndices.put(window, patternId, indices);
			}
		}
		return occurrencesIndices;
	}

	@GetMapping(
		value="/rest/mining/patternRender"
	)
	public ResponseEntity<?> patternRender(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String patternFilename,
			@RequestParam("imageid") int imageId,
			@RequestParam("patternids") String patternIdsString,
			HttpServletRequest request) throws IOException
	{
		FileItem currentItem = getCurrentItem(request, id);
		File imageFile = new File(Settings.IMAGE_CACHE + patternFilename + "@" +  currentItem.getId() + ":" + patternIdsString + ".jpg");

		System.out.println("Rendering image");

		if(imageFile.exists()) {
			FileInputStream fileStream = new FileInputStream(imageFile);
			byte[] bytes = fileStream.readAllBytes();
			fileStream.close();
			return ResponseEntity.ok(bytes);
		}

		final Optional<PatternSet> optPatternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(patternFilename)).findFirst();

		if(!optPatternSet.isPresent()) {
			return ResponseEntity.noContent().build();
		}

		if(!(currentItem instanceof ImageFileItem)) {
			return ResponseEntity.badRequest().body("the item must be from an un-edited image");
		}

		ImageFileItem fileItem = (ImageFileItem)currentItem;

		//TODO: fix this
		/*//dtw image have a window for each pixel and can produce only one image.
		if(fileItem.getImageCount() == 1) {
			return ResponseEntity.badRequest().body("DTW images are not supported for now");
		}*/

		PatternSet patternSet = optPatternSet.get();
		File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.getFilenameOccurrences());
		Byte[][][] image = new Byte[fileItem.getHeight()][fileItem.getWidth()][3];

		for(int i = 0; i < image.length; i++) {
			for(int j = 0; j < image[0].length; j++) {
				image[i][j][0] = 0;
				image[i][j][1] = 0;
				image[i][j][2] = 0;
			}
		}

		Table occurrencesTable = CSVUtils.loadCSV(patternOccFile);

		Set<Integer> requestPatterns = new HashSet<>();
		for(String patternId: patternIdsString.split(",")) {
			if(!patternId.isBlank())
				requestPatterns.add(Integer.valueOf(patternId));
		}

		// Windows <Pattern ids>
		for(List<String> row : occurrencesTable.getRowsStartingFrom1()) {
			int windowId = Integer.parseInt(row.get(0));
			int patternId = Integer.parseInt(row.get(1)) - 1;

			if(requestPatterns.isEmpty() || requestPatterns.contains(patternId)) {
				image[windowId / fileItem.getWidth()][windowId % fileItem.getWidth()][0] = (byte) (new Random(patternId).nextInt(254) + 1);
				image[windowId / fileItem.getWidth()][windowId % fileItem.getWidth()][1] = (byte) (new Random(patternId + 1).nextInt(254) + 1);
				image[windowId / fileItem.getWidth()][windowId % fileItem.getWidth()][2] = (byte) (new Random(patternId + 2).nextInt(254) + 1);
			}
		}

		//grayScale image
		RawImage rawImage = new RawImage(image, PhotoMetric.PHOTOMETRIC_RGB, 8);

		//saving the image for caching
		ImageUtils.convertRawImageToJpg(rawImage, imageFile);

		FileInputStream fileStream = new FileInputStream(imageFile);
		byte[] bytes = fileStream.readAllBytes();
		fileStream.close();
		return ResponseEntity.ok(bytes);
	}
}
