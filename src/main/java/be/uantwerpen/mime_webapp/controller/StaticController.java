package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
/**
 * Searched to long for 'standard' solution using spring-boot/jetty/maven.
 * Now: DIY for loading images/css/javascript
 *
 * @author lfereman
 *
 */
@Controller
public class StaticController {

	//See also: http://stackoverflow.com/questions/16332092/spring-mvc-pathvariable-with-dot-is-getting-truncated
	@GetMapping(value = "/static/{file_name:.+}")
	public void getStaticFile(@PathVariable("file_name") String fileName,  HttpServletResponse response) {
		try {
			File input = new File("src/main/webapp/resources/" + fileName);
			if(!input.exists()) {
				throw new RuntimeException("File " + input.getAbsolutePath() + " doesn't exist");
			}

			// get your file as InputStream
			InputStream is = new FileInputStream(input);
			// copy it to response's OutputStream
			org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
	}

	@GetMapping(value = "/images/{file_name:.+}")
	public void getStaticImageFile(@PathVariable("file_name") String fileName,  HttpServletResponse response) {
		try {
			File input = new File("src/main/webapp/resources/images/" + fileName);
			InputStream is = new FileInputStream(input);
			org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
	}
}
