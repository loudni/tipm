package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import be.uantwerpen.datamining.images.ImageClustering;
import be.uantwerpen.datamining.images.ImageClusteringDTW;
import be.uantwerpen.datamining.images.ImageStore;
import be.uantwerpen.datamining.images.ImageUtils;
import be.uantwerpen.datamining.images.PhotoMetric;
import be.uantwerpen.datamining.images.RawImage;
import be.uantwerpen.datamining.pattern_mining.MakeWindows;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.AutoConvertCSVToArff;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.Triple;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.ImageFileItem;
import be.uantwerpen.mime_webapp.model.Project;

@Controller
public class ImageController extends AbstractController {

	@PostMapping("/image/store")
	public ResponseEntity<Integer[]> store(
		@RequestParam("files") MultipartFile[] files
	) throws IOException {
		List<Integer> fileIds = new ArrayList<>(files.length);
		List<Future<?>> futures = new LinkedList<>();

		//thread pool
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		for(MultipartFile file : files) {
			Future<?> thread = executor.submit(() -> {
				//this function is slow so we are using multiple threads for it
				try {
					fileIds.add(ImageStore.store(file));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			futures.add(thread);
		}

		//waiting for all threads to finish
		executor.shutdown();
		while (!executor.isTerminated()) {
			try {
				executor.awaitTermination(Long.MAX_VALUE, java.util.concurrent.TimeUnit.DAYS);
			} catch (InterruptedException e) {}
		}

		//rethrow exceptions during thread execution
		for(Future<?> future : futures) {
			try {
				future.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new IOException(e);
			}
		}

		return ResponseEntity.ok(fileIds.toArray(new Integer[0]));
	}

	@GetMapping(
		value = "/image/{id}",
		produces = MediaType.IMAGE_JPEG_VALUE
	)
	public ResponseEntity<byte[]> get(
		@PathVariable("id") String id,
		HttpServletResponse response
	) {
		try {
			Pair<File, String> image = ImageStore.retrieveOverview(Integer.parseInt(id));
			InputStream fileContent = new FileInputStream(image.getFirst());
			ResponseEntity<byte[]> responseEntity = ResponseEntity
				.ok()
				.header("Content-Disposition", "attachment; filename=" + image.getSecond())
				.body(fileContent.readAllBytes());

			response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
			fileContent.close();
			return responseEntity;
		} catch(IOException | NumberFormatException exception) {
			exception.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}

	/**
	 * @param imageIds
	 * @return Pair of dataset [canal][pixels][imageIndex] and resolution width x height
	 * @throws IOException
	 */
	private Pair<double[][][], Triple<Integer, Integer, Integer>> getImageData(int[] imageIds) throws IOException {
		/** [canal][pixels][imageIndex] */
		double[][][] dataset = null;

		int width = 0;
		int height = 0;

		int photoMetric = PhotoMetric.PHOTOMETRIC_RGB;

		for(int imageIndex = 0; imageIndex < imageIds.length; imageIndex++) {
			RawImage image = ImageStore.retrieveRaw(imageIds[imageIndex]).getFirst();
			photoMetric = image.photoMetric;

			width = image.getWidth();
			height = image.getHeight();

			if(dataset == null) {
				dataset = new double[image.getCanalCount()][image.getWidth() * image.getHeight()][imageIds.length];
			}

			for(int y = 0; y < image.getHeight(); y++)
				for(int x = 0; x < image.getWidth(); x++)
					for(int canal = 0; canal < image.getCanalCount(); canal++)
						dataset[canal][y * image.getWidth() + x][imageIndex] = image.imageData[y][x][canal].doubleValue();
		}

		return new Pair<>(dataset, new Triple<>(width, height, photoMetric));
	}

	@PostMapping("/image/dtwcluster")
	public ResponseEntity<String> dtwKMeans(
		@RequestParam("ids") int[] ids,
		@RequestParam("k") int k,
		@RequestParam("maxiterations") Optional<Integer> iteration,
		@RequestParam("project") String project,
		@RequestParam("name") String name
	) {
		Project projectEntity = ProjectRepository.findByName(project);
		if(projectEntity == null) {
			return ResponseEntity.badRequest().body("Project not found");
		}

		if(name.isBlank()) {
			return ResponseEntity.badRequest().body("Name cannot be empty");
		}

		if(ids == null || ids.length == 0) {
			return ResponseEntity.badRequest().body("No images selected");
		}

		try {

			Pair<double[][][], Triple<Integer, Integer, Integer>> imageData = getImageData(ids);
			double[][][] dataset = imageData.getFirst();
			Triple<Integer, Integer, Integer> imageSize = imageData.getSecond();

			int[][] result = ImageClusteringDTW.kmeans(dataset, k, iteration.orElse(Integer.MAX_VALUE));

			ImageFileItem fileItem = new ImageFileItem(imageSize.getFirst(), imageSize.getSecond(), dataset.length, 1);
			fileItem.setFilename(name + ".csv");
			fileItem.setLogicalName(name);
			fileItem.setNoRows((long)result[0].length);
			fileItem.setNoColumns((long)result.length);
			fileItem.getStackOperations().add("kmeans DTW clustering for images : " + Arrays.toString(ids) + " - k=" + k);

			Table table = new Table();

			//TODO: check if this is the right order
			table.addRow(getLabels(result.length, imageSize.getThird()));

			for(int i = 0; i < result[0].length; i++) {
				List<String> row = new ArrayList<>(result.length);
				for(int j = 0; j < result.length; j++) {
					row.add(result[j][i] + "");
				}
				table.addRow(row);
			}

			//TODO: refactor with the next function
			CSVUtils.saveTable(table, fileItem.getFile());

			ImageFileItem arffFileItem = new ImageFileItem(fileItem);
			arffFileItem.setFilename(name + ".arff");
			arffFileItem.getStackOperations().add("Converting to arff");
			AutoConvertCSVToArff.run(fileItem.getFile(), false);

			ImageFileItem windowedFileItem = new ImageFileItem(arffFileItem);
			windowedFileItem.setFilename(name + "_windowed.arff");
			windowedFileItem.getStackOperations().add("make-windows by images");
			int windowCount = MakeWindows.makeWindows(arffFileItem.getFile(), windowedFileItem.getFile(), imageSize.getFirst(), imageSize.getFirst());
			windowedFileItem.setWindowCount(windowCount);

			projectEntity.add(fileItem);
			projectEntity.add(arffFileItem);
			projectEntity.add(windowedFileItem);
			ProjectRepository.saveProject(projectEntity);
			deleteImages(ids);
			return ResponseEntity.ok().body(windowedFileItem.getId());

		} catch(IOException exception) {
			exception.printStackTrace();
			return ResponseEntity.badRequest().body("image not found");
		}
	}

	//generate labels for the data type
	private List<String> getLabels(int length, int photoMetric) {
		List<String> list = new LinkedList<>();
		switch(photoMetric) {
		case PhotoMetric.PHOTOMETRIC_RGB:
			list.add("R");
			list.add("G");
			list.add("B");
			for(int i=3; i < length; i++) {
				list.add("extra-canal n°" + i);
			}
			return list;

		case PhotoMetric.PHOTOMETRIC_MINISBLACK:
		case PhotoMetric.PHOTOMETRIC_MINISWHITE:
			list.add("GreyScale");
			for(int i=1; i < length; i++) {
				list.add("extra-canal n°" + i);
			}
			return list;
		default:
			throw new IllegalArgumentException("PhotoMetric not supported");
		}
	}

	@PostMapping("/image/cluster")
	public ResponseEntity<String> kMeans(
		@RequestParam("ids") int[] ids,
		@RequestParam("k") int k,
		@RequestParam("maxiterations") Optional<Integer> iteration,
		@RequestParam("project") String project,
		@RequestParam("name") String name,
		@RequestParam("normalize") Optional<Boolean> normalize, //defaults to true
		@RequestParam("kmeanpp") Optional<Boolean> kmeanpp //k means++  defaults to true
	) {
		Project projectEntity = ProjectRepository.findByName(project);
		if(projectEntity == null) {
			return ResponseEntity.badRequest().body("Project not found");
		}

		if(name.isBlank()) {
			return ResponseEntity.badRequest().body("Name cannot be empty");
		}

		if(ids == null || ids.length == 0) {
			return ResponseEntity.badRequest().body("No images selected");
		}

		try {
			Pair<double[][][], Triple<Integer, Integer, Integer>> imageData = getImageData(ids);
			double[][][] dataset = imageData.getFirst();
			Triple<Integer, Integer, Integer> imageSize = imageData.getSecond();

			int[][] result = ImageClustering.kmeans(dataset, k, iteration.orElse(Integer.MAX_VALUE), normalize.orElse(true), !kmeanpp.orElse(true));

			ImageFileItem fileItem = new ImageFileItem(imageSize.getFirst(), imageSize.getSecond(), dataset.length, ids.length);
			fileItem.setFilename(name + ".csv");
			fileItem.setLogicalName(name);
			fileItem.setNoRows((long)result[0].length);
			fileItem.setNoColumns((long)result.length);
			fileItem.getStackOperations().add("kmeans clustering for images : " + Arrays.toString(ids) + " - k=" + k);

			Table table = new Table();

			table.addRow(getLabels(result.length, imageSize.getThirth()));

			for(int i = 0; i < result[0].length; i++) {
				List<String> row = new ArrayList<>(result.length);
				for(int j = 0; j < result.length; j++) {
					row.add(result[j][i] + "");
				}
				table.addRow(row);
			}

			CSVUtils.saveTable(table, fileItem.getFile());

			ImageFileItem arffFileItem = new ImageFileItem(fileItem);
			arffFileItem.setFilename(name + ".arff");
			arffFileItem.getStackOperations().add("Converting to arff");
			AutoConvertCSVToArff.run(fileItem.getFile(), false);

			ImageFileItem windowedFileItem = new ImageFileItem(arffFileItem);
			windowedFileItem.setFilename(name + "_windowed.arff");
			windowedFileItem.getStackOperations().add("make-windows by images");
			int windowCount = MakeWindows.makeWindows(arffFileItem.getFile(), windowedFileItem.getFile(), ids.length, ids.length);
			windowedFileItem.setWindowCount(windowCount);

			projectEntity.add(fileItem);
			projectEntity.add(arffFileItem);
			projectEntity.add(windowedFileItem);
			ProjectRepository.saveProject(projectEntity);

			deleteImages(ids);
			return ResponseEntity.ok().body(windowedFileItem.getId());

		} catch(IOException exception) {
			exception.printStackTrace();
			return ResponseEntity.badRequest().body("image not found");
		}
	}

	private void deleteImages(int[] ids) throws IOException {
		for(int imageId : ids)
			ImageStore.erase(imageId);
	}

	@GetMapping(
		value = "/image/project",
		produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getImageFromData(
		HttpServletRequest request,
		@RequestParam("id") Optional<String> fileItemId,
		@RequestParam("imageid") int imageid,
		@RequestParam("canalid") int canalId
	) {
		FileItem currentItem = getCurrentItem(request, fileItemId);
		if(!(currentItem instanceof ImageFileItem)) {
			return ResponseEntity.badRequest().body("file is not a image".getBytes());
		}

		ImageFileItem imageFileItem = (ImageFileItem)currentItem;



		try {
			new File(Settings.IMAGE_CACHE).mkdirs();
			File tmpimage = new File(Settings.IMAGE_CACHE + currentItem.getFilename() + imageid + ":" + canalId + ".jpg");
			/*if(tmpimage.exists()) {
				return ResponseEntity.ok(Files.readAllBytes(tmpimage.toPath()));
			}*/
			Table table = ArffDenseUtils.loadArff(currentItem.getFile());

			int imageCount = imageFileItem.getImageCount();

			//[y][x][canal]
			Number[][][] canalImage = new Number[imageFileItem.getHeight()][imageFileItem.getWidth()][1];

			long max = Long.MIN_VALUE;
			List<List<String>> rows = table.getRowsStartingFrom1();
			for(List<String> row : rows) {
				String col = row.get(canalId);
				long val = Long.parseLong(col);
				if(val > max) {
					max = val;
				}
			}

			int x = 0;
			int y = 0;
			for(int i=imageid; i < table.getRowsStartingFrom1().size(); i += imageCount) {
				//we know min is 0 since this is the output of kmeans
				canalImage[y][x][0] = getColor(Long.parseLong(rows.get(i).get(canalId)), 0, max);
				x++;
				if(x == imageFileItem.getWidth()) {
					x = 0;
					y++;
				}
			}

			ImageUtils.convertRawImageToJpg(new RawImage(canalImage, PhotoMetric.PHOTOMETRIC_MINISBLACK, 8), tmpimage);
			return ResponseEntity.ok(Files.readAllBytes(tmpimage.toPath()));

		} catch (IOException e) {
			return ResponseEntity.internalServerError().body(null);
		}
	}

	@GetMapping(value = "/image/projectmeta")
	public ResponseEntity<?> getImageMetadataFromData(
		HttpServletRequest request,
		@RequestParam("id") Optional<String> fileItemId,
		@RequestParam("imageid") int imageid,
		@RequestParam("canalid") int canalId
	) {
		FileItem currentItem = getCurrentItem(request, fileItemId);
		if(!(currentItem instanceof ImageFileItem)) {
			return ResponseEntity.badRequest().body("file is not a image");
		}

		try {
			Table table = ArffDenseUtils.loadArff(currentItem.getFile());
			//[y][x][canal]
			long max = Long.MIN_VALUE;
			List<List<String>> rows = table.getRowsStartingFrom1();
			for(List<String> row : rows) {
				String col = row.get(canalId);
				long val = Long.parseLong(col);
				if(val > max) {
					max = val;
				}
			}

			Map<Long, Integer> map = new HashMap<>();
			for(long i = 0; i < max; i++) {

				// 0xff allow use to convert to int ignoring the fact that Byte is signed (-128 to 127)
				map.put(i, getColor(i, 0, max) & 0xff);
			}

			return ResponseEntity.ok().body(map);
		} catch( IOException exception ) {
			return ResponseEntity.internalServerError().body(null);
		}
	}

	//max as double to get floating point precision for the conversion to byte
	private static Byte getColor(long val, long min, double max) {
		//setting the value as seed so each value correspond to one color
		return (byte) (((val - min) / max) * 255);
	}
}
