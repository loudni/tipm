package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.datamining.pattern_mining.MakePatternOccurrences;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.AnomalyScores;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.PatternSet;

@RestController
public class TableLoadController extends AbstractController{

	@GetMapping(value="/rest/load-data-windows")
	public @ResponseBody Table loadData(@RequestParam("pagination") Optional<String> pagination, @RequestParam("id") Optional<String> id, HttpServletRequest request)
	{
		return loadDataWindows(Optional.of(true), pagination, id, request);
	}

	@GetMapping(value="/rest/load-data")
	public @ResponseBody Table loadDataWindows(@RequestParam("windowed") Optional<Boolean> windowed, @RequestParam("pagination") Optional<String> pagination, @RequestParam("id") Optional<String> id, HttpServletRequest request)
	{
		String page;
		FileItem item = getCurrentItem(request, id);

		if(item == null)
			return null;

		if(!pagination.isPresent() || pagination.get().equals("")){
			page = "0-1000"; //default only first 1000 rows read
		} else {
			page = pagination.get();
		}

		int from = Math.max(0, Integer.valueOf(page.split("-")[0]));
		int to = Integer.parseInt(page.split("-")[1]);
		try {
			if(windowed.isPresent() && windowed.get().booleanValue()) {
				//Access database to get filename
				if(!item.isArff())
					throw new RuntimeException("ARRF input expected");
				return MakePatternOccurrences.groupByWindow(item.getFile(), from, to);
			} else {
				if(item.isArff()){
					return ArffDenseUtils.loadArff(item.getFile(), from, to);
				}
				else {
					return CSVUtils.loadCSV(item.getFile(), from, to);
				}
			}
		}catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping(value="/rest/load-patterns")
	public @ResponseBody Table loadPatterns(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request, id);
		if(item == null)
			return null;
		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");
		if(item.getPatterns().size() == 0 || item.getPatterns().stream().filter((p) -> p.getFilename().equals(filename)).findFirst() == null) {
			throw new RuntimeException("Unable to find pattern set with filename == " + filename);
		}
		try {
			System.out.println("rest>>loadPatterns(" + item.getLogicalName() + "," + filename + ")");
			Table table = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + filename));
			return table;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GetMapping(value="/rest/load-patterns-metadata")
	public @ResponseBody Table loadPatternsMetadata(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request, id
		);
		if(item == null)
			return null;

		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");

		if(item.getPatterns().size() == 0 || item.getPatterns().stream().filter((p) -> p.getFilename().equals(filename)).findFirst() == null) {
			throw new RuntimeException("Unable to find pattern set with filename == " + filename);
		}

		try {
			System.out.println("rest>>loadPatternsMetadata(" + item.getLogicalName() + "," + filename + ")");
			Table table = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + filename + "_metadata.csv"));
			return table;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GetMapping(value="/rest/load-pattern-occ")
	public @ResponseBody Table loadPatternOccurrences(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request, id);

		if(item == null)
			return null;

		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");

		Optional<PatternSet> patternSet = item.getPatterns().stream().filter((p) -> p.getFilename().equals(filename)).findFirst();

		if(!patternSet.isPresent()) {
			throw new RuntimeException("Unable to find pattern set with filename == " + filename);
		}

		if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().isEmpty()) {
			throw new RuntimeException("No occurrences are available for " + filename);
		}

		try {
			System.out.println("rest>>loadPatternsOccurrences(" + item.getLogicalName() + "," + filename + ")");
			return CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GetMapping(value="/rest/load-anomaly-score")
	public @ResponseBody Table loadAnomalyScore(
			@RequestParam("id") Optional<String> id,
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request, id);

		if(item == null)
			return null;

		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");

		Optional<AnomalyScores> anomalyScore = item.getScores().stream().filter((p) -> p.getFilename().equals(filename)).findFirst();
		if(!anomalyScore.isPresent()) {
			throw new RuntimeException("Unable to find anomaly score set with filename == " + filename);
		}

		try {
			System.out.println("rest>>loadAnomalyScore(" + item.getLogicalName() + "," + filename + ")");
			Table table = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + anomalyScore.get().getFilename()));
			return table;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
