package be.uantwerpen.mime_webapp.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import be.uantwerpen.datamining.pattern_mining.MakePatternOccurrences;
import be.uantwerpen.datamining.pattern_mining.MakeWindows;
import be.uantwerpen.datamining.pattern_mining.SeriesEvolution;
import be.uantwerpen.datamining.service.Operations;
import be.uantwerpen.ldataminining.model.CountMap;
import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.NaturalOrderComparator;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils.RowMapper;
import be.uantwerpen.ldataminining.preprocessing.ArffStatistics;
import be.uantwerpen.ldataminining.preprocessing.ArffTransforms;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils.StreamerArffGeneric;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils.StreamerArffRegular;
import be.uantwerpen.ldataminining.preprocessing.AutoConvertCSVToArff;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.preprocessing.SQLQueryOnArff;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.MathUtils;
import be.uantwerpen.ldataminining.utils.Triple;
import be.uantwerpen.ldataminining.utils.Utils;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.MySession;
import be.uantwerpen.mime_webapp.model.Project;

@RestController
public class TransformController extends AbstractController{

	@PostMapping(value="/rest/transform/convert-to-arff")
	public @ResponseBody void convertToArff(
		@RequestParam("id") Optional<String> id,
		HttpServletRequest request
	) {
		try {
			FileItem currentInput = getCurrentItem(request, id);
			System.out.println("currentInput" + currentInput.getFilename());
			if(!currentInput.isCSV())
				throw new RuntimeException("Only csv supported");
			boolean inferAlmost = true;
			AutoConvertCSVToArff.run(currentInput.getFile(), inferAlmost);
			File output = IOUtils.getFileWithDifferentExtension(currentInput.getFile(), "arff");
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output,"converted csv to arff");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@PostMapping(value="/rest/transform/drop-null-columns")
	public @ResponseBody void dropNullColumns(@RequestParam("id") Optional<String> id, HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);
		if(!currentInput.isArff())
			throw new IllegalArgumentException("Only arff is supported");
		try {
			File output = getNewFileName(currentInput);
			//generate output
			List<Map<String,String>> maps = ArffStatistics.getAllAttributeStatisticsFast(currentInput.getFile());
			final List<String> nullAttributes = new ArrayList<String>();
			for(Map<String,String> statsAttribute: maps){
				int noNotNull = Integer.valueOf(statsAttribute.get("noNotNull"));
				if(noNotNull == 0){
					nullAttributes.add(statsAttribute.get("attribute"));
				}
			}
			System.out.println("Dropping " + CollectionUtils.join(nullAttributes));
			ArffTransforms.filterAttributes(currentInput.getFile(), output, new ArffTransforms.AttributeFilter() {

				public boolean keepAttribute(String attributeName) {
					return !nullAttributes.contains(attributeName);
				}
			});
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, "drop null columns");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@RequestMapping(value="/rest/transform/run-sql-query", method= {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody void runSQLQuery(
			@RequestParam("id") Optional<String> id,
			@RequestParam("query") String query,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);
		if(!currentInput.isArff()) {
			System.err.println("Only arff supported");
			throw new IllegalArgumentException("Only arff is supported");
		}
		try {
			System.out.println("Run query:" + query);
			File outputCsv = new SQLQueryOnArff().runQuery(currentInput.getFile(), query);
			boolean inferAlmost = true;
			new AutoConvertCSVToArff().run(outputCsv, inferAlmost);
			File outputArff = IOUtils.getFileWithDifferentExtension(outputCsv, "arff");
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, outputArff, "sql:" + query.substring(0, Math.min(128, query.length())) + "...");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@PostMapping(value="/rest/transform/rename")
	public @ResponseBody void renameColumn(
		@RequestParam("id") Optional<String> id,
		@RequestParam("old_name") String oldName,
		@RequestParam("new_name") String newName,
		HttpServletRequest request) {
			FileItem currentInput = getCurrentItem(request, id);
			if(!currentInput.isArff() || oldName.contains(" ") || newName.contains(" "))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only arff is supported");

			try {
				List<String> attributeNames = ArffUtils.getAttributeNames(currentInput.getFile());
				List<String> newAttributeNames = translateAttributes(oldName + " " + newName, attributeNames);
				ArffUtils.updateAttributeNames(currentInput.getFile(), newAttributeNames);
				currentInput.getStackOperations().add("renamed "+ oldName + " to " + newName);
			} catch(IOException e) {
				throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR, "Foo Not Found", e);
			}
		}

	@PostMapping(value="/rest/transform/translate-labels")
	public @ResponseBody void translateLabels(
			@RequestParam("id") Optional<String> id,
			@RequestParam("labels_txt") String labelsTranslationTxt,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);
		if(!currentInput.isArff())
			throw new IllegalArgumentException("Only arff is supported");
		try {
			List<String> attributeNames = ArffUtils.getAttributeNames(currentInput.getFile());
			List<String> newAttributeNames = translateAttributes(labelsTranslationTxt, attributeNames);
			ArffUtils.updateAttributeNames(currentInput.getFile(), newAttributeNames);
			currentInput.getStackOperations().add("translate labels"); //in place!
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@PostMapping(value="/rest/transform/replace-outlier")
	public @ResponseBody void replaceOutlier(
			@RequestParam("id") Optional<String> id,
			@RequestParam("column") String column,
			@RequestParam("smaller") String smaller,
			@RequestParam("smaller_replace") String smallerReplace,
			@RequestParam("larger") String larger,
			@RequestParam("larger_replace") String largerReplace,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);

		if(!currentInput.isArff())
			throw new IllegalArgumentException("Only arff is supported");
		try {
			File output = getNewFileName(currentInput);
			List<String> names = ArffUtils.getAttributeNames(currentInput.getFile());
			final int idx = names.indexOf(column);
			ArffDenseUtils.mapRowsDense(currentInput.getFile(), output, new ArffDenseUtils.RowMapper() {
				@Override
				public List<String> map(List<String> row) {
					try {
						String val = row.get(idx);
						if(CSVUtils.isEmptyValueInCSV(val)) {
							return row;
						}
						if(!smaller.isEmpty() && Double.valueOf(val) < Double.valueOf(smaller)) {
							row.set(idx,smallerReplace);
						}
						else if(!larger.isEmpty() && Double.valueOf(val) > Double.valueOf(larger)) {
							row.set(idx,largerReplace);
						}}
					catch(Exception e){
						e.printStackTrace();
					}
					return row;
				}
			});
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, "replace outerlier " + column);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@PostMapping(value="/rest/transform/paa")
	public @ResponseBody void runTransformPAA(
			@RequestParam("id") Optional<String> id,
			@RequestParam("columns") String columns,
			@RequestParam("window") String window,
			HttpServletRequest request) {
		try{
			FileItem currentInput = getCurrentItem(request, id);

			if(!currentInput.isArff())
				throw new IllegalArgumentException("Only arff is supported");

			final int iWindow = Integer.parseInt(window);
			final List<String> selectedCols = Arrays.asList(columns.split(",\\s*"));
			File output = getNewFileName(currentInput);
			final List<String> attributes = ArffUtils.getAttributeNames(currentInput.getFile());
			ArffDenseUtils.mapRowsDense(currentInput.getFile(), output, new ArffDenseUtils.RowMapper() {

				List<List<String>> lastKRows = new ArrayList<>();
				@Override
				public List<String> map(List<String> row) {
					if(lastKRows.size() < iWindow -1) {
						lastKRows.add(row);
						return null;
					}
					else { //window rows openen
						lastKRows.add(row);
						List<String> newRow = new ArrayList<>();
						for(int colIdx=0; colIdx < attributes.size(); colIdx++) {
							//add attribute of first row
							if(!selectedCols.contains(attributes.get(colIdx))) {
								newRow.add(lastKRows.get(0).get(colIdx));
							}
							else {
								//attribute needs PAA transform
								double sum = 0.0;
								int count = 0;
								for(List<String> rowK: lastKRows) {
									String value = rowK.get(colIdx);
									if(!value.equals("?")) {
										sum += Double.valueOf(value);
										count++;
									}
								}
								newRow.add(String.valueOf(sum/(double)count));
							}
						}
						lastKRows.clear();
						return newRow;
					}
				}
			});
			//Project projectObj = ProjectRepository.findByName(mySession.getCurrentProject());

			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, "PAA transform (window=" + window + ")");

		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		return;
	}



	@PostMapping(value="/rest/transform/discretize")
	public @ResponseBody void discretize(
			@RequestParam("id") Optional<String> id,
			@RequestParam("columns") String columns,
			@RequestParam("density") String density,
			@RequestParam("bins") String bins,
			HttpServletRequest request)
	{
		FileItem currentInput = null;
		if(id.isPresent()) {
			currentInput = ProjectRepository.findItemById(id.get().trim());
		}
		if( currentInput == null) {
			currentInput = getCurrentItem(request, id);
		}

		if(!currentInput.isArff())
			throw new IllegalArgumentException("Only arff is supported");
		try {
			File output = getNewFileName(currentInput);
			List<String> names = ArffUtils.getAttributeNames(currentInput.getFile());
			String[] columnsArr = columns.split(",\\s*");
			boolean is_density = density.equals("true") || density.equals("1") ;
			int binsInt = Integer.parseInt(bins);
			if(is_density) {
				System.out.println("Density based dicretisation");
				File input = currentInput.getFile();
				for(String name: columnsArr) {
					int idx = names.indexOf(name);
					List<Double> values = new ArrayList<>();
					ArffDenseUtils.mapRowsDense(input, output, new ArffDenseUtils.RowMapper() {
						@Override
						public List<String> map(List<String> row) {
							try {
								String val = row.get(idx);
								if(CSVUtils.isEmptyValueInCSV(val)) {
									return row;
								}
								else {
									Double d = Double.valueOf(val);
									values.add(d);
								}
							}
							catch(Exception e){
							}
							return row;
						}
					});
					Collections.sort(values);
					ArffDenseUtils.mapRowsDense(input, output, new ArffDenseUtils.RowMapper() {
						@Override
						public List<String> map(List<String> row) {
							try {
								String val = row.get(idx);
								if(CSVUtils.isEmptyValueInCSV(val)){
									return row;
								}
								else {
									Double d = Double.valueOf(val);
									int selectedBin = 0;
									for(int bin=0;bin<=binsInt;bin++) {
										if(d>values.get(MathUtils.clamp((values.size() * bin)/ binsInt, 0, values.size()-1))) {
											selectedBin = bin;
										}
									}
									row.set(idx, String.valueOf(selectedBin));
								}
							}
							catch(Exception e){
								e.printStackTrace();
							}
							return row;
						}
					});
					input = output;
				}
			}
			else {
				System.out.println("Width based dicretisation");
				File input = currentInput.getFile();
				List<Map<String,String>> stats = ArffStatistics.getAllAttributeStatisticsFast(input);
				for(String name: columnsArr) {
					System.out.println(name);
					int idx = names.indexOf(name);
					final double min = Double.parseDouble(stats.get(idx).get("min"));
					final double max = Double.parseDouble(stats.get(idx).get("max"));
					final double binSize = (max - min) / binsInt;
					ArffDenseUtils.mapRowsDense(input, output, (List<String> row) -> {
						try {
							String val = row.get(idx);
							if(CSVUtils.isEmptyValueInCSV(val)) {
								return row;
							}
							else {
								Double d = Double.parseDouble(val);
								int bin = (int) ((d - min) / binSize);
								row.set(idx, String.valueOf(bin));
							}
						}
						catch(Exception e){
							e.printStackTrace();
						}
						return row;
					});
					input = output;
				}
			}
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, String.format("discretized %s (bins=%s, method=%s)", columns, bins, is_density?"density":"width"));

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	private List<String> translateAttributes(String labelsTranslationTxt, List<String> labels){
		String[] names = labelsTranslationTxt.trim().split("\\s+");
		System.out.println(names.length + names[0] + names[1]);
		if(names.length %2 != 0) {
			throw new RuntimeException("Invalid label string");
		}

		Map<String,String> translations = new HashMap<>();

		for(int i = 0; i < names.length; i += 2){
			translations.put(names[0], names[1]);
		}

		List<String> newattributeNames = new ArrayList<>();
		for(String attributeName: labels){
			if(translations.get(attributeName) != null){
				newattributeNames.add(translations.get(attributeName));
			}
			else{
				newattributeNames.add(attributeName);
			}
		}
		return newattributeNames;
	}

	@PostMapping(value="/rest/transform/run-date-transform")
	public @ResponseBody void runDateTransform(
			@RequestParam("id") Optional<String> id,
			@RequestParam("dateformat_from") String dateformatFrom,
			@RequestParam("dateformat_to") String dateformatTo,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);

		if(!currentInput.isArff())
			throw new IllegalArgumentException("Only arff is supported");
		try {
			File output = getNewFileName(currentInput);
			boolean transformed_anything = ArffDenseUtils.transformAnyDateColumns(currentInput.getFile(), output, dateformatFrom, dateformatTo);
			System.out.println("Transforming dates from " + dateformatFrom + " to " + dateformatTo);
			if(transformed_anything){
				//why should this be saved in a session ???
				saveNewOutput(request.getSession(), currentInput, output, "date format to " + dateformatTo);

			}
			else{
				System.err.println("No attributes match dataformat input");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}




	@PostMapping(value="/rest/transform/split")
	public @ResponseBody void runTransformSplit(
			@RequestParam("id") Optional<String> id,
			@RequestParam("nullpatterns") String nullpatterns,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);

		if(!currentInput.isArff())
			throw new IllegalArgumentException("Only arff is supported");

		try {
			File output = getNewFileName(currentInput);
			String[] nullpatternsArr = nullpatterns.split(";\\s*");
			List<String> names = ArffUtils.getAttributeNames(currentInput.getFile());
			//debug print
			System.out.println("Split nullpatterns:");
			Set<String> all_columns = new TreeSet<String>();
			Set<String> nullPatternsBinary = new HashSet<String>();
			for(String nullPattern: nullpatternsArr) {
				String[] nullPatternArr = nullPattern.split(",\\s*");
				String nullPatternBinary = "";
				for(String col: nullPatternArr) {
					all_columns.add(col);
				}
				for(int i=0; i<names.size(); i++) {
					if(CollectionUtils.contains(nullPatternArr, names.get(i))) {
						nullPatternBinary += "1";
					}
					else{
						nullPatternBinary += "0";
					}
				}
				nullPatternsBinary.add(nullPatternBinary);
			}
			ArffDenseUtils.mapRowsDense(currentInput.getFile(), output, new ArffDenseUtils.RowMapper() {
				@Override
				public List<String> map(List<String> row) {
					String binary = "";
					for(int i=0; i<row.size(); i++) {
						if(!row.get(i).equals("?")){
							binary += "1";
						}
						else{
							binary += "0";
						}
					}
					if(nullPatternsBinary.contains(binary)) {
						return row;
					}
					else {
						return null;
					}
				}
			});
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, "Filter nullpatterns " + CollectionUtils.join(all_columns));

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@PostMapping(value="/rest/transform/join")
	public @ResponseBody void runTransformJoin(
			@RequestParam("id") Optional<String> id,
			@RequestParam("file") String file,
			HttpServletRequest request) {
		try{
			//file: projectname/itemid-version
			System.out.println("Join(" + file + ")");
			FileItem currentInput = getCurrentItem(request, id);
			String otherItemID = String.format("%s-%03d",  file.split("/")[1].split("-")[0], Integer.valueOf(file.split("/")[1].split("-")[1]));
			FileItem otherItem = ProjectRepository.findItemById(otherItemID);

			if(!currentInput.isArff())
				throw new IllegalArgumentException("Only arff is supported");

			if(!otherItem.isArff())
				throw new RuntimeException("Only arff supported to joun with");

			File output = getNewFileName(currentInput);
			Operations.timeJoin(currentInput, otherItem, output);
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, "join with " + file);

		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		return;
	}

	@PostMapping(value="/rest/transform/transform-columns")
	public @ResponseBody void transformColumns(
			@RequestParam("id") Optional<String> id,
			@RequestParam("columns") String columns,
			@RequestParam("transforms")  String transforms,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);
		if(!currentInput.isArff())
			throw new IllegalArgumentException("Only arff is supported");
		try {
			final List<String> columnsList = new ArrayList<>(Arrays.asList(columns.split(",")));
			List<String> transformsList = Arrays.asList(transforms.split(","));
			File output = getNewFileName(currentInput);
			if(transformsList.contains("remove")){
				System.out.println(columns);
				dropColumns(request, currentInput, columnsList, output);
				return;
			}

			if(transformsList.contains("datetime2sec")){
				date2Seconds(request,currentInput, columnsList, output);
				return;
			}
			if(transformsList.contains("2categorical")){
				makeColumnCategorical(request,columnsList, currentInput, output);
				return;
			}
			//get stats
			List<Map<String,String>> stats = ArffStatistics.getAllAttributeStatisticsFast(currentInput.getFile()); //contains mean/std
			final List<Triple<Integer,Double,Double>> columnData = new ArrayList<>();
			ArrayList<String> toIgnore = new ArrayList<>();
			for(int i=0; i<stats.size(); i++){
				Map<String,String> stat = stats.get(i);
				if(columnsList.contains(stat.get("attribute"))){
					if(stat.get("mean") == null)
					{
						toIgnore.add(stat.get("attribute"));
						continue;
					}
					columnData.add(new Triple<>(i,
						Double.valueOf(stat.get("mean")),
						Double.valueOf(stat.get("std"))));
				}
			}
			columnsList.removeAll(toIgnore);
			if(transformsList.contains("normalize")){
				normalizeColumns(request, currentInput, columnsList, output, columnData);
			}
			else if(transformsList.contains("remove_outliers")){
				removeOutlierColumns(request, currentInput, output, columnsList, columnData);
			}

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void date2Seconds(HttpServletRequest request, FileItem currentInput, List<String> columnsList,
			File output) throws IOException {
		List<String> attributes = ArffUtils.getAttributeNames(currentInput.getFile());
		String from = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat format1 = new SimpleDateFormat(from);
		ArffDenseUtils.mapRowsDense(currentInput.getFile(), output, new ArffDenseUtils.RowMapper() {
			@Override
			public List<String> map(List<String> row) {
				for(String col: columnsList) {
					int colIndex = attributes.indexOf(col);
					String value = row.get(colIndex);
					if(value.equals("?"))//or mean?
						continue;
					try {
						Date d = format1.parse(value);
						long timestamp = d.getTime();
						row.set(colIndex, "" + timestamp);
					} catch (ParseException e) {
					}
				}
				return row;
			}
		});
		//rest: change schema (copy paste  ArffDensUtils.transformAnyDateColumns)
		List<String> attributeTypes = ArffUtils.getAttributeValues(output);
		for(String attributeToConvert: columnsList){
			attributeTypes.set(attributes.indexOf(attributeToConvert), "REAL");
		}
		List<String> header = ArffUtils.makeHeader("dates-fixed", attributes, attributeTypes, false);
		File tempFileWithCorrectHeader = File.createTempFile("bla", "foo");
		IOUtils.saveFile(header, tempFileWithCorrectHeader);
		final BufferedWriter writer = new BufferedWriter(new FileWriter(tempFileWithCorrectHeader, true));
		ArffUtils.streamFile(output, new StreamerArffRegular() {

			@Override
			public void doSomething(List<String> row) {
				try {
					writer.write(CSVUtils.serializeLineArff(row));
					writer.newLine();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
		writer.close();
		tempFileWithCorrectHeader.renameTo(output);
		//why should this be saved in a session ???
		saveNewOutput(request.getSession(), currentInput, output, "from date format to seconds" + CollectionUtils.join(columnsList));


	}

	private void dropColumns(HttpServletRequest request, FileItem currentInput, final List<String> columnsList,
			File output) throws IOException {
		System.out.println("Dropping " + CollectionUtils.join(columnsList));
		ArffTransforms.filterAttributes(currentInput.getFile(), output, attributeName -> !columnsList.contains(attributeName));
		//why should this be saved in a session ???
		saveNewOutput(request.getSession(), currentInput, output, "dropping " + CollectionUtils.join(columnsList));

	}

	private void normalizeColumns(HttpServletRequest request, FileItem currentInput, final List<String> columnsList,
			File output, final List<Triple<Integer, Double, Double>> columnData) throws IOException {
		ArffDenseUtils.mapRowsDense(currentInput.getFile(), output, new ArffDenseUtils.RowMapper() {
			@Override
			public List<String> map(List<String> row) {
				for(Triple<Integer,Double,Double> triple: columnData){
					String value = row.get(triple.getFirst());
					if(value.equals("?"))//or mean?
						continue;
					//normalize: (value - mean)/ std
					double newValue = (Double.valueOf(value) - triple.getSecond())/triple.getThirth();
					row.set((int)triple.getFirst(), "" + newValue);
				}
				return row;
			}
		});
		//why should this be saved in a session ???
		saveNewOutput(request.getSession(), currentInput, output, "normalize " + CollectionUtils.join(columnsList));

	}

	private void makeColumnCategorical(HttpServletRequest request, List<String> columns, FileItem currentInput, File output) throws IOException {
		List<String> cols = ArffUtils.getAttributeNames(currentInput.getFile());
		List<String> types = ArffUtils.getAttributeValues(currentInput.getFile());
		List<List<String>> data = ArffUtils.loadDataMatrix(currentInput.getFile());
		for(String column: columns) {
			List<String> columnValues = ArffUtils.getColumn(data, cols.indexOf(column));
			List<String> sortedDistinctColumnsValues = CollectionUtils.sorted(new TreeSet<String>(columnValues));
			types.set(cols.indexOf(column),String.format("{%s}", CollectionUtils.join(sortedDistinctColumnsValues)));
		}
		ArffUtils.saveFile(output,cols,types,data);
		//why should this be saved in a session ???
		saveNewOutput(request.getSession(), currentInput, output, "change attribute type to categorical " + CollectionUtils.join(columns));

	}

	private void removeOutlierColumns(HttpServletRequest request, FileItem currentInput, File output,  final List<String> columnsList,
			final List<Triple<Integer, Double, Double>> columnData) throws IOException {
		final CountMap<Integer> counts = new CountMap<Integer>();
		ArffDenseUtils.mapRowsDense(currentInput.getFile(), output, new ArffDenseUtils.RowMapper() {
			@Override
			public List<String> map(List<String> row) {
				for(Triple<Integer,Double,Double> triple: columnData){
					String value = row.get(triple.getFirst());
					if(value.equals("?"))
						continue;
					//filter: if value > mean + 2 * sigma or if < mean - 2 * sigma
					double newValue = Double.valueOf(value);
					double max = triple.getSecond() + 5 * triple.getThirth();
					double min = triple.getSecond() - 5 * triple.getThirth();
					if(newValue > max || newValue < min){
						counts.add(triple.getFirst());
						return null;
					}
				}
				return row;
			}
		});
		//why should this be saved in a session ???
		saveNewOutput(request.getSession(), currentInput, output, "remove outliers " + CollectionUtils.join(columnsList));

	}

	@RequestMapping(value="/rest/transform/make-windows", method= {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody void runMakeWindows(
			@RequestParam("id") Optional<String> id,
			@RequestParam("window") String window,
			@RequestParam("increment") String increment,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);

		if(currentInput == null) return;

		if(!currentInput.isArff()) {
			throw new IllegalArgumentException("Only arff is supported");
		}
		try {
			System.out.println("Run windows:" + window + "," + increment);
			File output = getNewFileName(currentInput);
			int windowCount;
			//check if window contains 'd(ay)', 'h(our)', 'm(in)' or 's(ec)'
			if(window.contains("d") || window.contains("h") || window.contains("m") || window.contains("s")) {
				int windowSeconds = parseTimeOffset(window);
				int incrementSeconds = parseTimeOffset(increment);
				windowCount = MakeWindows.makeWindowsTime(currentInput.getFile(), output, windowSeconds, incrementSeconds);
			}
			else {
				windowCount = MakeWindows.makeWindows(currentInput.getFile(), output, Integer.parseInt(window), Integer.parseInt(increment));
			}
			currentInput.setWindowCount(windowCount);
			System.out.println(currentInput.hashCode());
			saveNewOutput(request.getSession(), currentInput, output, String.format("make-windows (window=%s, increment=%s)", window, increment));

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@RequestMapping(value="/rest/transform/evolution", method= {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody void evolution(
			@RequestParam("id") Optional<String> id,
			@RequestParam("columns") String columns,
			HttpServletRequest request) throws IOException
	{
		FileItem currentInput = getCurrentItem(request, id);
		String[] columnsArr = columns.split(",\\s*");

		if(currentInput == null) return;

		if(!currentInput.isArff()) {
			throw new IllegalArgumentException("Only arff is supported");
		}

		File output = getNewFileName(currentInput);
		SeriesEvolution.transform(currentInput.getFile(), output, columnsArr);
		System.out.println(currentInput.hashCode());
		saveNewOutput(request.getSession(), currentInput, output, "Extract evolutions");
	}

	public static class FilterParameter{
		private String col;
		private String predicate;
		private String val;
		private String combination;
		public String getCol() {
			return col;
		}
		public void setCol(String col) {
			this.col = col;
		}
		public String getPredicate() {
			return predicate;
		}
		public void setPredicate(String predicate) {
			this.predicate = predicate;
		}
		public String getVal() {
			return val;
		}
		public void setVal(String val) {
			this.val = val;
		}
		public String getCombination() {
			return combination;
		}
		public void setCombination(String combination) {
			this.combination = combination;
		}
		public String toString() {
			return String.format("%s %s %s %s", col, predicate, val, combination);
		}

	}

	@RequestMapping(value="/rest/transform/filter-windows", method= {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody void runFilterWindows(
			@RequestParam("id") Optional<String> id,
			@RequestParam String parameters,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);
		try {
			System.out.println("Run filter windows:" + parameters); //parameters is list of dictionaries
			File output = getNewFileName(currentInput);
			//parse parameters
			//e.g. [{col: "EVENT_F01", predicate:"contains", val: "203", combination: "or"}, ...]
			//group by
			List<String> cols = ArffUtils.getAttributeNames(currentInput.getFile());
			ListMap<Integer,List<String>> windowsListMap =
					MakePatternOccurrences.groupByWindowGetListMap(currentInput.getFile(), 0, Integer.MAX_VALUE);
			//get list of windows matching predicates
			Set<Integer> keep = new TreeSet<>();
			ObjectMapper objectMapper = new ObjectMapper();

			List<FilterParameter> parametersParsed = objectMapper.readValue(parameters, new TypeReference<List<FilterParameter>>(){});

			System.out.println("PARS:" + parametersParsed );

			for(Integer window: windowsListMap.keySet()) {
				List<List<String>> rows = windowsListMap.get(window);
				boolean matches = false;
				String combination = "";
				for(FilterParameter filter: parametersParsed) {
					boolean matchesAnyRow = false;
					for(List<String> row: rows) {
						if(matches(filter, cols, row)){
							matchesAnyRow = true;
							break;
						}
					}
					if(filter.equals(parametersParsed.get(0))){
						matches = matchesAnyRow;
					}
					else {
						if(combination.equals("and")) {
							matches = matches && matchesAnyRow;
						}
						if(combination.equals("or")) {
							matches = matches || matchesAnyRow;
						}
					}
					combination = filter.combination;
				}
				if(matches) {
					keep.add(window);
				}
			}
			//apply to data
			ArffDenseUtils.mapRowsDense(currentInput.getFile(), output, new RowMapper() {
				int windowIdx = cols.indexOf("Window");
				@Override
				public List<String> map(List<String> row) {
					if(keep.contains(Integer.valueOf(row.get(windowIdx)))) {
						return row;
					}
					else {
						return null;
					}
				}
			});
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, String.format("filter-windows (%s)", CollectionUtils.join(parametersParsed)));

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private NaturalOrderComparator comp = new NaturalOrderComparator();

	/**
	 * @param filter
	 * @param cols liste de colones
	 * @param row liste de lignes
	 * @return
	 */
	private boolean matches(FilterParameter filter, List<String> cols, List<String> row) {
		//larger/smaller/equals/notEquals/contains/notContains
		String colValue = row.get(cols.indexOf(filter.col));
		if(filter.predicate.equals("equals")) {
			return colValue.equals(filter.val);
		}
		else if(filter.predicate.equals("notEquals")) {
			return !colValue.equals("?") && !colValue.equals(filter.val);
		}
		else if(filter.predicate.equals("smaller")) {
			return comp.compare(colValue, filter.val) < 0;
		}
		else if(filter.predicate.equals("larger")) {
			return comp.compare(colValue, filter.val) > 0;
		}
		else {
			return false;
		}
	}

	public static class TransformParameter{
		private String col;
		private String transform;
		private String parameter;

		public String getCol() {
			return col;
		}


		public void setCol(String col) {
			this.col = col;
		}


		public String getTransform() {
			return transform;
		}


		public void setTransform(String transform) {
			this.transform = transform;
		}


		public String getParameter() {
			return parameter;
		}


		public void setParameter(String parameter) {
			this.parameter = parameter;
		}


		public String toString() {
			return String.format("%s %s %s", col, transform, parameter);
		}

	}

	@RequestMapping(value="/rest/transform/groupby-windows", method= {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody void runGroupByWindows(
			@RequestParam("id") Optional<String> id,
			@RequestParam String parameters,
			HttpServletRequest request)
	{
		FileItem currentInput = getCurrentItem(request, id);
		try {
			System.out.println("Run group by windows:" + parameters);
			File output = getNewFileName(currentInput);
			List<String> cols = ArffUtils.getAttributeNames(currentInput.getFile());
			ListMap<Integer,List<String>> listmap =
					MakePatternOccurrences.groupByWindowGetListMap(currentInput.getFile(), 0, Integer.MAX_VALUE);
			//get list of windows matching predicates
			Set<Integer> keep = new TreeSet<>();
			ObjectMapper objectMapper = new ObjectMapper();
			List<TransformParameter> parametersParsed = objectMapper.readValue(parameters, new TypeReference<List<TransformParameter>>(){});
			System.out.println("PARS:" + parametersParsed );
			List<String> newCols = new ArrayList<String>();
			newCols.add(cols.get(0));
			for(TransformParameter transformParameter: parametersParsed) {
				newCols.add(transformParameter.transform + "_" + transformParameter.col);
			}
			newCols.add("Window");
			Table table = new Table();
			table.addRow(newCols);
			for(Integer window: listmap.keySet()) {
				List<List<String>> rows = listmap.get(window);
				/**
				 * Transforms:
				 * 				  <option value="min">Min</option>
		                          <option value="max">Max</option>
		                          <option value="mean">Mean</option>
		                          <option value="count">Count values</option>
		                          <option value="contains">If contains (parameter: x) set 1, else 0</option>
		                          ...
		                          <option value="paa">Piecewise Aggregate (parameter: window)</option>
		                          <option value="unique">Set of values</option>
				 */
				//for simple transforms
				List<String> newRowSingle = new ArrayList<String>();
				newRowSingle.add(rows.get(0).get(0));
				Map<String, List<Pair<String,String>>> newRowsUnique = new TreeMap<>();
				Map<String, List<Pair<String,String>>> newRowsPAA = new TreeMap<>();
				for(TransformParameter transformParameter: parametersParsed) {
					int colIdx = cols.indexOf(transformParameter.col);
					List<String> vals = new ArrayList<String>();
					for(List<String> row: rows){
						String val = row.get(colIdx);
						if(!val.equals("?"))
							vals.add(val);
					}
					if(vals.size() == 0) {
						newRowSingle.add("");
						continue;
					}
					if(transformParameter.transform.equals("min")) {
						Collections.sort(vals, comp);
						String min = vals.get(0);
						newRowSingle.add(min);
					}
					else if(transformParameter.transform.equals("max")) {
						Collections.sort(vals, comp);
						String max = vals.get(vals.size()-1);
						newRowSingle.add(max);
					}
					else if(transformParameter.transform.equals("mean")) {
						Double sum = 0.0;
						Double count = 0.0;
						for(String s: vals) {
							sum += Double.valueOf(s);
							count += 1;
						}
						newRowSingle.add(String.format(Locale.ENGLISH, "%.6f", sum/count));
					}
					else if(transformParameter.transform.equals("count")) {
						newRowSingle.add(String.valueOf(vals.size()));
					}
					else if(transformParameter.transform.equals("contains")) {
						if(vals.contains(transformParameter.parameter)) {
							newRowSingle.add("1");
						}
						else {
							newRowSingle.add("0");
						}
					}
					else if(transformParameter.transform.equals("unique")) {
						String newColName = transformParameter.transform + "_" + transformParameter.col;
						List<Pair<String,String>> newRowsCurrent = new ArrayList<>();
						Set<String> set = new HashSet<>();
						for(List<String> row: rows){
							String time = row.get(0);
							String val = row.get(colIdx);
							if(!val.equals("?"))
								if(!set.contains(val)) {
									newRowsCurrent.add(new Pair<>(time,val));
									set.add(val);
								}
						}
						newRowSingle.add("");
						newRowsUnique.put(newColName, newRowsCurrent);
					}
					else if(transformParameter.transform.equals("paa")) {
						String newColName = transformParameter.transform + "_" + transformParameter.col;
						List<Pair<String,String>> newRowsCurrent = new ArrayList<>();
						int windowPar = Integer.valueOf(transformParameter.parameter);
						List<Double> valsDouble = new ArrayList<Double>();
						for(List<String> row: rows){
							String time = row.get(0);
							String val = row.get(colIdx);
							if(!val.equals("?")) {
								valsDouble.add(Double.valueOf(val));
								if(valsDouble.size() == windowPar) {
									newRowsCurrent.add(new Pair<>(time, String.format(Locale.ENGLISH, "%.6f", MathUtils.mean(valsDouble))));
									valsDouble.clear();
								}
							}
						}
						newRowSingle.add("");
						newRowsPAA.put(newColName, newRowsCurrent);
					}
				}
				//save all
				List<List<String>> newRows = new ArrayList<>();
				newRowSingle.add(String.valueOf(window));
				newRows.add(newRowSingle);
				for(Map<String, List<Pair<String,String>>> map: Arrays.asList(newRowsUnique, newRowsPAA)) {
					for(String newCol: map.keySet()) {
						int idx = newCols.indexOf(newCol);
						for(Pair<String,String> pair: map.get(newCol)) {
							List<String> newRow = new ArrayList<>();
							for(String c: newCols)
								newRow.add("");
							newRow.set(0, pair.getFirst());
							newRow.set(idx, pair.getSecond());
							newRow.set(newCols.size()-1, String.valueOf(window));
							newRows.add(newRow);
						}
					}
				}
				Collections.sort(newRows, (l1,l2) -> l1.get(0).compareTo(l2.get(0)));
				//append to ALL windows
				for(List<String> row: newRows) {
					table.addRow(row);
				}
			}
			File tableCSV = new File("./temp/table.csv");
			CSVUtils.saveTable(table, tableCSV);
			File arff = new AutoConvertCSVToArff().run(tableCSV, false);
			arff.renameTo(output);
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, String.format("transform-group-by-window(%s)", CollectionUtils.join(parametersParsed)));

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private int parseTimeOffset(String window) {
		int multWindow = 0;
		if(window.contains("d")) {
			multWindow = 24 * 60 * 60;
		}
		else if(window.contains("h")) {
			multWindow = 60 * 60;
		}
		else if(window.contains("m")) {
			multWindow = 60;
		}
		else{
			multWindow = 1;
		}
		int windowSeconds = 0;
		for (int i = 0; i < window.length(); i++){
			char c = window.charAt(i);
			if(!Character.isDigit(c)) {
				windowSeconds = Integer.valueOf(window.substring(0, i)) * multWindow;
				return windowSeconds;
			}
		}
		return -1;
	}

	@RequestMapping(value="/rest/mining/run-corr", method= {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody String runCorrelationAnalysis(@RequestParam("id") Optional<String> id, @RequestParam("remove") String doRemove, HttpServletRequest request)
	{
		try {
			FileItem currentInput = getCurrentItem(request, id);
			if(!currentInput.isArff())
				throw new RuntimeException("Only Arff supported");
			runCorrelationAnalysis(currentInput, doRemove.toLowerCase().trim().equals("true"), request);
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			return String.format("Error: %s", e.getMessage());
		}
	}

	//TODO: move to transform
	public String runCorrelationAnalysis(FileItem currentInput, boolean remove, HttpServletRequest request) throws IOException
	{
		System.out.format("runCorrelationAnalysis(%s,%b)\n", currentInput.getLogicalName(), remove);
		List<Map<String,String>> stats = ArffStatistics.getAllAttributeStatisticsFast(currentInput.getFile());
		final ListMap<String, Double> numbers = new ListMap<String,Double>();
		final List<String> allAttributes = ArffUtils.getAttributeNames(currentInput.getFile());
		final List<String> types = ArffUtils.getAttributeValues(currentInput.getFile());
		final Map<String,String> typesIndexed = new HashMap<String,String>();
		for(int i=0; i< allAttributes.size(); i++){
			typesIndexed.put(allAttributes.get(i), types.get(i));
		}
		ArffUtils.streamSpareOrNormalFile(currentInput.getFile(), new StreamerArffGeneric() {
			@Override
			public void doSomething(List<String> attributes, List<String> row) {
				for(int i=0; i<row.size();i++){
					String value = row.get(i);
					if(!value.equals("?")){
						String name = attributes.get(i);
						String type = typesIndexed.get(name);
						if(type.equals("INTEGER") || type.equalsIgnoreCase("NUMERIC") || type.equals("REAL")){
							numbers.put(name,Utils.asDouble(value));
						}
					}
				}
			}
		});
		List<Pair<Integer,Integer>> pairs = new ArrayList<>();
		for(int i=0; i<allAttributes.size(); i++) {
			for(int j=i+1; j<allAttributes.size(); j++) {
				//skip not-normalized attributes (or other types)
				if( stats.get(i).get("mean") == null || Double.valueOf(stats.get(i).get("mean")) > 0.001 ||
					stats.get(j).get("mean") == null || Double.valueOf(stats.get(j).get("mean")) > 0.001 ) {
					continue;
				}
				pairs.add(new Pair<>(i,j));
			}
		}
		ListMap<String,String> correlated = new ListMap<>();
		for(Pair<Integer,Integer> pair: pairs) {
			String att1 = allAttributes.get(pair.getFirst());
			String att2 = allAttributes.get(pair.getSecond());
			System.out.format("Comparing correlation between %s and %s.%n", att1, att2);
			if(numbers.get(att1) != null && numbers.get(att2) != null) {
				//DO NOT COMPUTE ON FIRST 100 values -> normalisation is global, does not work!
				double resultf = 0.0;
				for(int i=0; i<numbers.get(att1).size() && i < numbers.get(att2).size(); i++) {
					resultf += numbers.get(att1).get(i) * numbers.get(att2).get(i);
				}
				resultf /= Math.min(numbers.get(att1).size(), numbers.get(att2).size());
				System.out.println("correlated att1-att2 (n=" +  Math.min(numbers.get(att1).size(), numbers.get(att2).size()) + "): " + att1 + ", " + att2 + " " + (resultf));
				if(resultf > 0.98 || resultf < -0.98) {
					correlated.put(att1, att2);
				}
				else {
					System.out.println("not correlated att1-att2: " + att1 + ", " + att2 + " " + resultf);
				}
			}
		}
		if(remove && correlated.values().size() > 0) {
			List<String> toKeep = new ArrayList<>(allAttributes);
			toKeep.removeAll(correlated.values());
			List<String> columnsList = new ArrayList<>(allAttributes);
			columnsList.removeAll(toKeep);
			System.out.println("Correlation analysis: dropping " + CollectionUtils.join(columnsList));
			File output = new File(currentInput.getFile().getParentFile(), currentInput.getFile().getName() + "_remove.arff");
			ArffTransforms.filterAttributes(currentInput.getFile(), output, new ArffTransforms.AttributeFilter() {

				public boolean keepAttribute(String attributeName) {
					return !columnsList.contains(attributeName);
				}
			});
			//why should this be saved in a session ???
			saveNewOutput(request.getSession(), currentInput, output, "Correlation Analysis: dropping redundant columns " + CollectionUtils.join(columnsList));

		}
		else {
			System.out.println("Correlation analysis: no correlations to remove.");
		}
		return "";
	}



	private File getNewFileName(FileItem currentItem) {
		int nextVersion = ProjectRepository.findNextVersion(currentItem.getLogicalName());
		return currentItem.generateNewOutputFilename(nextVersion);
	}


	private void saveNewOutput(HttpSession session, FileItem currentItem, File output, String transform) throws IOException{
		System.out.println(currentItem.hashCode());
		int nextVersion = ProjectRepository.findNextVersion(currentItem.getLogicalName());
		FileItem item = new FileItem(currentItem);
		item.setNoRows(ArffUtils.getNumberOfRows(output));
		item.setNoColumns((long)ArffUtils.getNumberOfAttributes(output));
		item.setFilename(output.getName());
		item.setVersion(nextVersion);
		item.getStackOperations().clear();
		item.getStackOperations().add(transform);
		item.getId();
		Project project = ProjectRepository.findProjectByItem(currentItem);
		project.add(item);
		ProjectRepository.saveProject(project);
		if(session != null) {
			session.setAttribute("mySession", new MySession(project.getName(), item.getId()));
		}
	}
}
