//TODO: GET LEGAL RIGHT TO DISTRIBUTE THE .JAR

package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.PatternSet;
import hybrid.model.HybridSoftClustering;
import hybrid.model.ResultModel;

@RestController
public class MltClusteringController extends AbstractController {
	@PostMapping(value = "/image/mtlClustering")
	public synchronized ResponseEntity<?> mtlClustering(
		HttpServletRequest request,
		@RequestParam("id") Optional<String> fileItemId,
		@RequestParam("filename") String patternFilename,
		@RequestParam("maxuncovered") Optional<Integer> maxUncovered,
		@RequestParam("maxstack") Optional<Integer> maxStack
	) throws IOException {
		FileItem currentItem = getCurrentItem(request, fileItemId);
		final Optional<PatternSet> optPatternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(patternFilename)).findFirst();
		if(!optPatternSet.isPresent()) {
			return ResponseEntity.badRequest().body("pattern not found");
		}
		PatternSet patternSet = optPatternSet.get();
		File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.getFilenameOccurrences());
		Table occurrencesTable = CSVUtils.loadCSV(patternOccFile);

		File clustering = generateOccurrencesFile(occurrencesTable);

		String[] args = {
			"-c", clustering.getAbsolutePath(),
			"--notc", maxUncovered.orElse(0).toString(),
			"--nbc", maxStack.orElse(1).toString()
		};


		ResultModel[] results = HybridSoftClustering.runAsApi(args);
		if(results.length == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(results[results.length - 1]);
	}



	private File generateOccurrencesFile(Table table) throws IOException {
		List<List<String>> rows = table.getRowsStartingFrom1();
		Map<Integer, List<Integer>> windowPatterns = new HashMap<>();

		for(List<String> row : rows) {
			Integer window = Integer.valueOf(row.get(0));
			Integer patternId = Integer.valueOf(row.get(1));

			windowPatterns.putIfAbsent(patternId, new LinkedList<>());
			windowPatterns.get(patternId).add(window);
		}

		File file = new File(Settings.TMP_CLUSTERING_FILE);
		FileWriter fw = new FileWriter(file);

		for(Integer patternId : windowPatterns.keySet()) {
			fw.append(patternId + "\n");
			fw.append(" " + CollectionUtils.join(windowPatterns.get(patternId), " ") + "\n");
		}

		fw.close();
		return file;
	}
}
