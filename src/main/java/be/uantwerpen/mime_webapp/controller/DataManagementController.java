package be.uantwerpen.mime_webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.MySession;
import be.uantwerpen.mime_webapp.model.Project;

@RestController
public class DataManagementController extends AbstractController{

	@RequestMapping("/")
	public String index() {
		return  "<html><head><meta http-equiv=\"refresh\" content=\"1;url=/list\"/></head><body>Welcome!</body></html>";
	}

	@GetMapping(value="/rest/projects")
	public @ResponseBody List<Project> getAllProjects()
	{
		return ProjectRepository.findAll();
	}

	@GetMapping(value="/rest/project/{name}")
	public @ResponseBody Project getProject( @PathVariable("name") String name)
	{
		return ProjectRepository.findByName(name);
	}

	@GetMapping(value="/rest/item/set-current")
	public @ResponseBody void setCurrent(
			@RequestParam("project") String project,
			@RequestParam("item") String itemId,
			HttpServletRequest request)
	{
		if(itemId.equals("") || project.equals(""))
			return;
		System.out.println("rest>>set-current(" + project + "," + itemId + ")");
		//Access database to get filename
		FileItem item = ProjectRepository.findItemById(itemId);
		if(!item.isArff() && !item.isCSV())
			throw new RuntimeException("Only ARRF or CSV input expected");
		request.getSession().setAttribute("mySession",new MySession(project,itemId));
	}

	@PostMapping(value="/rest/item/remove")
	public @ResponseBody void removeItem(
				@RequestParam("item") String itemId,
				HttpServletRequest request)
	{
		FileItem removedItem = ProjectRepository.removeItem(itemId);
		for(Project project: ProjectRepository.findAll()){
			for(FileItem item: project.getFileItems()){
				if(item.getLogicalName().equals(removedItem.getLogicalName())
					&& item.getVersion() == removedItem.getVersion() - 1) {
					//Found previous version, save as current
					request.getSession().setAttribute("mySession",new MySession(project.getName(),item.getId()));
				}
			}
		}
	}

	@PostMapping(value="/rest/project/create")
	public @ResponseBody Project addProject( @RequestParam("name") String name )
	{
		Project project = getProject(name);
		if(project !=null)
		{
			throw new RuntimeException("Project with same name already exists");
		}
		return ProjectRepository.saveProject(new Project( name ) );
	}
}
