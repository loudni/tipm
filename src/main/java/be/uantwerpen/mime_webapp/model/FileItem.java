package be.uantwerpen.mime_webapp.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.mime_webapp.Settings;


public class FileItem implements Serializable {
	private static final long serialVersionUID = -5141658337025657977L;

	private String logicalName;
	private int version=0;
	//id used for serialization and storage
	private String id;

	private List<String> stackOperations = new LinkedList<>();
	private String filename;

	private int windowCount = 0;

	private Long noColumns;
	private Long noRows;

	private List<PatternSet> patterns = new ArrayList<>();
	private List<AnomalyScores> scores = new ArrayList<>();

	//back link to project, give 'infinite' loop error when saving

	public FileItem() {}

	public FileItem(FileItem other) {
		this.setNoRows(other.getNoRows());
		this.setNoColumns(other.getNoColumns());
		this.setLogicalName(other.getLogicalName());
		this.setWindowCount(other.getWindowCount());
		this.setFilename(other.getFilename());
		this.setVersion(other.getVersion()+1);
		this.setStackOperations(new LinkedList<>(other.getStackOperations()));
		this.setPatterns(new ArrayList<>(other.getPatterns()));
		this.setScores(new ArrayList<>());
		this.getId();
	}

	public void setWindowCount(int windowCount) {
		this.windowCount = windowCount;
	}

	public int getWindowCount() {
		return windowCount;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getLogicalName() {
		return logicalName;
	}

	public void setLogicalName(String logicalName) {
		this.logicalName = logicalName;
	}

	public Long getNoColumns() {
		return noColumns;
	}

	public void setNoColumns(Long noColumns) {
		this.noColumns = noColumns;
	}

	public Long getNoRows() {
		return noRows;
	}

	public void setNoRows(Long noRows) {
		this.noRows = noRows;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<String> getStackOperations() {
		return stackOperations;
	}

	public void setStackOperations(List<String> stackOperations) {
		this.stackOperations = stackOperations;
	}

	public List<PatternSet> getPatterns() {
		return patterns;
	}

	public void setPatterns(List<PatternSet> patterns) {
		this.patterns = patterns;
	}

	//methods:
	public void add(PatternSet set) {
		//if pattern set with same algorithm and input columns, and higher support -> remove pattern set
		for(PatternSet set2: this.patterns) {
			if(set2.getAlgorithm().equals(set.getAlgorithm())
				&& set2.getColumns().equals(set.getColumns())
				&& Integer.valueOf(set2.getSupport()).compareTo(Integer.valueOf(set.getSupport())) > 0){
				this.patterns.remove(set2);
				break;
			}
		}
		this.patterns.add(set);
	}

	public List<AnomalyScores> getScores() {
		return scores;
	}

	public void setScores(List<AnomalyScores> scores) {
		this.scores = scores;
	}

	public boolean isArff(){
		return this.filename.toLowerCase().endsWith(".arff");
	}

	public boolean isCSV(){
		return this.filename.toLowerCase().endsWith(".csv");
	}

	public String getId(){
		id = String.format("%s-%03d", logicalName, version);
		return id;
	}

	public File getFile(){
		return new File(Settings.FILE_FOLDER, this.filename);
	}

	@Override
	public String toString() {
		return String.format(
				"Item[id='%s']",  getId());
	}

	public File generateNewOutputFilename(){
		return new File(Settings.FILE_FOLDER, String.format("%s-%03d.%s", logicalName, version+1, IOUtils.getExtension(getFile())));
	}

	public File generateNewOutputFilename(int nextVersion){
		return new File(Settings.FILE_FOLDER, String.format("%s-%03d.%s", logicalName, nextVersion, IOUtils.getExtension(getFile())));
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof FileItem)) {
			return false;
		}
		return getId().equals(((FileItem)obj).getId());
	}

}
