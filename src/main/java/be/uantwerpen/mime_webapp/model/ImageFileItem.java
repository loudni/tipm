package be.uantwerpen.mime_webapp.model;

public class ImageFileItem extends FileItem {
    private final int width;
    private final int height;
    private final int canalCount;
    private final int image_count;

    public ImageFileItem(int width, int height, int canal_count, int image_count) {
        this.width = width;
        this.height = height;
        this.canalCount = canal_count;
        this.image_count = image_count;
    }

    public ImageFileItem(ImageFileItem other) {
        super(other);
        this.width = other.width;
        this.height = other.height;
        this.canalCount = other.canalCount;
        this.image_count = other.image_count;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getImageCount() {
        return image_count;
    }

    public int getCanalCount() {
        return canalCount;
    }
}
