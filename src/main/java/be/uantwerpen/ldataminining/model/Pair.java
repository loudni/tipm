package be.uantwerpen.ldataminining.model;

public class Pair<F,S> {
	private F first;
	private S second;

	public Pair(F first, S second) {
		this.first = first;
		this.second = second;
	}

	public void setFirst(F first) {
		this.first = first;
	}

	public void setSecond(S second) {
		this.second = second;
	}

	public F getFirst() {
		return first;
	}

	public S getSecond() {
		return second;
	}

	@Override
	public String toString()
	{
		return String.format("%s:%s", first, second);
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null || obj.getClass().equals(this.getClass()))
			return false;

		return ((Pair<?,?>)obj).first.equals(this.first) && ((Pair<?,?>)obj).second.equals(this.second);
	}

	@Override
	public int hashCode() {
		return first.hashCode() + second.hashCode();
	}
}
