package be.uantwerpen.ldataminining.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Table {

	private List<List<String>> rows = new ArrayList<>();
	private long totalSize = 0;

	public long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

	public List<String> dropFirst(){
		List<String> first = rows.get(0);
		rows.remove(first);
		return first;
	}


	public void setRows(List<List<String>> rows){
		this.rows = rows;
	}

	public void addRow(List<String> row)
	{
		rows.add(row);
		totalSize+=1;
	}

	public List<List<String>> getRows()
	{
		return rows;
	}

	public List<List<String>> getRowsStartingFrom1()
	{
		return rows.subList(1, rows.size());
	}

	public List<String> getColumnValues(int colIdx)
	{
		List<String> columnValues = new ArrayList<>();
		for(List<String> row: rows)
		{
			columnValues.add(row.get(colIdx));
		}
		return columnValues;
	}

	public int getColumnCountNotNull(int colIdx){
		int count = 0;
		for(List<String> row: rows)
		{
			String col = row.get(colIdx);
			if(!col.equals("?"))
				count++;
		}
		return count;
	}

	public Set<String> getDistinctColumnValues(int colIdx)
	{
		TreeSet<String> set = new TreeSet<String>();
		for(List<String> row: rows)
		{
			set.add(row.get(colIdx));
		}
		return set;
	}

	public Table selectWhereColEquals(int colIdx, Object value)
	{
		Table filtered = new Table();
		for(List<String> row: getRows())
		{
			if(row.get(colIdx).equals(value))
			{
				List<String> newRow = new ArrayList<String>();
				newRow.addAll(row);
				filtered.addRow(newRow);
			}
		}
		return filtered;
	}

	public void dropColumn(int colIdx)
	{
		for(List<String> row: getRows())
		{
			row.remove(colIdx);
		}
	}

	public int size(){
		return rows.size();
	}

	public void columnFindAndReplace(int colIdx, String with, String by) {
		int i=0;
		for(List<String> row: rows)
		{
			String col = row.get(colIdx);
			if(col.equals(with)){
				row.set(colIdx, by);
				i++;
			}
		}
		System.out.println("columnFindAndReplace: Replaced " + i + " values");
	}


}
