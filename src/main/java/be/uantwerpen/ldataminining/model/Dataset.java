package be.uantwerpen.ldataminining.model;

import java.io.File;

public class Dataset
{
	public enum ClassAttributeOrder{
		FIRST, LAST
	}

	private String name;
	private File file;
	private File preProcessedFile = null;
	private char seperator = ',';
	private char quote = '\"';
	private ClassAttributeOrder classAttributeOrder;
	private String[] columnLabels;
	private boolean sparse = false;


	public Dataset(){}

	public Dataset(String name, File file, File output) {
		super();
		this.name = name;
		this.file = file;
		this.preProcessedFile = output;
	}

	public Dataset(String name, File file, File output, char seperator, char quoteChar) {
		super();
		this.name = name;
		this.file = file;
		this.preProcessedFile = output;
		this.seperator = seperator;
		this.quote = quoteChar;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}



	public boolean isSparse() {
		return sparse;
	}

	public void setSparse(boolean sparse) {
		this.sparse = sparse;
	}

	public File getPreProcessedFile() {
		return preProcessedFile;
	}

	public void setPreProcessedFile(File preProcessedFile) {
		this.preProcessedFile = preProcessedFile;
	}

	public ClassAttributeOrder getClassAttributeOrder() {
		return classAttributeOrder;
	}

	public void setClassAttributeOrder(ClassAttributeOrder classAttributeOrder) {
		this.classAttributeOrder = classAttributeOrder;
	}

	public char getSeperator() {
		return seperator;
	}

	public void setSeperator(char seperator) {
		this.seperator = seperator;
	}

	public char getQuote() {
		return quote;
	}

	public void setQuote(char quote) {
		this.quote = quote;
	}

	public String[] getColumnLabels() {
		return columnLabels;
	}

	public void setColumnLabels(String[] columnLabels) {
		this.columnLabels = columnLabels;
	}



}