package be.uantwerpen.ldataminining.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Utilities code for running programs command line.
 *
 * @author lfereman
 *
 */
public class CommandLineUtils {
	//see http://stackoverflow.com/questions/14165517/
	public static class StreamGobbler extends Thread {
		InputStream is;
		PrintStream os;

		public StreamGobbler(InputStream is, PrintStream os) {
			this.is = is;
			this.os = os;
		}

		@Override
		public void run() {
			try {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null)
				{
					os.println(line);
					os.flush();
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	public static int runCommandInUserDir(String[] command, File log, long timeOutSeconds) throws FileNotFoundException, InterruptedException{
		log.getParentFile().mkdirs();
		PrintStream out = new PrintStream(new FileOutputStream(log));
		int result = CommandLineUtils.runCommand(command, out, out, new File(System.getProperty("user.home")), timeOutSeconds);
		out.flush();
		out.close();
		return result;
	}

	/**
	 * Runs program command line.
	 *
	 * e.g. runCommand({"ls", "-lah"}, System.out, System.err , '/temp')
	 *
	 * Command: specify program and all arguments as array
	 * PrintStream out and err: can be System.out or IOUtils.getPrintStream(logFile) for file
	 * Workdir: null or directory where command is executed
	 * @throws InterruptedException
	 *
	 */
	public static int runCommand(final String[] command, PrintStream out, PrintStream err, File workingDirectory, long timeOutSeconds)
			throws InterruptedException{
		try{
			long timeStart = System.currentTimeMillis();
			String started = String.format(">Running %s%n", CollectionUtils.join(command, " "));
			System.out.print(started);
			if(out != System.out)
				out.print(started);
			//see http://stackoverflow.com/questions/14165517/
			ProcessBuilder pb = new ProcessBuilder().command(command);
			if(workingDirectory != null)
				pb.directory(workingDirectory);
			pb.redirectErrorStream(true);
			final Process p = pb.start();
			CommandLineUtils.StreamGobbler errorGobbler = new CommandLineUtils.StreamGobbler(p.getErrorStream(),  err);
			// any output?
			CommandLineUtils.StreamGobbler outputGobbler = new CommandLineUtils.StreamGobbler(p.getInputStream(),  out);
			// start gobblers
			outputGobbler.start();
			errorGobbler.start();
			// add shutdown hook -> If Java is killed, subprocessed should be killed
			// unsure if this works...
			// See also http://stackoverflow.com/questions/269494/how-can-i-cause-a-child-process-to-exit-when-the-parent-does
			Thread closeChildThread = new Thread() {
				public void run() {
					try{
						p.exitValue(); //Check if process active, if not exception
					}
					catch(IllegalThreadStateException e)
					{
						//process is active -> Destoy
						System.out.format("Killing %s\n", command[0]);
						p.destroy();
					}
				}
			};
			Runtime.getRuntime().addShutdownHook(closeChildThread);
			boolean result;
			if(timeOutSeconds > 0) {
				result = p.waitFor(timeOutSeconds, TimeUnit.SECONDS);
			} else {
				result = p.waitFor() == 0;
			}

			if(!result) {
				p.destroy();
				err.flush();
				out.flush();
				String msg = String.format("Triggered timeout of %d seconds after running %s\n", timeOutSeconds, CollectionUtils.join(command, " "));
				throw new InterruptedException(msg);
			}
			else {
				double elapsed = (System.currentTimeMillis() - timeStart)/1000.0;
				if(p.exitValue() != 0)
				{
					String msg = String.format("Error running %s", Arrays.toString(command));
					err.println( msg);
					System.err.println( msg);
				}
				String ended = String.format("<Finished took %5.2f seconds", elapsed);
				System.out.println(ended); out.println(ended);
				err.flush();
				err.close();
				out.flush(); //new?
				out.close();
				return p.exitValue();
			}
		}
		catch(java.lang.InterruptedException interupted) {
			String msg = String.format("Triggered timeout of %d seconds after running %s\n", timeOutSeconds, CollectionUtils.join(command, " "));
			System.err.println(msg);
			throw new InterruptedException(msg);
		}
		catch(Exception e)
		{
			System.err.format("Error running %s\n", Arrays.toString(command));
			e.printStackTrace();
			return -1;
		}
	}
}
