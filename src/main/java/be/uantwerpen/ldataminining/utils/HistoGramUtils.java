package be.uantwerpen.ldataminining.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import be.uantwerpen.ldataminining.model.Pair;

public class HistoGramUtils {
	private HistoGramUtils() {}

	public static Pair<Integer[], Double[]> getHistogram(List<Double> data, double min, double max, int numBins, int distinct) {
		if(numBins < distinct) {
			Map<Integer, List<Double>> histogram = new TreeMap<>();
			final double binSize = (max - min)/numBins;

			for(Double d : data) {
				if(d == null) continue;

				int bin = (int) ((d - min) / binSize);
				List<Double> values = histogram.get(bin);
				if(values == null) {
					values = new LinkedList<>();
					histogram.put(bin, values);
				}
				values.add(Double.valueOf(bin));
			}

			Double[] labels = histogram.values().stream().map(MathUtils::mean).toArray(Double[]::new);
			Integer[] values = histogram.values().stream().map(List::size).toArray(Integer[]::new);

			return new Pair<>(values, labels);
		} else {
			//since we have less distinct values than binds we will reduce the number of bins to the number of distinct and
			//we will no longer works with ranges
			Map<Double, Integer> histogram = new TreeMap<>();
			for(Double d : data) {
				if(d == null) continue;
				Integer integer = histogram.get(d);
				if(integer == null)integer = 0;
				histogram.put(d, ++integer);
			}

			Double[] labels = histogram.keySet().toArray(new Double[0]);
			Integer[] values = histogram.values().toArray(new Integer[0]);

			return new Pair<>(values, labels);
		}
	}


	//TODO: optimise
	public static int[] calcHistogramEqualDensity(List<Double> values, double min, double max ,int numBins) {
		final int[] result = new int[numBins+1];
		for (int i=0; i<= numBins; i++){
			result[i] = values.size()/(numBins+1);
		}
		return result;
	}

	public static double[] calcHistogramLegendEqualDensity(List<Double> sorted, double min, double max ,int numBins) {
		//e.g. numbins =3, |values|=90
		//values[0], values[30], values[60], values[90]
		final double[] result = new double[numBins+1];
		for(int i=0; i<=numBins; i++) {
			int idx = MathUtils.clamp((int)(sorted.size() * i/ (double)numBins), 0, sorted.size()-1);
			result[i] = sorted.get(idx);
		}
		return result;
	}
}
