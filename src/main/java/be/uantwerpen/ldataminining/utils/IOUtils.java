package be.uantwerpen.ldataminining.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;

import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;

/**
 * Generic utils for:
 *   Saving/loading text files.
 *   Finding string in files
 *   Filename manipulation
 *
 * @author lfereman
 *
 */
public class IOUtils {

	public static Pair<File,File> randomSplit(File textfile, double percentageTraining) throws IOException{
		List<String> lines = IOUtils.readFile(textfile);
		Collections.shuffle(lines);
		int numberOfTraining = (int)Math.ceil(lines.size() * percentageTraining);
		File output1 =  IOUtils.getTempFileWithDifferentExtension(textfile, "0-" + numberOfTraining);
		File output2 =  IOUtils.getTempFileWithDifferentExtension(textfile, numberOfTraining + "-" + lines.size());
		IOUtils.saveFile(lines.subList(0, numberOfTraining),output1);
		IOUtils.saveFile(lines.subList(numberOfTraining,lines.size()),output2);
		return new Pair<>(output1,output2);
	}

	public static List<Pair<File,File>> randomFolds(File textfile, int folds) throws IOException{
		List<String> data = IOUtils.readFile(textfile);
		Random seed = new Random(0);
		Collections.shuffle(data, seed); //fix seed
		List<Pair<File,File>> pairs = new ArrayList<>();
		int testSize = data.size()/folds;
		for(int i=0; i<folds; i++){
			int startTest = i * testSize;
			int endTest = startTest + testSize;
			List<String> contentTraining = new ArrayList<String>();
			contentTraining.addAll(data.subList(0, startTest));
			contentTraining.addAll(data.subList(endTest, data.size()));
			List<String> contentTest = new ArrayList<String>();
			contentTest.addAll(data.subList(startTest, endTest));
			File train = IOUtils.getTempFileWithDifferentExtension(textfile, "train-fold-" + i + ".libsvm");
			File test = IOUtils.getTempFileWithDifferentExtension(textfile, "test-fold-" + i + ".libsvm");
			IOUtils.saveFile(contentTraining ,train);
			IOUtils.saveFile(contentTest, test);
			pairs.add(new Pair<>(train,test));
		}
		return pairs;
	}

	public static void printHead(File file) throws IOException{
		printHead(file,10);
	}

	public static void printHead(File file, int n) throws IOException{
		System.out.println("====== HEAD " + file.getName() + " ========");
		List<String> lines = readFileUntil(file, n);
		for(String line: lines)
			System.out.println(line);
	}

	public static void printTail(File file, int n) throws IOException{
		System.out.println("====== TAIL " + file.getName() + " ========");
		//TODO: less efficient
		List<String> lines = readFileUntil(file, Integer.MAX_VALUE);
		lines = lines.subList(Math.max(0,lines.size()-n), lines.size());
		for(String line: lines)
			System.out.println(line);
	}

	public static int removeEmptyFilesRecursively(File dir){
		int count = 0;
		File[] files = dir.listFiles();
		if(files == null)
			return count;
		for(File file: files){
			if(file.isDirectory()){
				count += removeEmptyFilesRecursively(file);
			}
			else if(isEmptyFile(file)){
				file.delete();
				System.out.println("Removing " + file.getName());
				count++;
			}
		}
		return count;
	}

	public static void copy(File f1, File f2) throws IOException{
		if(!f2.exists())f2.createNewFile();
		FileUtils.copyFile(f1,f2);
	}

	public static File copyPartOfFile(File file, int from, int to) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		File output = new File(file.getParentFile(), file.getName() + "_part_" + from + "_" + to + ".csv");
		FileWriter writer = new FileWriter(output);
		String current = reader.readLine();
		int line = 1;
		while(current != null)
		{
			if(line >= from){
				writer.write(current);
				writer.write("\n");
			}
			if(line > to){
				break;
			}
			current = reader.readLine();
			line++;
		}
		reader.close();
		writer.close();
		System.out.println("Saved " + output);
		return output;
	}


	public static void appendFiles(File in, File in2, File output) throws IOException{
		stream2(in, output, new Streamer2() {

			@Override
			public String doLine(String line) {
				return line;
			}
		}, false);
		stream2(in2, output, new Streamer2() {

			@Override
			public String doLine(String line) {
				return line;
			}
		}, true);
	}

	public static void removeFirstLine(File in, File out) throws IOException{
		List<String> lines = readFile(in);
		lines = lines.subList(1, lines.size());
		saveFile(lines, out);
	}

	public static void saveFile(String s, File file) throws IOException
	{
		saveFile(s, file, Encoding.DEFAULT);
	}

	public static <K,V> void saveMapAsCSV(Map<K,V> map, File out, String col1, String col2) throws IOException{
		if(!out.getParentFile().exists())
			out.getParentFile().mkdirs();
		BufferedWriter writer = new BufferedWriter(new FileWriter(out));
		writer.write(String.format("%s,%s%n", col1, col2));
		for(Entry<K,V> entry: map.entrySet()){
			writer.write(String.format("%s,%s%n",
					CSVUtils.escapeValueForCSV(String.valueOf(entry.getKey())),
					CSVUtils.escapeValueForCSV(String.valueOf(entry.getValue()))));
		}
		writer.flush();
		writer.close();
		System.out.format("Saved %s\n", out.getName());
	}

	public static void saveFile(String s, File file, Encoding encoding) throws IOException
	{
		if(!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		BufferedWriter writer = null;
		if(encoding.equals(Encoding.DEFAULT))
			 writer = new BufferedWriter(new FileWriter(file));
		else if(encoding.equals(Encoding.UTF_8))
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		else
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "ISO-8859-1"));
		writer.write(s);
		writer.flush();
		writer.close();
		System.out.format("Saved %s\n", file.getName());
	}

	public static void saveFile(List<String> content, File file) throws IOException
	{
		if(!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		int idx = 0;
		for(String line: content)
		{
			writer.write(line);
			if(idx != content.size()-1)
				writer.write("\n");
			idx++;
		}
		writer.flush();
		writer.close();
		System.out.format("Saved %s\n", file.getName());
	}

	public static boolean isEmptyFile(File file)
	{
		long len = file.length();
		return len <= 1;
		/*
		//BufferedReader reader = new BufferedReader(new FileReader(file));
		String current = reader.readLine();
		reader.close();
		return current.isEmpty();
		*/
	}

	public static String readFileFlat(File file) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuffer buff = new StringBuffer();
		String current = reader.readLine();
		while(current != null)
		{
			buff.append(current);
			buff.append("\n");
			current = reader.readLine();
		}
		reader.close();
		return buff.toString();
	}

	public enum Encoding{
		DEFAULT,
		UTF_8,
		ISO_8859_1
	}

	public static String readFirstLine(File file) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String current = reader.readLine();
		reader.close();
		return current;
	}

	public static List<String> readFile(File file) throws IOException{
		return readFile(file, Encoding.DEFAULT, -1);
	}

	public static List<String> readFile(File file, Encoding encoding) throws IOException
	{
		return readFile(file, encoding, -1);
	}

	public static List<String> readFile(File file, Encoding encoding, int maxLines) throws IOException
	{
		BufferedReader reader = null;
		if(encoding == Encoding.UTF_8)
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		else if(encoding == Encoding.ISO_8859_1)
		{
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));
		}
		else
			reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		int idx =0;
		String current = reader.readLine();
		idx++;
		while(current != null)
		{
			lines.add(current);
			if(idx >= maxLines && maxLines !=-1)
				break;
			current = reader.readLine();
			idx++;
		}
		reader.close();
		return lines;
	}

	public static void findAndReplace(File input, File output, final CharSequence str, final CharSequence replacement) throws IOException{
		File temp = output;
		if(input.getPath().equals(output.getPath())){
			temp = File.createTempFile("pref", "suff");
		}
		stream2(input, temp, new Streamer2() {

			@Override
			public String doLine(String line) {
				return line.replace(str, replacement);
			}
		}, false);
		if(!temp.getPath().equals(output.getPath()))
			copy(temp, output);
	}

	public interface Streamer{
		public void doLine(String line);
	}

	public interface Streamer2{
		public String doLine(String line);
	}

	public static class Streamer2Identity implements Streamer2{

		@Override
		public String doLine(String line) {
			return line;
		}
	}

	public static void stream(File input, Streamer streamer) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(input));
		String current = reader.readLine();
		while(current != null)
		{
			streamer.doLine(current);
			current = reader.readLine();
		}
		reader.close();
	}

	public static void stream2(File input, File output, Streamer2 streamer2, boolean append) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(input));
		BufferedWriter writer = new BufferedWriter(new FileWriter(output, append));
		String current = reader.readLine();
		while(current != null)
		{
			String out = streamer2.doLine(current);
			if(out!=null){
				writer.write(out);
				writer.newLine();
			}
			current = reader.readLine();
		}
		reader.close();
		writer.close();
	}

	public static List<String> readFileUntil(File file, int lineNumber) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String current = reader.readLine();
		int line = 1;
		while(current != null)
		{
			lines.add(current);
			current = reader.readLine();
			line++;
			if(line >= lineNumber)
				break;
		}
		reader.close();
		return lines;
	}

	public static List<String> readFileUntil(File file, String stopOnPrefix) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String current = reader.readLine();
		while(current != null)
		{
			lines.add(current);
			current = reader.readLine();
			if(current.startsWith(stopOnPrefix))
				break;
		}
		reader.close();
		return lines;
	}

	public static List<String> readFileFrom(File file, String startOnPrefix) throws IOException
	{
		List<String> lines = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String current = reader.readLine();
		boolean started = false;
		while(current != null)
		{
			started = started || current.startsWith(startOnPrefix);
			if(started)
				lines.add(current);
			current = reader.readLine();
		}
		reader.close();
        return lines;
	}

	public static List<String> readFileFrom(File file, int line) throws IOException
	{
		List<String> lines = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String current = reader.readLine();
		int lineNumber =0;
		while(current != null)
		{
			lineNumber++;
			if(lineNumber >= line)
				lines.add(current);
			current = reader.readLine();
		}
		reader.close();
        return lines;
	}


	public static String getFilenameNoExtension(File file)
	{
		int idx = file.getName().lastIndexOf(".");
		if(idx == -1)
			return file.getName();
		else
			return file.getName().substring(0, idx);
	}

	public static String getExtension(File file)
	{
		int idx = file.getName().lastIndexOf(".");
		if(idx == -1)
			return "";
		else
			return file.getName().substring(idx+1);
	}

	public static File createPrefixFile(File dir, File file, String formatingString){
		return new File(dir, String.format(formatingString, IOUtils.getFilenameNoExtension(file)));
	}

	public static File getFileWithDifferentExtension(File file, String ext)
	{
		return new File(file.getParentFile(), getFilenameNoExtension(file) + "." + ext);
	}

	public static File getTempFileWithDifferentExtension(File file, String ext)
	{
		return new File("./temp", getFilenameNoExtension(file) + "." + ext);
	}


	public static File getFileWithDifferentSuffix(File file, String suffix)
	{
		return new File(file.getParentFile(), getFilenameNoExtension(file) + suffix + "." + getExtension(file));
	}

	public static File getFileWithDifferentSuffix(File file, String suffix, String extension)
	{
		return new File(file.getParentFile(), getFilenameNoExtension(file) + suffix + "." + extension);
	}

	/**
	 * After file is read as list of lines (see readFile), these functions can help in parsing simple files
	 *
	 * @param lines
	 * @param prefix
	 * @return
	 */
	public static int indexOf(List<String> lines, String prefix) {
		int idx  = 0;
		for(String line: lines){
			if(line.startsWith(prefix)){
				return idx;
			}
			idx++;
		}
		return -1;
	}

	public static long countLines(File input) throws IOException {
		//see http://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
        LineNumberReader  lnr = new LineNumberReader(new FileReader(input));
        lnr.skip(Long.MAX_VALUE);
        long lines = lnr.getLineNumber();
        // Finally, the LineNumberReader object should be closed to prevent resource leak
        lnr.close();
        return lines + 1; //+1 right?
	}

	public static void prefixLines(File inputFile, File outputFile, String data) throws IOException {
		IOUtils.saveFile(data, outputFile);
		stream2(inputFile, outputFile, new Streamer2() {

			@Override
			public String doLine(String line) {
				return line;
			}
		}, true);//append
	}

}
