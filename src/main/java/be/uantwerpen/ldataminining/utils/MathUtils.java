package be.uantwerpen.ldataminining.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.commons.math3.distribution.NormalDistribution;

public class MathUtils {

	public static Random rand = new Random(1);

	public static int randomInteger(int min, int maxInclusive){
	    int randomNum = rand.nextInt(maxInclusive - min + 1) + min;
	    return randomNum;
	}

	public static double randomDouble(double min, double maxExclusive){
	    double randomNum = rand.nextDouble(); //[0,1)
	    return min + (maxExclusive - min) * randomNum;
	}

	public static double min(double... x)
	{
		double min = x[0];
		for(double i: x)
			min = Math.min(min, i);
		return min;
	}

	public static double[] makeRange(double min, double maxExclusive, double delta){
		int size = (int) Math.floor((maxExclusive-min)/delta);
		double[] range = new double[size];
		int i=0;
		for(double d = min; i<size; d+=delta){
			range[i++] = d;
		}
		return range;
	}

	public static double[] makeRangeIncl(double min, double maxInclusive, double delta){
		int size = (int) Math.floor((maxInclusive-min)/delta);
		double[] range = new double[size+1];
		int i=0;
		for(double d = min; i<size; d+=delta){
			range[i++] = d;
		}
		range[i++] = maxInclusive;
		return range;
	}


	public static int[] makeRange(int min, int maxExclusive, int delta){
		int size = (int) Math.floor((maxExclusive-min)/delta);
		int[] range = new int[size];
		int i=0;
		for(int d = min; i<size; d+=delta){
			range[i++] = d;
		}
		return range;
	}

	public static double min(Collection<Double> x)
	{
		if(x.isEmpty())
			return Double.NaN;

		List<Double> xNoNull = x.parallelStream().filter(Objects::nonNull).sorted().collect(Collectors.toList());
		return xNoNull.get(0);
	}

	public static int minInt(Collection<Integer> x)
	{
		if(x.size() == 0)
			return Integer.MIN_VALUE;
	    int min = x.iterator().next();
		for(int i: x)
			min = Math.min(min, i);
		return min;
	}

	public static int getQuantitle(int[] quantiles, int score){
		int closest = binary_search_return_closest(score, quantiles);
		return closest;
	}

	//from http://algs4.cs.princeton.edu/11model/BinarySearch.java.html
	//and adapted
	public static int binary_search_return_closest(int key, int[] a) {
		int lo = 0;
		int hi = a.length - 1;
		while (lo <= hi) {
			// Key is in a[lo..hi] or not present.
			int mid = lo + (hi - lo) / 2;
			if      (key < a[mid]) hi = mid - 1;
			else if (key > a[mid]) lo = mid + 1;
			else{ //equal
				lo = hi = mid;
				break;
			}
		}
		//adapted from binary search, was -1, now return 'closest', note: lo can be bigger then hi
		lo = clamp(lo, 0, a.length-1);
		hi = clamp(hi, 0, a.length-1);
		int ret = hi;
		if(Math.abs(key - a[lo]) < Math.abs(a[hi] - key)){
			ret = lo;
		}
		//special case, if duplicated in a, return one with lowest index
		while(ret> 0 && a[ret-1] == key){
			ret--;
		}
		return ret;
	}

	//e.g. resample([1,2,3,10], 2) will return [1+2/2=1, 3+10/2=6]
	public static int[] resample(int[] numbers, int denominator){
		int[] newNumbers = new int[numbers.length/denominator];
		for(int i=0; i<numbers.length && i/denominator < newNumbers.length; i+=denominator){
			int average = 0;
			for(int j=i; j<i+denominator && j<numbers.length; j++){
				average += numbers[j];
			}
			average = average / denominator;
			newNumbers[clamp(i/denominator, 0, newNumbers.length-1)] = average;
		}
		return newNumbers;
	}

	/**
	 * e.g. if [1,1,1,1,2,3,9,17, 22, 29]
	 * 		edf(3) == position 6 / 10  = 0.6       *1
	 * 		edf(4) == position 6-7     = 0.6-0.7, but since |4-3|==1 < |4-9|==5, position is 0.6 * (1-1/5) + 0.7 * 1/5 = 0.62
	 * 		edf(1) == position 1-4	   = (0.1+0.4)/2 = 0.25
	 *
	 *  *1: Use 1-based index
	 * @param x
	 * @param numbers
	 * @return
	 */
	public static double empericalDistributionFunction(int x, int[]numbers){
		int idx = binary_search_return_closest(x, numbers);
		if(idx == numbers.length-1){
			return 1.0;
		}
	    //case 1: exact match
		if(numbers[idx] == x){
			//case 1.1: exact match, no duplicate in next element
			if(numbers[idx+1] != x){
				return (idx+1)/(double)numbers.length;
			}
			//case 1.1: exact match,  duplicate in next element(s)
			else{
				int lastIdx = idx;
				for(int i=idx+1; i<numbers.length; i++){
					if(numbers[i] == x)
						lastIdx = i;
					else
						break;
				}
				return (idx+1)/(double)numbers.length * 0.5 + (lastIdx+1)/(double)numbers.length * 0.5;
			}
		}
		else{
			//case 2: no exact match
			int dist1 = Math.abs(x-numbers[idx]);
			int dist2 = Math.abs(x-numbers[idx+1]);
			if(dist1<dist2){
				double factor = dist1/(double)dist2;
				return (1 - factor) * (idx+1)/(double)numbers.length + factor * (idx+2)/(double)numbers.length;
			}
			else{
				double factor = dist2/(double)dist1;
				return factor * (idx+1)/(double)numbers.length + (1 - factor) * (idx+2)/(double)numbers.length;
			}

		}
	}

	public static int clamp(int x, int min, int max){
		if(x<min)
			return min;
		if(x>max)
			return max;
		else
			return x;
	}

	public static double min(int... x)
	{
		Arrays.sort(x);
		return x[0];
	}

	public static int sumInt(Collection<Integer> x){
		int sum = 0;
		for(int i: x)
			sum+=i;
		return sum;
	}

	public static double max(double... x)
	{
		Arrays.sort(x);
		return x[x.length - 1];
	}

	public static double max(Collection<Double> x)
	{
		Collection<Double> xNoNull = new ArrayList<>();
		for(Double d: x) {
			if(d!= null)
				xNoNull.add(d);
		}
		double max = xNoNull.iterator().next();
		for(double i: xNoNull)
			max = Math.max(max, i);
		return max;
	}

	public static int maxInt(Collection<Integer> ints) {
		int max = ints.iterator().next();
		for(int i: ints)
			max = Math.max(max, i);
		return max;
	}

	public static int maxInt(int ... ints) {
		int max = ints[0];
		for(int i: ints)
			max = Math.max(max, i);
		return max;
	}

	public static double mean(double... x)
	{
		double mean = 0.0;
		for(double i: x)
			mean += i;
		return mean/x.length;
	}

	public static double mean(List<Double> lst)
	{
		//if(lst.size() == 0){
		//	throw new RuntimeException("Mean: lst is empty");
		//}
		Double mean = 0.0;
		for(Double i: lst)
			if(i != null)
				mean += i;
		return mean/lst.size();
	}

	public static double meanInt(List<Integer> lst)
	{
		Double mean = 0.0;
		for(Integer i: lst)
			mean += i;
		return mean/lst.size();
	}

	public static double std(double... x)
	{
		double mean = mean(x);
		double std = 0.0;
		for(double i: x)
			std += (i - mean) * (i - mean);
		std = std / x.length;
		return Math.sqrt(std);
	}

	public static double std(List<Double> lst)
	{
		double mean = mean(lst);
		double std = 0.0;
		for(Double i: lst)
			if(i!= null)
				std += (i - mean) * (i - mean);
		std = std / lst.size();
		return Math.sqrt(std);
	}

	/**
	 * returns frequency of elements within -5,-4,-3,-2,-1,0,1,2,3,4,5 sigma as counts/total
	 * should look like  68-95-99.7% for a normal distribution, with very few elements as 3/-3 sigma,
	 * @param lst
	 * @return
	 */
	public static int[] backOfEnvelopTest(List<Double> lst)
	{
		double mean = mean(lst);
		double std = std(lst);
		int[] sigmaCounts = new int[12];
		double[] ranges = new double[]{-5, -4,      -3,      -2,    -1,      0,     +1,    +2,    +3,   +4,  +5,  Double.POSITIVE_INFINITY};
								    //[-inf,-4),[-4,-3),[-3,-2),[-2,-1),[-1,0),[0,1),[1,2),[2,3),  [3,4), [4,+inf)
		for(double val: lst){
			val = (val - mean) / std;
			for(int i=0; i<ranges.length; i++){
				if(val < ranges[i]){
					sigmaCounts[i]+=1;
					break;
				}
			}
		}
		return sigmaCounts;
	}

	public static int[] backOfEnvelopTestExpectationND(List<Double> lst)
	{
		NormalDistribution nd = new NormalDistribution(0,1);
		double[] ranges = new double[]{Double.NEGATIVE_INFINITY,-5,-4,-3,-2,-1,0,1,2,3,4,5,Double.POSITIVE_INFINITY};
		int[] expected = new int[12];
		for(int i=0; i<ranges.length-2;i++){
			double prob = nd.probability(ranges[i],ranges[i+1]);
			int count = (int)Math.round(prob * lst.size());
			expected[i] = count;
		}
		return expected;
	}


	public static double stdInt(List<Integer> lst)
	{
		double mean = meanInt(lst);
		double std = 0.0;
		for(Integer i: lst)
			std += (i - mean) * (i - mean);
		std = std / lst.size();
		return Math.sqrt(std);
	}

	public static double[] minMedianMax(List<Double> lst){
		Collections.sort(lst);
		return new double[]{lst.get(0), lst.get(lst.size()/2), lst.get(lst.size()-1)};
	}

	public static int[] minMedianMaxInt(List<Integer> lst){
		Collections.sort(new ArrayList<Integer>(lst));
		return new int[]{lst.get(0), lst.get(lst.size()/2), lst.get(lst.size()-1)};
	}

	public static double[] filterZero(double...x)
	{
		List<Double> doubles = new ArrayList<Double>();
		for(double i: x)
		{
			if(i != 0.0)
				doubles.add(i);
		}
		double[] results = new double[doubles.size()];
		int idx = 0;
		for(Double i: doubles)
		{
			results[idx++] = i;
		}
		return results;
	}

	public static boolean equals(Double x1, Double x2) {
		return Math.abs(x1 -x2) < 0.000001;
	}

}
