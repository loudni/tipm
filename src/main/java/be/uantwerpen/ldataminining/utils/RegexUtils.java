package be.uantwerpen.ldataminining.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

	private static final String[] REGEX_CHARS = new String[]{"*","+","?",".","|","(",")","[","]"};

	public static String regexExtractValue(String str, String regex){
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		if (m.find()){
			return m.group(1);
		}
		else{
			return null;
		}
	}

	//or Pattern SPECIAL_REGEX_CHARS = Pattern.compile("[{}()\\[\\].+*?^$\\\\|]"
	public static String escapeSpecialsForRegex(String pattern){
		for(String special: REGEX_CHARS){
			if(special.equals("(") || special.equals(")") || special.equals("."))
				pattern = pattern.replace(special, String.format("\\\\" + special));
			else
				pattern = pattern.replace(special, String.format("\\" + special));

		}
		return pattern;
	}

	/**
	 * Given pattern: ('B-', '[0-9]+', ' ', '[A-Z]+') and value 'B-2000 ANTWERPEN'
	 * returns ('B-', '2000', ' ', 'ANTWERPEN') or null if no match
	 * @param pattern
	 * @param value
	 * @return
	 */
	public static List<String> getTokens(List<String> pattern, String value){
		//assuming pattern of form (Contants, [chars], Contants, [chars]+), e.g.
		//('BE-', '[0-9]+'...)
		StringBuffer patternWithGroupsBuff = new StringBuffer();
		for(String patternToken: pattern){
			if(!(patternToken.startsWith("[") && (patternToken.endsWith("]") || patternToken.endsWith("]+") || patternToken.endsWith("]?")))){
			 //&& !patternToken.startsWith("(")  && (patternToken.endsWith(")") || patternToken.endsWith(")?") || patternToken.endsWith(")+"))){
				patternToken = escapeSpecialsForRegex(patternToken);
			}
			patternWithGroupsBuff.append("(" + patternToken + ")");
		}
		String patternWithGroups = patternWithGroupsBuff.toString();
		Matcher matcher = Pattern.compile(patternWithGroups).matcher(value);
		if(!matcher.matches()){
			return null;
		}
		//get valueTokens according to pattern:
		List<String> valueTokens = new ArrayList<String>();
		for(int i=1; i <= matcher.groupCount(); i++){
			valueTokens.add(matcher.group(i));
		}
		if(valueTokens.size() != pattern.size()){
			System.err.format(" Value/Pattern size mismatch: %s -> %s\n", CollectionUtils.join(pattern,""), CollectionUtils.join(valueTokens,"|"));
			return null;
		}
		return valueTokens;
	}

	private static String[] tokenTypes =
				new String[]{"[0-9]+\\.[0-9]+", "[0-9]+", "[A-Za-z]+", "[\\s]+", "[^A-Za-z0-9\\s]"};
	private static String[] tokenReplacements =
				new String[]{"f", 				"d", 		"w", 		" ", 	 "identic"};
	/**
	 * e.g. converts
	 * 		or "25343" to "d"
	 * 		or "ksjfkf" to "w"
	 * 		or "3232.333" to "f"
	 * 		or "afsf25343@this.its" to "wd@w.w"
	 * 		or "aba    1  24" to "w d d"
	 * @param s
	 * @return
	 */
	public static String toTokenString(String s){
		for(int idx=0; idx<tokenTypes.length; idx++){
			String tokenType = tokenTypes[idx];
			String replace = tokenReplacements[idx];
			s = s.replaceAll(tokenType, replace);
		}
		return s;
	}
}
