package be.uantwerpen.ldataminining.utils;

public class Triple<T1, T2, T3> extends ca.pfv.spmf.algorithms.sequentialpatterns.qcsp.util.Triple<T1,T2,T3> {

	public Triple(T1 first, T2 second, T3 third) {
		super(first, second, third);
	}

	public String toString() {
		return String.format("<%s,%s,%s>", getFirst(), getSecond(), getThirth());
	}

	public T3 getThird() {
		return super.getThirth();
	}
}
