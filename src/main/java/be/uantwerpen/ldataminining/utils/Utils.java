package be.uantwerpen.ldataminining.utils;

import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;


/**
 * Generic Utils for running code
 * @author lfereman
 *
 */
public class Utils {


	/**
	 * Escape non-ascii + special chars except - and _
	 * Replaces whitespace by _
	 * @param name
	 * @return
	 */
	public static String escapeName(String name){
		if(name.matches("'.*'") || name.matches("\".*\""))
			name = name.substring(1, name.length()-1);
		String escaped = replaceSpecialCharactersByDash(escapeNonAscii(name));
		escaped = escaped.replaceAll("\\s+", "_");
		return escaped.trim();
	}

	/**
	 * Keep A-Z,a-z,0-9 and _- and space, remove the rest
	 * @param s
	 * @return
	 */
	public static String replaceSpecialCharactersByDash(String s){
		String alphaAndDigits = s.replaceAll("[^A-Za-z0-9\\s]+","_");
		alphaAndDigits = alphaAndDigits.replace('/', '_');
		alphaAndDigits = alphaAndDigits.replace('\\', '_');
		alphaAndDigits = alphaAndDigits.replaceAll("[_]+","_");
		if(alphaAndDigits.startsWith("_"))
			alphaAndDigits = alphaAndDigits.substring(1);
		if(alphaAndDigits.endsWith("_"))
			alphaAndDigits = alphaAndDigits.substring(0,alphaAndDigits.length() -1);
		return alphaAndDigits.trim();
	}

	/**
	 * see http://stackoverflow.com/questions/8519669/replace-non-ascii-character-from-string
	 * Does:
	 * 		Replace "öäü with oau
	 * 		Remove non-ascii characters
	 * @param name
	 * @return
	 */
	public static String escapeNonAscii(String name){
		String escapedName = Normalizer.normalize(name, Normalizer.Form.NFD);
		escapedName= escapedName.replaceAll("[^\\x00-\\x7F]", " ");
		return escapedName.trim();
	}

	public static boolean isDate(String str, SimpleDateFormat formatter){
		try{
			formatter.parse(str);
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public static String milisToStringReadable(long milis){
		if(milis < 1000){
			return String.format(Locale.ENGLISH, "%d ms", milis);
		}
		else if(milis >= 1000 && milis < 60000){
			return String.format(Locale.ENGLISH, "%.1f sec", milis/1000.0);
		}
		else if(milis >= 60 * 1000 && milis < 60 * 60 * 1000){
			return String.format(Locale.ENGLISH, "%.1f min", milis/(60 * 1000.0));
		}
		return String.format(Locale.ENGLISH, "%.2f h", milis/(3600 * 1000.0));
	}

	/**
	 * e.g.
	 *  new SimpleDateFormat("yyyyddmm")
		new SimpleDateFormat("ddmmyyyy")
		new SimpleDateFormat("mmddyyyy")
		new SimpleDateFormat("yyyymmdd")
	 * @param str
	 * @param format
	 * @return
	 */
	public static boolean isDateNotInteger(String str, String format){
		try{
			if(str.length() != format.length())
				return false;
			if(!isInteger(str))
				return false;
			int idxYear = format.indexOf("yyyy");
			int year = Integer.parseInt(str.substring(idxYear, idxYear + 4));
			if(!(year > 1800 && year < 3000)){
				return false;
			}
			int idxMonth = format.indexOf("mm");
			int month = Integer.parseInt(str.substring(idxMonth, idxMonth + 2));
			if(!(month > 0 && month < 13)){
				return false;
			}
			int idxDay = format.indexOf("dd");
			int day = Integer.parseInt(str.substring(idxDay, idxDay + 2));
			if(!(day > 0 && month < 32)){
				return false;
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public static boolean isInteger(String str){
		try{
			Long.parseLong(str);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}


	private static NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMANY);
	public static boolean isFloat(String str){
		try{
			if(!str.trim().matches("[\\-]?[0-9]*[\\.\\,]*[0-9]*[\\\\.\\\\,]*[E]?[0-9]*")){
				return false;
			}
			Double.parseDouble(str);
			return true;
		}
		catch(NumberFormatException e){
			try {
				numberFormat.parse(str).doubleValue();
				return true;
			}
			catch(Exception e2) {
				return false;
			}
		}
	}

	public static Double asDouble(String str){
		try{
			return Double.parseDouble(str);
		}
		catch(NumberFormatException e){
			try {
				return numberFormat.parse(str).doubleValue();
			}
			catch(Exception e2) {
			}
		}
		return null;
	}

	//see http://viralpatel.net/blogs/getting-jvm-heap-size-used-memory-total-memory-using-java-runtime/
	public static void printMemory(){
		int mb = 1024*1024;
		//Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();
		System.out.println("##### Heap utilization statistics [MB] #####");
		//Print used memory
		System.out.println("Used Memory:"  + (runtime.totalMemory() - runtime.freeMemory()) / mb);
		//Print free memory
		System.out.println("Free Memory:" + runtime.freeMemory() / mb);
		//Print total available memory
		System.out.println("Total Memory:" + runtime.totalMemory() / mb);
		//Print Maximum available memory
		System.out.println("Max Memory:" + runtime.maxMemory() / mb);
	}

	public static String substringBefore(String str, String suffix)
	{
		int idx = str.indexOf(suffix);
		if(idx != -1)
			return str.substring(0, idx);
		else
			return "";
	}

	public static String substringAfter(String str, String prefix)
	{
		int idx = str.indexOf(prefix);
		if(idx != -1)
			return str.substring(idx + prefix.length());
		else
			return "";
	}

	public static String substringBetween(String str, String prefix, String suffix)
	{
		return Utils.substringBefore(Utils.substringAfter(str, prefix), suffix);
	}

	public static String unQuote(String s){
		if(s.length() > 1 &&
			(
				s.startsWith("'") && s.endsWith("'") ||
				s.startsWith("\"") && s.endsWith("\"")
			)
		) {
				s = s.substring(1, s.length()-1);
		}

		return s;
	}
}
