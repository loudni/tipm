package be.uantwerpen.ldataminining.preprocessing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import be.uantwerpen.ldataminining.model.NaturalOrderComparator;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils.StreamerArffRegular;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Utils;
import weka.core.Attribute;
import weka.core.Instances;

public final class ArffDenseUtils {
	private ArffDenseUtils(){}

	public static interface RowFilter{
		public boolean keepRow(String[] attributeNames, String[] values);
	}

	public static interface ColumnMapper{
		public String map(String attributeName, String value);
	}

	public static interface RowMapper{
		public List<String> map(List<String> row);
	}


	public interface GroupByMapper{
		public List<List<String>> multiple(String key, List<List<String>> rows);
		public List<String> singleOrKeyNull(String key, List<String> rows);
	}

	/**
	 * Why? transform something like '\'(-inf-5.55]\'', to '(-inf-5.55]', because it caused some bugs
	 * @param val
	 */
	private static String prePreprocessArffValue(String val)
	{
		if(val.startsWith("'\\'") && val.endsWith("\\''"))
			val = "'" + val.substring(3,val.length()-3) + "'";
		return val;
	}

	@Deprecated
	public static int inferClassAttribute(Instances data) {
		// setting class attribute if the data format does not provide this information
		// For example, the XRFF format saves the class attribute information as well
		if (data.classIndex() == -1)
		{
			for(int i=0; i < data.numAttributes(); i++)
			{
				if(data.attribute(i).name().toLowerCase().equals("class"))
				{
					return i;
				}
			}
			return -1;
		}
		return data.classIndex();
	}



	@Deprecated
	public static boolean isBinaryAttribute(Attribute attr) {
		if(!attr.isNominal())
			return false;
		boolean binary = true;
		Enumeration enumeration = attr.enumerateValues();
		while(enumeration.hasMoreElements()){
			String val = enumeration.nextElement().toString();
			if(!CollectionUtils.contains(new String[]{"0","1"}, val)){
				binary = false;
				break;
			}
		}
		return binary;
	}

	@Deprecated
	public static List<String> getNominalValues(Attribute attr){
		List<String> values = new ArrayList<String>();
		Enumeration enumeration = attr.enumerateValues();
		if(enumeration == null)
			return Collections.emptyList();
		while(enumeration.hasMoreElements())
		{
			String val = prePreprocessArffValue(enumeration.nextElement().toString());
			values.add(val);
		}
		return values;
	}

	@Deprecated
	public static List<Attribute> getAttributes(Instances data)
	{
		List<Attribute> attributes = new ArrayList<Attribute>();
		for(int i=0; i<data.numAttributes(); i++)
		{
			Attribute attr = data.attribute(i);
			attributes.add(attr);
		}
		return attributes;
	}

	@Deprecated
	public static String[] getRow(Instances data, int i)
	{
		List<String> valsList = CSVUtils.parseLine(data.instance(i).toString(), ',', '\"');
		String[] vals = valsList.toArray(new String[valsList.size()]);
		for(int idx=0; idx<vals.length; idx++)
			vals[idx] = prePreprocessArffValue(vals[idx]);
		return vals;
	}


	public static Table loadArff(File file, int from, int to) throws IOException{
		Table table = new Table();
		List<List<String>> rows = ArffUtils.loadDataMatrix(file, from, to);
		List<String> names = ArffUtils.getAttributeNames(file);
		rows.add(0, names);
		table.setRows(rows);
		table.setTotalSize(ArffUtils.getNumberOfRows(file));
		return table;
	}

	public static Table loadArff(File file) throws IOException{
		Table table = new Table();
		List<List<String>> rows = ArffUtils.loadDataMatrix(file);
		List<String> names = ArffUtils.getAttributeNames(file);
		rows.add(0, names);
		table.setRows(rows);
		table.setTotalSize(rows.size());
		return table;
	}



	public static Map<String, List<String>> makeReverseIndex(File input, String uniqueNotNullColumn) throws IOException{
		List<List<String>> matrix = ArffUtils.loadDataMatrix(input);
		int colIdx = ArffUtils.getAttributeNames(input).indexOf(uniqueNotNullColumn);
		Map<String, List<String>> index = new TreeMap<String, List<String>>();
		for(List<String> row: matrix){
			String column = row.get(colIdx);
			index.put(column, row);
		}
		return index;
	}

	public static Multimap<String, List<String>> makeReverseMultiIndex(File input, String notUniqueColumn) throws IOException{
		List<List<String>> matrix = ArffUtils.loadDataMatrix(input);
		int colIdx = ArffUtils.getAttributeNames(input).indexOf(notUniqueColumn);
		Multimap<String, List<String>> index = ArrayListMultimap.create();
		for(List<String> row: matrix){
			String column = row.get(colIdx);
			index.put(column, row);
		}
		return index;
	}

	public static Map<String, String> makeMapTwoColumns(File input, String uniqueNotNullColumn, String valueColumn) throws IOException{
		List<List<String>> matrix = ArffUtils.loadDataMatrix(input);
		int colIdx = ArffUtils.getAttributeNames(input).indexOf(uniqueNotNullColumn);
		int colIdx2 = ArffUtils.getAttributeNames(input).indexOf(valueColumn);
		Map<String, String> index = new TreeMap<String, String>();
		for(List<String> row: matrix){
			String column = row.get(colIdx);
			String column2 = row.get(colIdx2);
			index.put(column, column2);
		}
		return index;
	}

	public static boolean transformAnyDateColumns(File file, File output, String dataformat_from, String dateformat_to) throws IOException{
		//try all columns to see if matches dateformat_from, based on sample of 20
		List<List<String>> dataSample = ArffUtils.loadDataMatrixSample(file, 20);
		List<String> attributes = ArffUtils.getAttributeNames(file);
		List<String> attributesToConvert = new ArrayList<>();
		final SimpleDateFormat format_from = new SimpleDateFormat(dataformat_from);
		final SimpleDateFormat format_to = new SimpleDateFormat(dateformat_to);
		for(int attributeIdx=0; attributeIdx < attributes.size(); attributeIdx++){
			Boolean matchesOldFormat = null;
			for(List<String> row: dataSample){
				String value = row.get(attributeIdx);
				if(value.equals("?"))
					continue;
				if(Utils.isDate(value, format_from)){
					if(matchesOldFormat == null)
						matchesOldFormat = true;
					else
						matchesOldFormat = matchesOldFormat && true;
				}
				else{
					matchesOldFormat = false;
				}
			}
			if(matchesOldFormat != null && matchesOldFormat)
				attributesToConvert.add(attributes.get(attributeIdx));
		}
		System.out.println("Converting attributes: " + CollectionUtils.join(attributesToConvert));
		File input = file;
		for(String attributeToConvert: attributesToConvert){
			mapRowsDense(input, output, attributeToConvert, new ColumnMapper() {

				boolean first = true;
				@Override
				public String map(String attributeName, String value) {
					try {
						if(value.equals("?"))
							return value;
						java.util.Date date = format_from.parse(value);
						String dateToStr = format_to.format(date);
						if(first){
							System.out.format("DEBUG: Converting %s, from %s to %s\n", attributeName, value, dateToStr);
							first=false;
						}
						return dateToStr;
					} catch (ParseException e) {
						throw(new RuntimeException(e));
					}
				}
			});
			input = output;
		}
		//rest: change schema
		List<String> attributeTypes = ArffUtils.getAttributeValues(file);
		for(String attributeToConvert: attributesToConvert){
			attributeTypes.set(attributes.indexOf(attributeToConvert), "DATE \"" + dateformat_to + "\"");
		}
		List<String> header = ArffUtils.makeHeader("dates-fixed", attributes, attributeTypes, false);
		File tempFileWithCorrectHeader = File.createTempFile("bla", "foo");
		IOUtils.saveFile(header, tempFileWithCorrectHeader);
		final BufferedWriter writer = new BufferedWriter(new FileWriter(tempFileWithCorrectHeader, true));
		ArffUtils.streamFile(input, new StreamerArffRegular() {

			@Override
			public void doSomething(List<String> row) {
				try {
					writer.write(CSVUtils.serializeLineArff(row));
					writer.newLine();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
		writer.close();
		tempFileWithCorrectHeader.renameTo(output);
		return attributesToConvert.size() > 0;
	}



	public static void printTopKRows(File arffFile, int k) throws IOException
	{
		List<String> lines = IOUtils.readFile(arffFile);
		int dataIdx = IOUtils.indexOf(lines, "@data");
		List<String> names = ArffUtils.getAttributeNames(arffFile);
		for(int idx=dataIdx+1; idx<dataIdx+1+k; idx++){
			String line = lines.get(idx);
			System.out.println(arffFile.getName() + ":" + line);
		}
	}


	public static void addIndexDense(File file, File outputFile) throws IOException{
		List<String> lines = IOUtils.readFile(file);
		List<String> output = new ArrayList<>();
		int indexData = IOUtils.indexOf(lines, "@data");
		//copy header + add index
		boolean firstAttribute = false;
		for(int idx=0; idx<indexData+1; idx++){
			String line = lines.get(idx);
			if(!firstAttribute && line.startsWith("@attribute")){
				firstAttribute = true;
				output.add("@attribute Index INTEGER");
				output.add(line);
			}
			else{
				output.add(line);
			}
		}
		//save filtered rows
		int rowIdx = indexData + 1;
		int idx=0;
		for(; rowIdx < lines.size(); rowIdx++){
			String row = lines.get(rowIdx);
			List<String> values = CSVUtils.parseLine(row, ',', '\"');
			values.add(0, "" + idx++);
			row = CSVUtils.serializeLineArff(values);
			output.add(row);
		}
		IOUtils.saveFile(output, outputFile);
	}


	public static void filterRowsDense(File file, File outputFile, RowFilter filter) throws IOException{
		List<String> lines = IOUtils.readFile(file);
		List<String> output = new ArrayList<>();
		int indexData = IOUtils.indexOf(lines, "@data");
		//copy header
		output.addAll(lines.subList(0, indexData+1));
		//save filtered rows
		List<String> attributeNames = ArffUtils.getAttributeNames(file);
		String[] attributeNamesArr = attributeNames.toArray(new String[attributeNames.size()]);
		int rowIdx = indexData + 1;
		for(; rowIdx < lines.size(); rowIdx++){
			String row = lines.get(rowIdx);
			List<String> values = CSVUtils.parseLine(row, ',', '\"');
			if(filter.keepRow(attributeNamesArr, values.toArray(new String[values.size()]))){
				output.add(row);
			}
		}
		IOUtils.saveFile(output, outputFile);
	}

	public static void convertArffToCSVDense(File arffFile, File csvOutput, boolean keepHeader) throws IOException
	{
		List<String> output = new ArrayList<>();
		List<String> lines = IOUtils.readFile(arffFile);
		int dataIdx = IOUtils.indexOf(lines, "@data") + 1;
		if(keepHeader){
			List<String> attributeNames = ArffUtils.getAttributeNames(arffFile);
			output.add(CollectionUtils.join(attributeNames, ","));
		}
		for(int i=dataIdx; i<lines.size(); i++)
		{
			String line = lines.get(i);
			if(line.isEmpty())
				continue;
			List<String> tokens = CSVUtils.parseLine(line, ',', '"');
			line = CSVUtils.serializeLineCSV(tokens);
			output.add(line);
		}
		IOUtils.saveFile(output, csvOutput);
	}


	public static void filterEmptyRowsDense(File input, File outputFile) throws IOException {
		List<String> lines = IOUtils.readFile(input);
		List<String> output = new ArrayList<String>();
		int indexData = IOUtils.indexOf(lines, "@data");
		//copy header
		output.addAll(lines.subList(0, indexData+1));
		//save filtered rows
		int rowIdx = indexData + 1;
		int countAllEmpty = 0;
		for(; rowIdx < lines.size(); rowIdx++){
			String row = lines.get(rowIdx);
			List<String> values = CSVUtils.parseLine(row, ',', '\"');
			boolean allEmpty = true;
			for(String value: values){
				if(!value.equals("?")){
					allEmpty = false;
					break;
				}
			}
			if(!allEmpty){
				row = CSVUtils.serializeLineArff(values);
				output.add(row);
			}
			else{
				countAllEmpty++;
			}
		}
		System.out.format("Removed %d empty rows\n", countAllEmpty);
		IOUtils.saveFile(output, outputFile);
	}

	public static void mapRowsDense(File file, File outputFile, final String column, final ColumnMapper mapper) throws IOException{
		File tempFile = outputFile;
		if(outputFile.getPath().equals(file.getPath())){ //input maybe same file as output
			tempFile = File.createTempFile(file.getName(), "temp-map");
		}
		Timer timer = new Timer("mapRows");
		List<String> attributeNames = ArffUtils.getAttributeNames(file);
		final int colIdx = attributeNames.indexOf(column);
		System.out.format("Mapping rows in file %s on column %s, with index %d\n", file, column, colIdx);
		ArffUtils.copyHeader(file, tempFile);
		final BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile, true));
		ArffUtils.streamFile(file, new ArffUtils.StreamerArffRegular() {

			@Override
			public void doSomething(List<String> values) {
				String colValue = values.get(colIdx);
				colValue = mapper.map(column, colValue);
				if(colValue.isEmpty())
					colValue = "?";
				values.set(colIdx,colValue);
				String row = CSVUtils.serializeLineArff(values);
				try {
					writer.write(row);
					writer.newLine();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
		writer.close();
		if(!tempFile.getPath().equals(outputFile.getPath())){
			tempFile.renameTo(outputFile);
		}
		timer.end();
	}

	public static void mapRowsDense(File file, File outputFile, final RowMapper mapper) throws IOException{
		File tempFile = outputFile;
		if(outputFile.getPath().equals(file.getPath())){ //input maybe same file as output
			tempFile = File.createTempFile(file.getName(), "temp-map");
		}
		Timer timer = new Timer("mapRows");
		ArffUtils.copyHeader(file, tempFile);
		final BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile, true));
		ArffUtils.streamFile(file, new ArffUtils.StreamerArffRegular() {

			@Override
			public void doSomething(List<String> values) {
				values = mapper.map(values);
				if(values == null)
					return; //Filter if mapper returns null!
				String row = CSVUtils.serializeLineArff(values);
				try {
					writer.write(row);
					writer.newLine();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
		writer.close();
		if(!tempFile.getPath().equals(outputFile.getPath())){
			tempFile.renameTo(outputFile);
		}

		//if the input and output are the same we need to copy tempFile
		if(outputFile.getPath().equals(file.getPath())){
			FileUtils.copyFile(tempFile, outputFile);
		}
		timer.end();
	}

	public static void distinctDense(File input, File outputFile, String[] columns) throws IOException {
		List<String> columnsAll = ArffUtils.getAttributeNames(input);
		List<Integer> columnsIndexes = new ArrayList<Integer>();
		for(String s: columns){
			columnsIndexes.add(columnsAll.indexOf(s));
		}
		Timer timer = new Timer("Distinct");
		List<String> lines = IOUtils.readFile(input);
		List<String> output = new ArrayList<String>();
		int indexData = IOUtils.indexOf(lines, "@data");
		//copy header
		output.addAll(lines.subList(0, indexData+1));
		//save filtered rows
		int duplicates = 0;
		int rowIdx = indexData + 1;
		Set<String> rows = new HashSet<String>();
		for(; rowIdx < lines.size(); rowIdx++){
			String rowStr= lines.get(rowIdx);
			List<String> row = CSVUtils.parseLineAndUnquote(rowStr, ',', '"');
			List<String> subRow = new ArrayList<String>();
			for(Integer idx: columnsIndexes){
				subRow.add(row.get(idx));
			}
			String subRowStr = CSVUtils.serializeLineArff(subRow);
			if(!rows.contains(subRowStr) || subRowStr.equals("?")){
				output.add(rowStr);
			}
			else{
				duplicates++;
			}
			rows.add(subRowStr);
		}
		System.out.format("Removed non-distinct %d rows using attributes %s\n", duplicates, CollectionUtils.join(columns));
		IOUtils.saveFile(output, outputFile);
		timer.end();
	}

	public static int groupByHavingCountMoreThenOneDense(File input, File outputFile, String... columns) throws IOException {
		List<String> names = ArffUtils.getAttributeNames(input);
		int[] columnsIndexes = new int[columns.length];
		for(int i=0; i<columns.length; i++){
			columnsIndexes[i] = names.indexOf(columns[i]);
		}
		List<String> lines = IOUtils.readFile(input);
		List<String> output = new ArrayList<String>();
		int indexData = IOUtils.indexOf(lines, "@data");
		//copy header
		output.addAll(lines.subList(0, indexData+1));
		//save filtered rows
		int rowIdx = indexData + 1;
		Multimap<String,String> hashMap = ArrayListMultimap.create();
		for(; rowIdx < lines.size(); rowIdx++){
			String row = lines.get(rowIdx);
			List<String> values = CSVUtils.parseLine(row, ',', '\"');
			String key = "";
			for(int colIdx: columnsIndexes){
				String col = values.get(colIdx);
				key += col + "|";
			}
			if(key.matches("[\\?\\|]+")){
				continue;
			}
			hashMap.put(key, row);
		}
		int count = 0;
		for(String key: hashMap.keySet()){
			Collection<String> rows = hashMap.get(key);
			if(rows.size() > 1){
				count++;
				List<String> sorted = new ArrayList<String>(rows);
				Collections.sort(sorted, new NaturalOrderComparator());
				output.addAll(sorted);
			}
		}
		System.out.format("Found %d rows\n", count);
		IOUtils.saveFile(output, outputFile);
		return count;
	}

	public static int mapGroupByDense(File input, File outputFile, GroupByMapper mapper, String... columns) throws IOException {
		List<String> names = ArffUtils.getAttributeNames(input);
		int[] columnsIndexes = new int[columns.length];
		for(int i=0; i<columns.length; i++){
			columnsIndexes[i] = names.indexOf(columns[i]);
		}
		List<String> lines = IOUtils.readFile(input);
		List<String> output = new ArrayList<String>();
		int indexData = IOUtils.indexOf(lines, "@data");
		//copy header
		output.addAll(lines.subList(0, indexData+1));
		//save filtered rows
		int rowIdx = indexData + 1;
		Multimap<String,String> hashMap = ArrayListMultimap.create();
		for(; rowIdx < lines.size(); rowIdx++){
			String row = lines.get(rowIdx);
			List<String> values = CSVUtils.parseLine(row, ',', '\"');
			String key = "";
			for(int colIdx: columnsIndexes){
				String col = values.get(colIdx);
				key += col + "|";
			}
			if(key.matches("[\\?\\|]+")){
				List<String> outputMapper = mapper.singleOrKeyNull(null, values);
				if(outputMapper != null)
					output.add(CSVUtils.serializeLineArff(outputMapper));
				continue;
			}
			hashMap.put(key, row);
		}
		int count = 0;
		for(String key: hashMap.keySet()){
			List<String> rows = (List)hashMap.get(key);
			if(rows.size() > 1){
				count++;
				List<String> sorted = new ArrayList<String>(rows);
				Collections.sort(sorted, new NaturalOrderComparator());
				List<List<String>> sortedValues = new ArrayList<List<String>>();
				for(String row: sorted){
					sortedValues.add( CSVUtils.parseLine(row, ',', '\"'));
				}
				List<List<String>> outputMapper = mapper.multiple(key, sortedValues);
				if(outputMapper != null){
					for(List<String> outputMapperLine: outputMapper){
						output.add(CSVUtils.serializeLineArff(outputMapperLine));
					}
				}
			}
			else{
				List<String> rowValues = CSVUtils.parseLine(rows.get(0), ',', '\"');
				List<String> outputMapper = mapper.singleOrKeyNull(key, rowValues);
				if(outputMapper != null)
					output.add(CSVUtils.serializeLineArff(outputMapper));
			}
		}
		System.out.format("Found (and mapped) %d multiple rows grouped by columns %s\n", count, CollectionUtils.join(columns));
		IOUtils.saveFile(output, outputFile);
		return count;
	}

}
