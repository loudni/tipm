package be.uantwerpen.ldataminining.preprocessing;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Utils;

public class ApplyRules {

	public Multimap<String, String> loadRules(File resultActionsFile) throws IOException{
		List<String> lines = IOUtils.readFile(resultActionsFile);
		Multimap<String, String> output = ArrayListMultimap.create();
		outer:
		for(int idx=0; idx<lines.size();idx++){
			String line = lines.get(idx);
			if(line.startsWith("Actions")){
				String attribute = Utils.substringAfter(line, "Actions ").trim();
				for(int idx2=idx+1; idx2<lines.size();idx2++){
					String line2 = lines.get(idx2);
					if(line2.startsWith("Actions ")){
						idx = idx2 -1;
						continue outer;
					}
					else{
						output.put(attribute, line2.trim());
					}
				}
			}
		}
		return output;
	}

	public void applyRules(File resultActionsFile, File input, File output) throws IOException{
		Multimap<String, String> attributeActions = loadRules(resultActionsFile);
		if(!input.getAbsolutePath().equals(output.getAbsolutePath()))
			FileUtils.copyFile(input, output);
		for(String attribute: attributeActions.keySet()){
			//show actions
			System.out.format("Actions %s\n", attribute);
			//e.g. Actions Company_Nb
			//removePrefix(0)
			//removeCharacter(.)
			for(String action: attributeActions.get(attribute)){
				System.out.format("  %s\n", action);
				if(action.startsWith("removePrefix")){
					final String prefix = Utils.substringBetween(action, "(", ")");
					ArffDenseUtils.mapRowsDense(output, output, attribute, new ArffDenseUtils.ColumnMapper() {

						public String map(String attributeName, String value) {
							if(value.startsWith(prefix)){
								return Utils.substringAfter(value, prefix);
							}
							else
								return value;
						}
					});
				}
				else if(action.startsWith("setNull") || action.startsWith("removeIllegalValue")){
					final String by = Utils.substringBetween(action, "(", ")");
					ArffDenseUtils.mapRowsDense(output, output, attribute, new ArffDenseUtils.ColumnMapper() {

						public String map(String attributeName, String value) {
							if(value.equals(by)){
								return "";
							}
							else
								return value;
						}
					});
				}
				else if(action.startsWith("removeCharacter")){
					final CharSequence characters = Utils.substringBetween(action, "(", ")");
					ArffDenseUtils.mapRowsDense(output, output, attribute, new ArffDenseUtils.ColumnMapper() {

						public String map(String attributeName, String value) {
							value = value.replace(characters,"");
							return value;
						}
					});
				}
				else if(action.startsWith("replace")){
					CharSequence replace = Utils.substringBetween(action, "(", ",");
					if(replace.toString().contains("COMMA")){
						replace = replace.toString().replace("COMMA", ",");
					}
					final CharSequence replace2 = replace;
					final CharSequence by = Utils.substringBetween(action, ",",")");
					ArffDenseUtils.mapRowsDense(output, output, attribute, new ArffDenseUtils.ColumnMapper() {

						public String map(String attributeName, String value) {
							value = value.replace(replace2,by);
							return value;
						}
					});
				}
			}
		}
	}
}
