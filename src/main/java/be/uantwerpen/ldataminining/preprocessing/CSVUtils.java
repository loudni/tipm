package be.uantwerpen.ldataminining.preprocessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Set;

import be.uantwerpen.ldataminining.model.Dataset;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.model.Dataset.ClassAttributeOrder;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Utils;
import be.uantwerpen.ldataminining.utils.IOUtils.Encoding;

//import com.opencsv.CSVReader;

/**
 * Loads CSV file, conver to  ARFF ...
 */
public class CSVUtils {

	public interface RowMapper{
		//return null, does not save
		public List<String> doRow(List<String> row);
	}

	public interface RowMapper2{
		//return null, does not save
		public List<String> doRow(List<String> row, List<String> nextRow);
	}

	public static List<String> getColumns(File input) throws IOException{
		List<String> lines = IOUtils.readFile(input, Encoding.UTF_8, 1);
		return parseLine(lines.get(0), ',', '"');
	}

	public static void replaceHeader(File input, List<String> newColumns) throws IOException{
		List<String> lines = IOUtils.readFile(input);
		lines.set(0, CSVUtils.serializeLineCSV(newColumns));
		IOUtils.saveFile(lines, input);
	}

	public static void mapStreaming(File csvFile, File csvFileOutput, char seperator, char quoteChar, RowMapper mapper) throws IOException{
		mapStreaming(csvFile, csvFileOutput, seperator, quoteChar, mapper, true, false);
	}

	public static void mapStreaming(File csvFile, File csvFileOutput, char seperator, char quoteChar, RowMapper mapper, boolean checkSize, boolean hasHeaderRow) throws IOException{
		Long noRecords = IOUtils.countLines(csvFile);
		Timer timer = new Timer("mapStreaming " + csvFile.getName() + " (#" + noRecords + ")" );
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));
		BufferedWriter out = null;
		if(csvFileOutput != null){
			csvFileOutput.getParentFile().mkdirs();
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFileOutput), "UTF-8"));
		}
		String line = null;
		List<String> firstLine = null;
		if(hasHeaderRow){
			line = in.readLine();
			List<String> columns = parseLine(line, seperator, quoteChar);
			firstLine= columns;
			if(out !=null){
				out.write(serializeLineCSV(columns, seperator));
				out.newLine();
			}
		}
		line = in.readLine();
		if(!hasHeaderRow){
			firstLine = parseLine(line, seperator, quoteChar);
		}
		List<String> tokens = null;
		int count = 0;
		while (line  != null) {
 			tokens = parseLine(line, seperator,  quoteChar);
			if(tokens.size() == 0){
				line = in.readLine();
				count++;
				continue;
			}
			else if(checkSize && firstLine.size() != tokens.size()){
				System.err.format("ERROR PARSING: line %s: No attributes is %d, previous was %s.\n", line, firstLine.size(), tokens.size());
				line = in.readLine();
				count++;
				continue;
			}
			else{
				for(int i=0; i<tokens.size(); i++){
					String s = tokens.get(i);
					if(isEmptyValueInCSV(s))
						tokens.set(i, "?");
				}
				List<String> newRow = mapper.doRow(tokens);
				if(newRow != null && out !=null){
					out.write(serializeLineCSV(newRow, seperator));
					out.newLine();
				}
				line = in.readLine();
			}
			count++;
			if(noRecords > 10 && count % (noRecords/10) == 0 && count > 100.000){
				timer.progress(count, noRecords);
			}
		}
		in.close();
		if(out != null)
			out.close();
		timer.end();
	}

	public static void mapStreaming2(File csvFile, File csvFileOutput, char seperator, char quoteChar, RowMapper2 mapper) throws IOException{
		Timer timer = new Timer("mapStreaming");
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));
		csvFileOutput.getParentFile().mkdirs();
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFileOutput), "UTF-8"));
		String line = in.readLine();
		String line2 = in.readLine();
		List<String> tokens = null;
		List<String> tokens2 = null;
		while (line  != null) {
 			tokens = parseLine(line, seperator,  quoteChar);
 			if(line2 != null)
 				tokens2 = parseLine(line2, seperator,  quoteChar);
 			else
 				tokens2 = Collections.EMPTY_LIST;
			if(tokens.size() == 0){
				line = line2;
				line2 = in.readLine();
				continue;
			}
			else{
				List<String> newRow = mapper.doRow(tokens, tokens2);
				if(newRow != null){
					out.write(serializeLineCSV(newRow, seperator));
					out.newLine();
				}
				line = line2;
				line2 = in.readLine();
			}
		}
		in.close();
		out.close();
		timer.end();
	}



	public static Table loadCSV(File csvFile, char seperator, char quoteChar) throws IOException
	{
		return loadCSV(csvFile, seperator, quoteChar, -1, -1);
	}

	public static Table loadCSV(File csvFile) throws IOException
	{
		return loadCSV(csvFile, -1, -1);
	}

	public static Table loadCSV(File csvFile, int from, int to) throws IOException
	{
		//determine seperator and quotechare automatically
		char[] seperators = detectSeperator(csvFile);
		return loadCSV(csvFile, seperators[0], seperators[1], from, to);
	}

	public static long getNoColumns(File csvFile) throws IOException{
		char[] seperators = detectSeperator(csvFile);
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));
		String line1 = in.readLine();
		in.close();
		return parseLine(line1, seperators[0], seperators[1]).size();
	}

	public static char[] detectSeperator(File csvFile) throws IOException{
		if(IOUtils.countLines(csvFile) < 5) {
			return new char[] {',','\''};
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));
		String line1 = in.readLine();
		String line2 = in.readLine();
		String line3 = in.readLine();
		in.close();
		for(Character seperator: new Character[]{',',';','|','\t'}){
			for(Character quoteChar: new Character[]{'"','\''}){
				List<String> tokens1 = parseLine(line1, seperator, quoteChar);
				List<String> tokens2 = parseLine(line2, seperator, quoteChar);
				List<String> tokens3 = parseLine(line3, seperator, quoteChar);
				if(tokens1.size()>1&&tokens1.size() == tokens2.size() && tokens1.size() == tokens3.size()){
					return new char[]{seperator, quoteChar};
				}
			}
		}
		throw new RuntimeException("Unable to guess seperator/quote char CSV File");
	}

	public static Table loadCSV(File csvFile, char seperator, char quoteChar, int from, int to) throws IOException
	{
		Timer timer = new Timer("loadCSV");
		Table table = new Table();
		//List<String> lines = IOUtils.readFile(csvFile);
		//String seperatorExpression = String.format("\\s*%s\\s*", seperator);
		int noAttributes = 0;
		int idx = 0;
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));
		//NOTE: CSVReader crashed somethimes...
		//CSVReader reader = new CSVReader(in, seperator.charAt(0));
		//String [] nextLine = reader.readNext();
		String line = in.readLine();
		List<String> tokens = null;
		int errorsSize = 0;
		int empty = 0;
		while (line  != null) {
			if(from != -1){
				while(idx < from && line != null){
					line = in.readLine();
					idx++;
				}
			}
			if(to != -1){
				if(idx > to){
					break;
				}
			}
 			tokens = parseLine(line, seperator,  quoteChar);
			if(tokens.size() == 0){
				empty++;
				idx++;
				line = in.readLine();
				continue;
			}
			if(noAttributes == 0)
				noAttributes = tokens.size();
			if(noAttributes != tokens.size())
			{
				System.err.format("ERROR PARSING: line %d: No attributes is %d, previous was %s.\n  Line: %s\n", idx,  tokens.size(), noAttributes,line);
				errorsSize++;
				idx++;
				line = in.readLine();
				continue;
			}
			for(int i=0; i<tokens.size(); i++){
				String s = tokens.get(i);
				if(isEmptyValueInCSV(s))
					tokens.set(i, "?");
			}
			table.addRow(tokens);
			idx++;
			if(idx % 100000 == 0){
				timer.progress(idx);
			}
			line = in.readLine();
		}
		System.out.format("Read %d lines with %d attributes. Lines with errors: %d. Lines empty: %d\n", idx, noAttributes, errorsSize, empty);
		in.close();
		timer.end();
		return table;
	}

	public static List<String> parseLineAndUnquote(String input, char seperator, char quoteChar){
		List<String> row = parseLine(input,seperator,quoteChar);
		ListIterator<String> it = row.listIterator();
		while(it.hasNext()){
			String token = it.next().trim();
			token = Utils.unQuote(token);
			it.set(token);
		}
		return row;
	}
	/**
	 * Tokenize CSV line, less trivial then it sounds, e.g.
	 * tokenize(a;b;c) -> ["a","b","c"]
	 * tokenize(a;'b;c') -> ["a","b,c"]
	 * tokenize("a;'b\'c') -> ["a","b,c"]
	 *
	 * See also http://stackoverflow.com/questions/1757065/java-splitting-a-comma-separated-string-but-ignoring-commas-in-quotes
	 *
	 * @param line
	 * @param seperator
	 * @param quoteCharacter
	 * @return
	 */
	public static List<String> parseLine(String input, char seperator, char quoteChar){
		//linked list are better performing for insertion.
		List<String> result = new LinkedList<>();
		int start = 0;
		boolean inQuotes = false;
		for (int current = 0; current < input.length(); current++) {
			if (input.charAt(current) == quoteChar){
				inQuotes = !inQuotes; // toggle state
				//second bugfix found in stackoverflow, e.g. if input "a\"b", ignore \"
				if(current != 0 && input.charAt(current -1) == '\\'){
					inQuotes = !inQuotes;
				}
			}
			boolean atLastChar = (current == input.length() - 1);
			if(atLastChar){
				//first bugfix found in stackoverflow, e.g. if input ;;; -> [null,null,null] not [null,null]
				if(input.charAt(current) == seperator){
					result.add(input.substring(start, input.length() -1));
					result.add("");
				}
				else
					result.add(input.substring(start));
			}
			else if (!inQuotes && input.charAt(current) == seperator) {
				result.add(input.substring(start, current));
				start = current + 1;
			}
		}
		return result;
	}

	public static void saveTable(Table table, File file) throws IOException
	{
		List<String> lines = new ArrayList<>();
		for(List<String> row: table.getRows())
		{
			String line = CollectionUtils.join(row,",");
			lines.add(line);
		}
		IOUtils.saveFile(lines, file);
	}


	/**
	 * E.g. splitting file on attribute 1, where values are true, false
	 * will create two files csvFileName_true.csv and csvFileName_false.csv
	 * @throws IOException
	*/
	public static void splitFileOnAttribute(File csvFile, int colIdx) throws IOException
	{
		Table table = loadCSV(csvFile, ',', '\"');
		Set<String> colums = table.getDistinctColumnValues(colIdx);
		for(String columnUnique: colums)
		{
			Table tableSplit = table.selectWhereColEquals(colIdx, columnUnique);
			tableSplit.dropColumn(colIdx);
			saveTable(tableSplit, new File(csvFile.getParentFile(),
										   IOUtils.getFilenameNoExtension(csvFile) + "_" + columnUnique + ".csv"));
		}
	}


	/**
	 * Return attr1, attr2 ... attrk, class as attribute column names
	 * @param inputCSV
	 * @return
	 * @throws IOException
	 */
	public static String[] defaultAttributes(Dataset dataset) throws IOException
	{
		assert(dataset.getFile().exists());
		String seperatorExpression = String.format("\\s*%s\\s*", dataset.getSeperator());
		BufferedReader reader = new BufferedReader(new FileReader(dataset.getFile()));
		String current = reader.readLine();
		reader.close();
		String[] attributes = current.split(seperatorExpression);
		if(dataset.getClassAttributeOrder() == ClassAttributeOrder.FIRST){
			attributes[0] = "class";
		}
		else{ // last
			attributes[attributes.length -1] = "class";
		}
		int startIdx = dataset.getClassAttributeOrder() == ClassAttributeOrder.FIRST? 1: 0;
		int endIdx = dataset.getClassAttributeOrder() == ClassAttributeOrder.LAST? attributes.length -1: attributes.length;
		for(int idx=startIdx;idx<endIdx; idx++)
		{
			attributes[idx] = String.format(Locale.ENGLISH, "%05d", idx);
		}
		return attributes;
	}

	public static String serializeLineArff(List<String> values){
		int index = 0;
		StringBuffer buff = new StringBuffer();
		for(String val: values){
			buff.append(escapeValueForArff(String.valueOf(val)));
			index++;
			if(index != values.size())
				buff.append(",");
		}
		return buff.toString();
	}

	//Nominal values with special characters, commas or spaces are enclosed in 'single quotes'.
	public static String escapeValueForArff(String stringValue){
		if(isEmptyValueInCSV(stringValue))
			return "?";
		else if(stringValue.matches("(\\-)?[0-9]+\\,[0-9]+")) { //special case: Excel files with "2,1" decimals
			stringValue = stringValue.replace(',','.');
			return stringValue.trim();
		}
		else if(CollectionUtils.stringContainsAny(stringValue, new String[]{"'"," ", "\"",","})){
			if(stringValue.matches("'.*'") || stringValue.matches("\".*\""))
				stringValue = stringValue.substring(1, stringValue.length()-1);
			//make re-entrant, e.g. if " -> \" -> \"
			String escapeQuotes = stringValue.replaceAll("[\\\\]?\"", "\\\\\"");
			//before: escapeQuotes = stringValue.replaceAll("\"", "\\\\\"");
			return String.format("\"%s\"", escapeQuotes);
		}
		else
			return stringValue;
	}

	public static String serializeLineCSV(List<String> values){
		return serializeLineCSV(values, ',');
	}

	public static String serializeLineSparseArff(String[][] pairs){
		StringBuilder buff = new StringBuilder();
		for(String[] pair: pairs){
			buff.append(String.format("%s %s,",pair[0],pair[1]));
		}
		String s = String.format("{%s}", buff.toString().substring(0, buff.length()-1));
		return s;
	}

	public static String serializeLineSparseArff(List<Integer> attributes, List<String> values){
		StringBuilder buff = new StringBuilder();
		for(int i=0; i<attributes.size(); i++){
			buff.append(String.format("%s %s,",attributes.get(i),values.get(i)));
		}
		return String.format("{%s}", buff.toString().substring(0, buff.length()-1));
	}

	public static String serializeLineCSV(List<String> values, char sep){
		int index = 0;
		StringBuilder buff = new StringBuilder();
		for(String val: values){
			buff.append(escapeValueForCSV(String.valueOf(val)));
			index++;
			if(index != values.size())
				buff.append(sep);
		}
		return buff.toString();
	}


	public static String escapeValueForCSV(String stringValue){
		if(isEmptyValueInCSV(stringValue))
			return "";
		if(stringValue.matches("'.*'") || stringValue.matches("\".*\""))
			stringValue = stringValue.substring(1, stringValue.length()-1);
		if(stringValue.contains(",")){
			return String.format("\"%s\"", stringValue);
		}
		return stringValue;
	}

	public static boolean isEmptyValueInCSV(String stringValue) {
		return stringValue.trim().isEmpty() || stringValue.equals("?") || stringValue.equals("n/a") || stringValue.equals("None") || stringValue.equals("NULL");
	}

}
