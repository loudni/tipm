package be.uantwerpen.ldataminining.preprocessing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Timer;

public class ArffTransforms {

	public static interface AttributeFilter{
		public boolean keepAttribute(String attributeName);
	}

	public static void addRowIdxColumn(File arffFile, File outputFile, int start) throws IOException
	{
		assert(arffFile.exists());
		assert(arffFile.getName().toLowerCase().endsWith(".arff"));

		List<String> output = new LinkedList<String>();
		List<String> lines = IOUtils.readFile(arffFile);
		int idxFirstAttribute = IOUtils.indexOf(lines, "@attribute");
		int idxData = IOUtils.indexOf(lines, "@data") + 1;
		//add ROW INDEX as first attribute
		for(int i=0; i < idxFirstAttribute; i++)
		{
			output.add(lines.get(i));
		}
		output.add("@attribute ROWINDEX INTEGER");
		for(int i=idxFirstAttribute; i < idxData; i++)
		{
			output.add(lines.get(i));
		}
		for(int i=idxData; i < lines.size(); i++)
		{
			if(lines.get(i).trim().equals(""))
				continue;
			if(lines.get(i).startsWith("{")) //sparse arff format -> more work
			{
				//remove starting { and ending }
				String line = lines.get(i).substring(1);
				line = line.substring(0, line.lastIndexOf('}'));
				String[] splitted = line.split("\\s*,\\s*");
				//splitted like "0 1", "1 b" ... -> now add rowindex 0: $var, and modify other attribute indexes by adding 1
				StringBuffer outputLine = new StringBuffer();
				outputLine.append("{0 ").append(i-idxData + start);
				for(String pair: splitted)
				{
					String[] pairTokens = pair.split("\\s+");
					assert(pairTokens.length == 2);
					outputLine.append(", ").append(Integer.valueOf(pairTokens[0]) + 1).append(" ").append(pairTokens[1]);
				}
				outputLine.append("}");
				output.add(outputLine.toString());
			}
			else
				output.add(String.format("%d,%s",i-idxData + start, lines.get(i)));
		}
		//save file
		IOUtils.saveFile(output, outputFile);
	}


	public static void filterAttributes(File file, File outputFile, final List<String> ATTRIBUTES_TO_KEEP) throws IOException{
		final Set<String> attributesSet = new HashSet<String>(ATTRIBUTES_TO_KEEP);
		ArffTransforms.filterAttributes(file, outputFile, new AttributeFilter(){

			@Override
			public boolean keepAttribute(String attributeName) {
				return attributesSet.contains(attributeName);
			}});
	}

	public static void filterAttributes(File file, File outputFile, AttributeFilter filter) throws IOException{
		Timer timer = new Timer("filterAttributes");
		List<String> lines = IOUtils.readFile(file);
		List<String> output = new ArrayList<String>();
		//copy file, but remove attributes
		output.add(lines.get(0));
		List<String> attributeNames = ArffUtils.getAttributeNames(file);
		List<String> attributeValues = ArffUtils.getAttributeValues(file);
		List<String> newAttributes = new ArrayList<String>();
		List<Integer> newAttributesIndexes = new ArrayList<Integer>();
		Set<String> newAttributesIndexesSet = new TreeSet<String>();
		for(int idx=0; idx<attributeNames.size(); idx++){
			String name = attributeNames.get(idx);
			if(filter.keepAttribute(name)){
				newAttributes.add(name);
				newAttributesIndexes.add(idx);
				newAttributesIndexesSet.add("" + idx);
			}
		}
		//save attributes
		for(int idx=0; idx<newAttributes.size(); idx++){
			String name = newAttributes.get(idx);
			String value = attributeValues.get(newAttributesIndexes.get(idx));
			String line = String.format("@attribute %s %s", name, value);
			output.add(line);
		}
		output.add("");
		output.add("@data");
		timer.progress("Made header", 0);
		//save rows
		int start = IOUtils.indexOf(lines, "@data");
		int rowIdx = start + 1;
		for(; rowIdx < lines.size(); rowIdx++){
			String row = lines.get(rowIdx);
			if(rowIdx % 10000 == 0){
				timer.progress(rowIdx - start, lines.size() -start);
			}
			if(!row.startsWith("{"))
			{
				List<String> values = CSVUtils.parseLine(row, ',', '\"');
				String[] newValues = new String[newAttributes.size()];
				int idx = 0;
				try{
					for(Integer index: newAttributesIndexes){
						newValues[idx++] = values.get(index);
					}
				}
				catch(Exception e){
					System.err.format("Error find columns %s value on line %d: Skipping.\n", newAttributes, rowIdx);
				}
				output.add(CSVUtils.serializeLineArff(Arrays.asList(newValues)));
			}
			else{
				String[][] pairs= ArffUtils.getSparseRow(row);
				List<String[]> newPairs = new ArrayList<String[]>();
				for(String[] pair: pairs){
					if(newAttributesIndexesSet.contains(pair[0])){
						pair[0] = "" + newAttributesIndexes.indexOf(Integer.valueOf(pair[0]));
						newPairs.add(pair);
					}
				}
				//order
				/*
				List<String[]> newPairs2 = new ArrayList<String[]>();
				for(Integer index: newAttributesIndexes){
					for(String[] pair: newPairs){
						if(pair[0].equals("" + index)){
							newPairs2.add(pair);
							break;
						}
					}
				}
				*/ //nodig?
				if(newPairs.size() > 0)
					output.add(CSVUtils.serializeLineSparseArff(newPairs.toArray(new String[newPairs.size()][])));
			}
		}
		IOUtils.saveFile(output, outputFile);
		timer.end();
	}

	public static void distinct(File input, File outputFile) throws IOException {
		Timer timer = new Timer("Distinct");
		List<String> lines = IOUtils.readFile(input);
		List<String> output = new ArrayList<String>();
		int indexData = IOUtils.indexOf(lines, "@data");
		//copy header
		output.addAll(lines.subList(0, indexData+1));
		//save filtered rows
		int duplicates = 0;
		int rowIdx = indexData + 1;
		Set<String> rows = new HashSet<String>();
		for(; rowIdx < lines.size(); rowIdx++){
			String row = lines.get(rowIdx);
			if(!rows.contains(row)){
				output.add(row);
			}
			else{
				duplicates++;
			}
			rows.add(row);
		}
		System.out.format("Removed %d duplicates\n", duplicates);
		IOUtils.saveFile(output, outputFile);
		timer.end();
	}


	public static void mergeArffDatasets(File arffFile1, File arffFile2, File outputArff) throws IOException
	{
		List<String> in1 = IOUtils.readFile(arffFile1);
		List<String> in2 = IOUtils.readFile(arffFile2);
		int dataIdx = IOUtils.indexOf(in2, "@data") + 1;
		for(int i=dataIdx; i<in2.size(); i++)
		{
			in1.add(in2.get(i));
		}
		IOUtils.saveFile(in1, outputArff);
	}

}
